#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import os
import datetime
import pylab as P

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

class Params(object):
    def __init__(self):
        self.todayDate = datetime.date(2019, 1, 20)
        self.initialdate = datetime.date(2018, 12, 19)
        self.daysSince = 6
        self.rangePlotNests = np.arange(26)


        # set path and output file names 
        adeliepath = os.getenv('ADELIEPROJDIR', default = '.')
        # paths and data to read in
        self.growDatFile = os.path.join(adeliepath, 'chick_work', 'chickData', 'ChickGrowthData.csv')
        self.resultsPath = os.path.join(adeliepath, 'chick_work', 'resultsChick')
        print('path', self.growDatFile)


class ProcessData(object):
    def __init__(self, params):

        self.params = params
        self.readData()
        self.getBirdID()
        self.getDates()
#        self.getDaysSince()
        self.getDaysSinceIndividuals()
        self.plotGrowth()

    def readData(self):
        """
        ## read in data
        """
        growDat = np.genfromtxt(self.params.growDatFile, delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'i8','i8', 'i8', 'i8', 'i8','i8', 'S10',
                     'i8', 'i8','i8', 'i8', 'i8', 'S10', 'S10'])
        self.year = growDat['year']
        self.month= growDat['month']
        self.day = growDat['day']
        self.nDat = len(self.day)
        ftCol = growDat['FT_Colour']
        self.ftColour = decodeBytes(ftCol, self.nDat)
        self.nest = growDat['nest']
        self.chick = growDat['chick']
        self.flipper = growDat['flipper']
        self.bill = growDat['bill']
        self.mass = growDat['mass']
        self.bagWT = growDat['bagWt']
        self.score = growDat['score']
        self.ftNumber = growDat['FT_Number']
        self.nChicks = growDat['N_chicks']
        self.nEggs = growDat['N_eggs']
        self.deceased = growDat['deceased']

        print('min mass', np.min(self.mass[self.mass != 0]))


    def getBirdID(self):
        """
        get unique chick id
        """
        self.nonZeroNest = self.nest[self.nest != 0]
        self.uNonZeroNest = np.unique(self.nonZeroNest)
        self.nNonZeroNest = len(self.uNonZeroNest)
        self.uNest = np.unique(self.nest)
        self.nNest = len(self.uNest)
        self.chickID = np.zeros(self.nDat, dtype = int) 
        bid = 0
        self.uBirdID = np.array([], dtype = int)
        self.uNestID = np.array([], dtype = int)
        self.uChickID = np.array([], dtype = int)
        self.uFishTag = np.array([], dtype = int)
        for i in range(self.nNest):
#        for i in range(self.nNonZeroNest):
#            maskNest = (self.nonZeroNest == self.uNonZeroNest[i])
            maskNest = (self.nest == self.uNest[i])
            uchick = np.unique(self.chick[maskNest])
            if np.isscalar(uchick):
                nchick = 1
            else:
                nchick = len(uchick)
            for j in range(nchick):
#                self.uNestID = np.append(self.uNestID, self.uNonZeroNest[i])
                self.uNestID = np.append(self.uNestID, self.uNest[i])
                self.uBirdID = np.append(self.uBirdID, uchick[j])
                maskChick = (self.chick == uchick[j])
                maskNestChick = maskNest & maskChick
                self.chickID[maskNestChick] = bid
                self.uChickID = np.append(self.uChickID, bid)
                FT_ij = np.max(self.ftNumber[maskNestChick])
                self.uFishTag = np.append(self.uFishTag, FT_ij)
                bid += 1
        self.nUChicks = len(self.uChickID)
        ## Get Nest and chick number for data with only Fish tags
###        for i range(self.nDat):
###            if self.nest[i] == 0:
###                mask_i = self.ftNumber == self.ftNumber[i]
###                nest_i = np.max(self.nest[mask_i]
###                self.nest[mask_i] = nest_i
###                self.chick                


#        ## for printing            
#        for i in range(self.nUChicks):
#            print('nest', self.uNestID[i],
#                'chick', self.uBirdID[i],
#                'bid', self.uChickID[i])
#        print('len', len(self.uChickID))

    def getDates(self):
        """
        get dates for data
        """
        self.dates = []
        for i in range(self.nDat):
            di = datetime.date(self.year[i], self.month[i], self.day[i])
            self.dates = np.append(self.dates, di)

    def getDaysSince(self):
        """
        get days since last measurement for each nest
        """
        self.daysDiff = np.zeros(self.nNest, dtype = int)
        for i in range(self.nNest):
            maskNest = (self.nest == self.uNest[i]) 
            dates_i = self.dates[maskNest]
            lastDate = np.max(dates_i)
            diffDay = self.params.todayDate - lastDate
            self.daysDiff[i] = diffDay.days
#            if self.daysDiff[i] > self.params.daysSince:
#                print('nest =', self.uNest[i], 'days =', self.daysDiff[i])


    def getDaysSinceIndividuals(self):
        """
        get days since last measurement for each individual chick
        """
        self.daysDiff = np.zeros(self.nUChicks, dtype = int)
        for i in range(self.nUChicks):
            bid = self.uChickID[i]
            nid = self.uNestID[i]
            nestbird = self.uBirdID[i]
            maskNest = (self.nest == self.uNestID[i])
            deceased_i = self.deceased[self.chickID == bid]
            deceased_i = (np.sum(deceased_i) > 0)           # bird died = T/F
#            if self.uChickID[i] == 1:
#                print('cid', self.uChickID[i], 'nid', nid, 'nestbird', nestbird, 'dead', deceased_i)

 
            dates_i = self.dates[maskNest]
            lastDate = np.max(dates_i)
            diffDay = self.params.todayDate - lastDate
            self.daysDiff[i] = diffDay.days
            if (self.daysDiff[i] > self.params.daysSince) & (not deceased_i):
                print('nest =', self.uNestID[i], 
                    'nest_Bird = ', self.uBirdID[i], 'FT =', self.uFishTag[i],
                    'days =', self.daysDiff[i])

    def plotGrowth(self):
                        
        self.dateArray = np.array(self.dates)
        self.getXTicks()
        nplots = self.nNest
        plotIndx = 0
        nfigures = np.int(np.ceil(nplots/4.0))
        for i in range(nfigures):
            P.figure(figsize=(10, 8))
            lastFigure = i == (nfigures - 1)
            for j in range(4):
                P.subplot(2,2,j+1)
                if plotIndx < nplots:
                    nest_j = self.uNest[plotIndx]
                    nestMask = (self.nest == nest_j)
                    uchick = np.unique(self.chick[nestMask])
                    if np.isscalar(uchick):
                        nchick = 1
                    else:
                        nchick = len(uchick)
                    for k in range(nchick):
                        maskChick = (self.chick == uchick[k])
                        maskNestChick = nestMask & maskChick
                        date_jk = np.unique(self.dateArray[maskNestChick])
                        nDates = len(date_jk)
                        mass_jk = np.zeros(nDates)
                        ax1 = P.gca()
                        for l in range(nDates):
                            dateMask = self.dateArray == date_jk[l]
                            nestChickDateMask = maskNestChick & dateMask
                            mass_jk[l] = self.mass[nestChickDateMask]
                        if k == 0:
                            lns1 = ax1.plot(date_jk, mass_jk, 'k-o', linewidth=5)
                        else:
                            lns1 = ax1.plot(date_jk, mass_jk, 'r-o', linewidth=3)                                            
                        for tick in ax1.xaxis.get_major_ticks():
                            tick.label.set_fontsize(8)
                        for tick in ax1.yaxis.get_major_ticks():
                            tick.label.set_fontsize(10)
                        if (j < 2):
                            ax1.set_xlabel('')
                        else:
                            ax1.set_xlabel('Months', fontsize = 12)
                        xmin = np.min(self.dateArray - datetime.timedelta(1))
                        xmax = np.max(self.dateArray + datetime.timedelta(1))
                        ymax = np.max(self.mass + 100)
                        ymin = -20.0
                        if (j==0) | (j==2):
                            ax1.set_ylabel('Mass (g)', fontsize = 12)
                        else:
                            ax1.set_ylabel('')
                        ax1.set_ylim([ymin, ymax])
#                        ax1.set_xlim([xmin, xmax])
                        P.xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
                                self.dateAxis[-1]])
#                        ax1.set_xticks([self.dateAxis])
                    P.title('nest' + str(nest_j))
                plotIndx = plotIndx + 1
            plotFileName = os.path.join(self.params.resultsPath, 'chickPlot_' + str(i) + '.png')
            P.savefig(plotFileName, format='png')
            P.show(block=lastFigure)

            
    def getXTicks(self):
        startdate = np.min(self.dateArray)
        enddate = np.max(self.dateArray)
        self.dateAxis = np.empty(4, dtype = datetime.date)
        self.dateAxis[0] = startdate - datetime.timedelta(1)
        diff = enddate - startdate
        totaldays = diff.days
        self.dateAxis[1] = startdate + datetime.timedelta(int(totaldays/3))
        self.dateAxis[2] = startdate + datetime.timedelta(int(totaldays*2.0/3.0))
        self.dateAxis[-1] = enddate
        print('dateAxis', self.dateAxis)

def main():

    params = Params()
    processdata = ProcessData(params)


if __name__ == '__main__':
    main()




