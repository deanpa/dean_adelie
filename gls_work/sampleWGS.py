#!/usr/bin/env python
import numpy as np
from osgeo import gdal
from osgeo import osr
import pandas as pd
import datetime
import os

def getColsAndRows(easting, northing, MIN_X, MAX_Y, XRES, YRES, transformer):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    # Transform the input lat/lon coordinates to the raster's coordinate system
    (lon, lat) = transformer.transform(easting, northing)

    print('min', np.min(lon), np.min(lat), 
                'max', np.max(lon), np.max(lat)) 


    col = np.round((lon - MIN_X) / XRES).astype(np.int32)
    row = np.round((MAX_Y - lat) / YRES).astype(np.int32)
#    col = np.round((easting - MIN_X) / XRES).astype(np.int32)
#    row = np.round((MAX_Y - northing) / YRES).astype(np.int32)
    print('big col', easting[col>33810])
    return(col,row)




def readFromFile(fname, bandNumber):
    """
    Read the population data out of the first band
    of the named file
    """
    ds = gdal.Open(fname)
    band = ds.GetRasterBand(bandNumber)
    data = band.ReadAsArray()
    del ds
    return data


class ProcessData():
    def __init__(self, inDataPath, outPath, bathymetryPath):

        ## RUN FUNCTIONS
        self.getLocationData(inDataPath)
#        self.getBathyData(bathymetryPath)
        self.getWGSData(bathymetryPath)


    def getLocationData(self, inDataPath):
        self.locDat = pd.read_csv(inDataPath, delimiter=',')
        print('cols', self.locDat.columns.values)        

        ## REMOVE NORTH OUTLIERS
#        self.removeNorthOutliers(northLimit)
#        self.doy = np.array(self.locDat[['doy']]).flatten().astype(np.int32)
        self.lon360 = np.array(self.locDat[['lon360']]).flatten()
        self.lon = np.array(self.locDat[['lon']]).flatten()
        self.lat = np.array(self.locDat[['lat']]).flatten()
        self.maLon360 = np.array(self.locDat[['maLon360']]).flatten()

#        self.maLon = np.array(self.locDat[['maLon']]).flatten().astype(np.float64)
#        self.maLon = np.array([self.maLon])
#        print('lon', self.maLon[:5], 'shp', self.maLon.shape)
        self.maLon = np.array(self.locDat[['maLon']]).flatten()
        print('type', type(self.maLon), self.maLon[:5], self.maLon.shape)


#        self.maLat = np.array([self.locDat['maLat'].values]).astype(np.float64)

        self.maLat = np.array(self.locDat[['maLat']]).flatten()
#        self.maLat = np.array([self.maLat])
        print('lat shp', np.shape(self.maLat), self.maLat.shape)

#        self.uDOY = np.unique(self.doy)
        self.dtime = np.array(self.locDat[['dTime']]).flatten()
        self.tag = np.array(self.locDat[['tag']]).flatten()
        self.site = np.array(self.locDat[['site']]).flatten()

        ## NUMBER OF DATA
        self.ndat = len(self.tag)
        print('min', np.min(self.maLon), np.min(self.maLat), 
                'max', np.max(self.maLon), np.max(self.maLat), 'ndat', self.ndat) 




    def getWGSData(self, bathymetryPath):
        ## READ IN BATHYMETRY DATA
        ds = gdal.Open(bathymetryPath)
        transform = ds.GetGeoTransform()
        inv_transform = gdal.InvGeoTransform(transform) # creates one that goes coords -> col/row
#        projection = ds.GetProjection()        
        rasterData = ds.GetRasterBand(1).ReadAsArray()
        print('shp rasterData', rasterData.shape)
        ## GET EXTENT AND RES OF RASTER
        xres = transform[1]
        yres = transform[5]
        x_min = transform[0]  # Minimum x-coordinate
        y_max = transform[3]  # Maximum y-coordinate
        x_max = x_min + xres * ds.RasterXSize  # Maximum x-coordinate
        y_min = y_max + yres * ds.RasterYSize  # Minimum y-coordinate
        print('Corners', x_min, y_max, x_max, y_min, 'res', xres, yres, 'Adj', x_max - (-yres * 0.3))

        print('rotation', inv_transform)
        ## ADJUST LONG FOR MAX OF 179.9
        adjustLon = x_max - (-yres * 0.3)
        self.maLon = np.where(self.maLon >= adjustLon, adjustLon, self.maLon)

        col = np.int32(inv_transform[0] + self.maLon * inv_transform[1] + self.maLat * inv_transform[2])
        row = np.int32(inv_transform[3] + self.maLon * inv_transform[4] + self.maLat * inv_transform[5])

#        col, row = np.int32(gdal.ApplyGeoTransform(inv_transform, self.maLon, self.maLat))
#        col[col > 33811] = 33811
        self.bathymetry = rasterData[row, col]

        for i in range(self.ndat):

            if col[i] > 33811:
                print('i', i, 'col', col[i], 'row', row[i], 'lon', self.maLon[i], 
                    'bath', self.bathymetry[i])
#            self.bathymetry[i] = rasterData[int(row), int(col)]
            if (i < 13000) & (i > 12980):
                print('i', i, 'col', col[i], 'row', row[i], 'lon', self.maLon[i], 
                    'lat', self.maLat[i], 'bath', self.bathymetry[i])





    def getBathyData(self, bathymetryPath):
        ## READ IN BATHYMETRY DATA
        print(bathymetryPath)
        ds = gdal.Open(bathymetryPath)
        transform = ds.GetGeoTransform()
        inv_transform = gdal.InvGeoTransform(transform) # creates one that goes coords -> col/row
        projection = ds.GetProjection()        
        rasterData = ds.GetRasterBand(1).ReadAsArray()
        print('shp rasterData', rasterData.shape)
        ## GET EXTENT AND RES OF RASTER
#        (ulx, xres, xrot, uly, yrot, yres) = src_ds.GetGeoTransform()
#        print('Details', ulx, xres, xrot, uly, yrot, yres)

        # assume points are in lat/long and raster is in some other projection
        ## SPATIAL REFERENCE POINTS
        sr_points = osr.SpatialReference()
        sr_points.ImportFromEPSG(4326)
        # Prefer to pass the points long, lat so I do this. Remove if you prefer the "new" way - lat, long
        sr_points.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        ## SPATIAL REFERENCE RASTER
        sr_raster = osr.SpatialReference()
        sr_raster.ImportFromWkt(projection)

        ct = osr.CoordinateTransformation(sr_points, sr_raster)

        self.bathymetry = np.zeros(self.ndat)
        for i in range(self.ndat):
            northing, easting, z = ct.TransformPoint(self.maLon[i], self.maLat[i])
            col, row = gdal.ApplyGeoTransform(inv_transform, easting, northing)

            if col > 33811:
                print('i', i, 'col', col, 'row', row, 'n', northing, 'e', easting, 'bath')
            self.bathymetry[i] = rasterData[int(row), int(col)]
            if (i < 3000) & (i > 2980):
                print('i', i, 'col', col, 'row', row, 'n', northing, 'e', easting, 'bath', self.bathymetry[i])


        ## EMPTY ARRAY TO POPULATE WITH ICE CONC DATA
#        self.bathymetry = np.zeros(self.ndat)
        ## GET COLUMNS AND ROWS
        ## ADJUST LONG FOR MAX OF 179.9
#        adjustLon = 180.0000001 - (-yres * 0.51)
#        lonTemp = np.where(self.lon >= adjustLon, adjustLon, self.lon)


#        (col, row) = getColsAndRows(self.maLon, self.maLat, ulx, uly, xres, -yres,
#            self.transformer)
#        print('shp bath', np.shape(srcband), 'len col', len(col), 
#            'min col', np.min(col), 'maxcol', np.max(col), 'maxrow',np.max(row))
        ## SAMPLE RASTER AND POPULATE ARRAY
#        self.bathymetry = srcband[row, col] 
#        del srcband
#        del src_ds



###########################
###########################
def main():

#    ## DAILY SEA ICE CONCENTRATION
#    seaIcePath = os.path.join('environmentData/icec.day.mean.2016.v2.nc')
    ## BATHYMETRY DATA IN WGS84
    bathymetryPath = os.path.join('/home/dean/AA_Mega_Local/LCR_CurrentResearch/Antarctica/Data/Bathymetry/IBCSO_V2/IBCSO_v2_bed_WGS84.tif')

    ## 


#    ## DAILY DAY LENGTH DATA 
#    dayLengthPath = os.path.join('environmentData/dayLightLength.csv')
    ## INPUT DATA PATH
    inDataPath = os.path.join('data/glsAdareBird3031_Wt2_Win3.csv')

    ## OUTPUT DATA WITH COVARIATES POPULATED        
    outPath = os.path.join('processedLoc/trialSample.csv')   
    ## INSTANCE OF PROCESSDATA CLASS - PROCESS DATA
    ProcessData(inDataPath, outPath, bathymetryPath)




if __name__ == '__main__':
    main()

