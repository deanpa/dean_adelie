#!/usr/bin/env python
import numpy as np
import pandas as pd
import datetime
import os


class StackData(object):
    def __init__(self, inDataPath, in2016Path, outputFName):

        ## RUN FUNCTIONS
        self.readglsdata(inDataPath)
        self.readMerge2016(in2016Path)
        self.writeData(outputFName)

    def readglsdata(self, inDataPath):
        cc = 0
        # Iterate directory
        for file in os.listdir(inDataPath):
            # check if current path is a file
            pathFName = os.path.join(inDataPath, file)
#            print('file', file, 'cc', cc)
            if not os.path.isfile(pathFName):
#                print('test', not os.path.isfile(pathFName))
                continue
            if cc == 0:
                ## READ AND OPEN FIRST FILE
                self.glsAll = pd.read_csv(pathFName, delimiter=',')
                tagNum = file.split('gls')[1].split('_')[0]
                self.glsAll.loc[:, 'tag'] = int(tagNum)           
                print('file', file, 'cc', cc, 'tag', tagNum, self.glsAll['tag'])
            else:
                dat_cc = pd.read_csv(pathFName, delimiter=',')
                tagNum = file.split('gls')[1].split('_')[0]
                dat_cc.loc[:, 'tag'] = int(tagNum) 
                self.glsAll = pd.concat([self.glsAll, dat_cc])
                print('cc', cc, 'shp dat_cc', dat_cc.shape, 'shp gls', self.glsAll.shape)

            cc += 1
        self.glsAll.loc[:, 'site'] = 'Bird'
        self.glsAll.columns = self.glsAll.columns.str.replace('.', '', regex = True)
#        self.glsAll.columns = self.glsAll.columns.str.replace('.', '')
        print('dat', self.glsAll.iloc[:, :5], 'shp', self.glsAll.shape)


    def readMerge2016(self, in2016Path):
        """
        ## READ IN DATA FROM 2016 AND MERGE WITH 2019
        """
        gls2016 = pd.read_csv(in2016Path, delimiter=',')
        gls2016.loc[:, 'site'] = 'Adare'
        self.glsCombine = pd.concat([gls2016, self.glsAll])
        print('shp glsCombine', self.glsCombine.shape, self.glsCombine.iloc[:5, -5:])




    def writeData(self, outputFName):
        ## WRITE TO FILE PANDAS DATA FRAME
        self.glsCombine.to_csv(outputFName, sep=',', header=True, index_label = 'did')







###########################
###########################
def main():

    ## INPUT DATA PATH
    inDataPath = os.path.join('processedLoc/glsNoEquinox2019')
    in2016Path = os.path.join('processedLoc/glsALL.csv')
    ## OUTPUT FILE PATH
    outputFName = os.path.join('data/glsAdareBird.csv')
    print('out data fname', outputFName)
    ## INSTANCE OF PROCESSDATA CLASS - PROCESS DATA
    stackdata = StackData(inDataPath, in2016Path, outputFName)




if __name__ == '__main__':
    main()
