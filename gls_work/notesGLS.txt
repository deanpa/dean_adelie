1) preProcessGLS.R
    A) work thru the four stages. see website: https://rdrr.io/github/slisovski/TwGeos/man/preprocessLight.html
    B) save data as *.csv


2) probGLS.R
    # This script is to prepare data and then run the data in the probGLS package 

    # write most likely path to *.csv

                        # not good
    speed.wet                   = c(0.11, 0.09, 0.41),  #most likely, sd, and max in m/sec
    speed.dry                   = c(0.06, 0.04, 0.14),

    speed.wet                   = c(0.14, 0.09, 0.46),  #most likely, sd, and max in m/sec
    speed.dry                   = c(0.06, 0.04, 0.14),

    # can swim 4-8 km/hr; and walk 2.5 km/hr (over short periods)




3) adare_GeoLight.R
    # makes plotting map antimeridian projection



## TODO:
0.5) try to find daylength or civil twilight data, or distance to darkness.         X
1) redo probGLS > 005                                                               X
2) put all data together                                                            X
3) remove equinox in R                                                              X
4) get projection in EPSG:3031 from 4326 with org and *.vrt file on command line    X
5) Add covariates - see addCovariates.py
6) Get moving average location
7) Run crawl from momentuHMM
8) Run MIFitHmm from momentuHMM


################ GIS PROCESSING
1) make vrt file to project seacurrent data to WGS: mk_multiBandUVel.py

2) Project the seacurrent data with gdalwarp using the vrt files:

    A) gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 Bird_U_Vel_WGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Bird_U_Vel_WGS.img

    B) gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 Bird_V_Vel_WGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Bird_V_Vel_WGS.img

    C) gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 Adare_U_Vel_WGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Adare_U_Vel_WGS.img

    D) gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 Adare_V_Vel_WGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Adare_V_Vel_WGS.img

3) see sampleCurrent.py


gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 projectVVel2019ToWGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Bird_V_Vel_WGS.img


gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 projectUVel2019ToWGS.vrt /home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters/Bird_U_Vel_WGS.img











2) Reproject the WGS Sea Current data to EPSG:9354, and restrict to Adare and Bird 
    migration areas.  

    Bird:
gdalwarp -s_srs EPSG:4326 -t_srs EPSG:9354 -te -1800000 -3760000 1000000 -1225000 -tap -tr 10000 -10000 -cutline blend180Longitude.shp -r bilinear -of HFA -dstnodata 0 Full_U_Vel_WGS.img Bird_U_Vel_9354.img

    gdalwarp -of HFA -co COMPRESSED=YES -geoloc -t_srs EPSG:4326 -r bilinear -tr 0.2 0.2 projectVVel2019T

gdalwarp -s_srs EPSG:4326 -t_srs [target_projection] -wo CUTLINE_POLYgons_ALL_SAME=YES -wo CUTLINE=[input_vector_file] [input_raster] [output_raster]



