#!/usr/bin/env python

import os
import numpy as np
from pathlib import Path
import pickle
import datetime



class HMMParams(object):
    def __init__(self, model=None):
        """
        ## PARAMETER CLASS FOR SIMULATION
        """
        #########   SET THE SCENARIO    ######################
        self.model = model      # Model set in command line
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3   #3000    # 1000  #3000  # number of estimates to save for each
        self.thinrate = 1    #500   #10   #30   # 200      # thin rate
        self.burnin = 0     # burn in number of iterations
        self.totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        self.interval = self.totalIterations
        self.checkpointfreq = self.interval

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True   # True or False         
        print('First Run:', self.firstRun)
        ## USE CHECKED DATA
        self.useCheckedData = False   # gibbs arrays not full from previous runs


        print('############################')
        print('Model:               ', self.model)
        print('Total Iterations:    ', self.totalIterations)
        print('ngibbs:              ', self.ngibbs)
        print('Burnin:              ', self.burnin)
        print('Thin rate:           ', self.thinrate)
        print('First run:           ', self.firstRun)

        ## GET BASE DIRECTORY LOCALLY OR ON NESI
        baseDir = os.getenv('BIRDPROJDIR', default='.')
        if baseDir == '.':
            baseDir = Path.cwd()
        # SET PATH TO DATA - SHOULD EXIST ALREADY 
        self.inputDataPath = os.path.join(baseDir, 'HMM_Data_Results', 'Data')
        ## RESULTS DIRECTORY FOR THIS SCENARIO AND SPECIES
        modelDir = 'Model' + str(self.model)
        resultsPath = os.path.join('HMM_Data_Results','Results', modelDir)
        ## PUT TOGETHER THE BASE DIRECTORY AND PATH TO RESULTS DIRECTORY 
        self.outputDataPath = os.path.join(baseDir, resultsPath)
        ## MAKE NEW RESULTS DIRECTORY IF DOESN'T EXIST
        if not os.path.isdir(self.outputDataPath):
            (baseDir / resultsPath).mkdir(parents = True)

        print('Results directory:', self.outputDataPath)
        print('############################')

        ## SET PICKLE FILE NAMES FOR PRE-PROCESSING AND RESULTS
        basicdataFName = 'basicdata_' + modelDir + '.pkl'
        self.basicdataFName = os.path.join(self.outputDataPath, basicdataFName)
        gibbsFName = 'gibbs_' + modelDir + '.pkl'
        self.gibbsFName = os.path.join(self.outputDataPath, gibbsFName)
        

        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.outputDataPath,'out_checking_mod' + 
            str(self.model)  + '.pkl')

        print('params basic path', self.basicdataFname) 
 