#!/usr/bin/env python
import numpy as np
#from osgeo import gdal
import pandas as pd
import datetime
import os



def getMeanLatLon(lonSelect, latSelect, dayWtSelect, earthRadius):
    """
    ## get the weighted ave location, given a spherical earth
    ## SEE: http://www.geomidpoint.com/calculation.html
    """
    ## CONVERT LON AND LAT TO RADIANS
    lonRad = lonSelect * np.pi / 180.0
    latRad = latSelect * np.pi / 180.0
    ## CONVERT LON/LAT TO CARTESIAN COORDINATES
    X = np.cos(latRad) * np.cos(lonRad) * earthRadius
    Y = np.cos(latRad) * np.sin(lonRad)  * earthRadius
    Z = np.sin(latRad) * earthRadius
    totalDayWt = np.sum(dayWtSelect)
#    print('dayWtSelect', np.round(dayWtSelect,4))
#    if np.sum(dayWtSelect) < .1:
#        print('totWt', totalDayWt, 
#            'sum daywtsel < .1', np.round(dayWtSelect,4), site, 'tag', tag, 'i', i)
    xMA = np.sum(X * dayWtSelect) / totalDayWt
    yMA = np.sum(Y * dayWtSelect) / totalDayWt
    zMA = np.sum(Z * dayWtSelect) / totalDayWt
    ## CONVERT BACK TO LON AND LAT IN RADIANS
    lonMA = np.arctan2(yMA, xMA)
    hyp = np.sqrt(xMA**2 + yMA**2)
    latMA = np.arctan2(zMA, hyp)
    ## CONVERT RADIAN TO DEC DEGREES
    lonMA = lonMA * 180.0 / np.pi
    latMA = latMA * 180.0 / np.pi
    return(lonMA, latMA)



class ProcessingData(object):
    def __init__(self, inDataPath, dayWindow, outputFName, gapDays, 
        minPathDays, northLimit, wtPower):
#        ## EARTH'S RADIUS IN METRES
        self.earthRadius = 6371e3
        x = 1
        ## RUN FUNCTIONS
        self.getLocationData(inDataPath, northLimit)
        self.getDayDecimal()
#        self.getPathID(gapDays)
#        self.removeShortPaths(minPathDays)
####        self.getOneLocationPerDay()
####        self.getMoveAveOnePerDay(dayWindow, wtPower)
####        self.makeWriteDF(outputFName)

        self.getMoveAverage(dayWindow, wtPower)
        self.makeWriteReducedDF(outputFName)
####        self.writeData(outputFName, minPathDays, northLimit) 


    def getLocationData(self, inDataPath, northLimit):
        self.locDat = pd.read_csv(inDataPath, delimiter=',')
        ## REMOVE NORTH OUTLIERS
#        self.removeNorthOutliers(northLimit)
        self.doy = np.array(self.locDat[['doy']]).flatten().astype(np.int32)
        self.lon360 = np.array(self.locDat[['lon360']]).flatten()
        self.lon = np.array(self.locDat[['lon']]).flatten()
        self.lat = np.array(self.locDat[['lat']]).flatten()
        self.uDOY = np.unique(self.doy)
        self.dtime = np.array(self.locDat[['dtime']]).flatten()
        self.tag = np.array(self.locDat[['tag']]).flatten()
        self.site = np.array(self.locDat[['site']]).flatten()

        ## NUMBER OF DATA
        self.ndat = len(self.tag)


    def removeNorthOutliers(self, northLimit):
        """
        remove locations above set latitude
        """
        lat = np.array(self.locDat[['lat']]).flatten()
        latMask = lat > northLimit[1]
        tagMask = (np.array(self.locDat[['tag']]).flatten() == northLimit[0])
        tagLatMask = latMask & tagMask 
        print('shp before cull', self.locDat.shape, 'nrem', np.sum(tagLatMask))
        indexNames = self.locDat[tagLatMask].index        
        self.locDat.drop(indexNames , inplace=True)
        print('shp after cull', self.locDat.shape)

    def getDayDecimal(self):
        self.dayDecimal = np.zeros(self.ndat, dtype=float)
        for i in range(self.ndat):
            dt_i = datetime.datetime.strptime(self.dtime[i], '%Y-%m-%d %H:%M:%S')
            dt_iTuple = dt_i.timetuple()
            jul_i = dt_iTuple.tm_yday
            hour_i = dt_i.hour
            min_i = dt_i.minute
            sec_i = dt_i.second
            self.dayDecimal[i] = (jul_i + (((hour_i * 3600) + (min_i * 60) + sec_i) / 
                                (24*60*60)))

        print('dayDecimal', self.dayDecimal[self.tag==5][:50], 
            'shp dayDec', self.dayDecimal.shape)



    def culldata(self):
        self.cullMask = np.ones(self.ndat, dtype = bool)
        maskadd = (self.tag == 5) & (self.dayDecimal < 67)        
        self.cullMask[maskadd] = 0
        maskadd = (self.tag == 52) & (self.dayDecimal >= 275)        
        self.cullMask[maskadd] = 0
        maskadd = (self.tag == 30) & (self.dayDecimal < 70)
        self.cullMask[maskadd] = 0


    def getPathID(self, gapDays):
        """
        set path ids for diff individual and time gaps in location data
        """
        pathID = 0
        self.pathID = np.zeros(self.ndat, dtype = int)
        prevDayDec = self.dayDecimal[0] 
        for i in range(1, self.ndat):
            dayDec_i = self.dayDecimal[i]
            timeCond = (np.abs(dayDec_i - prevDayDec) < gapDays)
            if timeCond:
                self.pathID[i] = pathID
            else:
                pathID += 1
                self.pathID[i] = pathID
            prevDayDec = self.dayDecimal[i]

    def removeShortPaths(self, minPathDays):
        """
        remove paths with days less than set in minPathDays
        """
        uPaths = np.unique(self.pathID)
        self.keepDataMask = np.ones(self.ndat, dtype=bool)
        for i in range(len(uPaths)):
            mask_i = self.pathID == uPaths[i]
            nLocs = np.sum(mask_i)
            if nLocs < minPathDays:
                self.keepDataMask[mask_i] = 0

            tag_i = self.tag[mask_i][0]
            print('tag', tag_i, 'path', uPaths[i], 'nLocs', nLocs)
#            print('path', uPaths[i], 'nloc', nLocs, 'keep', nLocs>=minPathDays)
#            if uPaths[i] == 38:
#                print('keepmask', self.keepDataMask[self.pathID == uPaths[i]])


    def getOneLocationPerDay(self):
        """
        ## GET ONE LOCATION PER JULIAN DAY
        """
        keepMask = np.zeros(self.ndat, dtype = bool)
        dayMean = np.zeros(self.ndat)
        uTags = np.unique(self.tag)
        nUTags = len(uTags)
        doy = np.int32(self.dayDecimal)
        self.newLon = []
        self.newLon360 = []
        self.newLat = []
        self.newDTime = []
        self.newTag = []
        self.newSite = []
        self.newDayDecimal = []
        ## LOOP THRU UNIQUE TAGS
        for t in range(nUTags):
            tag_t = uTags[t]
            maskTag = self.tag == tag_t
            doy_t = doy[maskTag]
            uDoy_t = np.unique(doy_t)
            nUDoy_t = len(uDoy_t)
            if tag_t < 1000:
                site_t = 'Adare'
            else:
                site_t = 'Bird'
            ## LOOP THRU UNIQUE DAYS FOR TAG t
            for d in range(nUDoy_t):
                day_d = uDoy_t[d]
                maskDOY = doy == day_d
                ## MASK OF TAG t AND DAY d
                maskTagDOY = maskTag & maskDOY
                indxAll = np.asarray(np.where(maskTagDOY)).flatten()
                dayDecimal_d = self.dayDecimal[maskTagDOY]
                minDayDecMask = dayDecimal_d == np.min(dayDecimal_d)
                

                keepIndx = indxAll[minDayDecMask] 
                self.newDTime.append(self.dtime[keepIndx].item())
                self.newTag.append(tag_t)
                self.newSite.append(site_t)
                self.newDayDecimal.append(self.dayDecimal[keepIndx].item())
                ## GET MEAN LOCATION
                dayWt = 1.0
                (lonMean, latMean) = getMeanLatLon(self.lon[maskTagDOY], 
                    self.lat[maskTagDOY], dayWt, 1, site_t, tag_t, day_d)
                self.newLon.append(lonMean)
                self.newLat.append(latMean)
                if lonMean < 0:
                    lon360 = 360.0 + lonMean
                else:
                    lon360 = lonMean
                self.newLon360.append(lon360)
        ## CONVERT TO NUMPY ARRAYS
        self.newLon = np.array(self.newLon)
        self.newLon360 = np.array(self.newLon360)
        self.newLat = np.array(self.newLat)
        self.newDTime = np.array(self.newDTime)
        self.newTag = np.array(self.newTag)
        self.newSite = np.array(self.newSite)
        self.newDayDecimal = np.array(self.newDayDecimal)
        self.newNDat = len(self.newTag)    
        print('shp newDayDecimal', self.newDayDecimal.shape, 'newlon', self.newLon.shape)
        print('site', self.newSite[:20], 'dd', self.newDayDecimal[:20], 'tag',
            self.newTag[:20])

    def getMoveAveOnePerDay(self, dayWindow, wtPower):
        self.newMALon = np.zeros(self.newNDat)
        self.newMALat = np.zeros(self.newNDat)
        for i in range(self.newNDat):
            tagMask = self.newTag == self.newTag[i]
            diffDayDecimal = np.abs(self.newDayDecimal[i] - self.newDayDecimal)
            dayDecimalMask = diffDayDecimal <= dayWindow
            tagDayDecimalMask = tagMask & dayDecimalMask
#            print('shp', tagMask.shape, dayDecimalMask.shape, self.newDayDecimal.shape,
#                self.newTag.shape)
#            print('lengths', (diffDayDecimal.shape), (tagDayDecimalMask.shape))


            self.dayWtSelect = (np.abs(dayWindow - 
                diffDayDecimal[tagDayDecimalMask]))**wtPower

#            if i < 20:
#                print('i', i, 'daywtsel', np.round(self.dayWtSelect,4), 
#                    'shp', self.dayWtSelect.shape)
#            if np.sum(self.dayWtSelect) < .1:
#                print('i', i, 'sum daywtsel < .1', np.round(self.dayWtSelect,4))

            self.lonSelect = self.newLon[tagDayDecimalMask]
            self.latSelect = self.newLat[tagDayDecimalMask]
            ## FUNCTION TO GET WEIGHTED MEAN LOCATION IN LAT LON
            earthRadius = 1.0
            (lonMA, latMA) = getMeanLatLon(self.lonSelect, self.latSelect, 
                self.dayWtSelect, earthRadius, self.newSite[i], self.newTag[i],  i)
            ## CONVERT RADIAN TO DEC DEGREES
            self.newMALon[i] = lonMA 
            self.newMALat[i] = latMA 
        self.newMALon360 = np.where(self.newMALon < 0, 360 + self.newMALon, self.newMALon) 

    def makeWriteDF(self, outputFName):
        """
        ## MAKE AND WRITE PANDAS DATAFRAME
        """
        outDF = pd.DataFrame({'lon': self.newLon, 'lon360': self.newLon360,
            'lat' : self.newLat, 'dTime' : self.newDTime, 'tag' : self.newTag,
            'site' : self.newSite, 'dayDecimal': self.newDayDecimal,
            'maLon' : self.newMALon, 'maLat' : self.newMALat, 
            'maLon360' : self.newMALon360})

        outDF.to_csv(outputFName, sep=',', header=True, index = False)


    def getMoveAverage(self, dayWindow, wtPower):
        self.maLon = np.zeros(self.ndat)
        self.maLat = np.zeros(self.ndat)
        for i in range(self.ndat):
            tagMask = self.tag == self.tag[i]
            diffDayDecimal = np.abs(self.dayDecimal[i] - self.dayDecimal)
            dayDecimalMask = diffDayDecimal <= dayWindow
            tagDayDecimalMask = tagMask & dayDecimalMask

            self.dayWtSelect = (dayWindow - diffDayDecimal[tagDayDecimalMask])**wtPower
            self.lonSelect = self.lon[tagDayDecimalMask]
            self.latSelect = self.lat[tagDayDecimalMask]
            ## FUNCTION TO GET WEIGHTED MEAN LOCATION IN LAT LON
            earthRadius = 1.0
            (lonMA, latMA) = getMeanLatLon(self.lonSelect, self.latSelect, 
                self.dayWtSelect, earthRadius)
            ## CONVERT RADIAN TO DEC DEGREES
            self.maLon[i] = lonMA
            self.maLat[i] = latMA

            if i < 10:
                print('i', i, 'n in wt', len(self.lonSelect), 
                    'lon i', np.round(self.lon[i], 4),
                    'maLon', np.round(self.maLon[i], 4), 'lat i', 
                    np.round(self.lat[i], 4), 'ma Lat', 
                    np.round(self.maLat[i], 4))


    def makeWriteReducedDF(self, outputFName):
        """
        ## MAKE AND WRITE PANDAS DATAFRAME
        """
        maLon360 = np.where(self.maLon < 0, 360. + self.maLon, self.maLon)

        outDF = pd.DataFrame({'lon': self.lon, 'lon360': self.lon360,
            'lat' : self.lat, 'dTime' : self.dtime, 'tag' : self.tag,
            'site' : self.site, 'dayDecimal': self.dayDecimal,
            'maLon' : self.maLon, 'maLat' : self.maLat, 
            'maLon360' : maLon360})

        outDF.to_csv(outputFName, sep=',', header=True, index = False)



#    def getMeanLatLon(self, i):
#        """
#        ## get the weighted ave location, given a spherical earth
#        ## SEE: http://www.geomidpoint.com/calculation.html
#        """
#        ## CONVERT LON AND LAT TO RADIANS
#        lonRad = self.lonSelect * np.pi / 180.0
#        latRad = self.latSelect * np.pi / 180.0
#        ## CONVERT LON/LAT TO CARTESIAN COORDINATES
#        X = np.cos(latRad) * np.cos(lonRad) # * self.earthRadius
#        Y = np.cos(latRad) * np.sin(lonRad) # * self.earthRadius
#        Z = np.sin(latRad) # * self.earthRadius
#        totalDayWt = np.sum(self.dayWtSelect)
#        xMA = np.sum(X * self.dayWtSelect) / totalDayWt
#        yMA = np.sum(Y * self.dayWtSelect) / totalDayWt
#        zMA = np.sum(Z * self.dayWtSelect) / totalDayWt
#        ## CONVERT BACK TO LON AND LAT IN RADIANS
#        lonMA = np.arctan2(yMA, xMA)
#        hyp = np.sqrt(xMA**2 + yMA**2)
#        latMA = np.arctan2(zMA, hyp)
#        ## CONVERT RADIAN TO DEC DEGREES
#        self.maLon[i] = lonMA * 180.0 / np.pi
#        self.maLat[i] = latMA * 180.0 / np.pi


    def writeData(self, outputFName, minPathDays, northLimit):
        ## ADD ICE TO PANDAS DATA FRAME
        self.locDat.loc[:, 'dayDecimal'] = self.dayDecimal
        self.locDat.loc[:, 'maLon'] = self.maLon
        self.locDat.loc[:, 'maLat'] = self.maLat
        maLon360 = np.where(self.maLon < 0, 360. + self.maLon, self.maLon)
        self.locDat.loc[:, 'maLon360'] = maLon360 
        self.culldata()
        indexNames = self.locDat[~self.cullMask].index        
        self.locDat.drop(indexNames , inplace=True)
        ## remove north outliers
#        self.removeAverOutlier(northLimit)
        self.locDat.to_csv(outputFName, sep=',', header=True, index = False)
###        self.locDat.to_csv(outputFName, sep=',', header=True, index_label = 'did')
#        self.printMasked(minPathDays)


    def printMasked(self, minPathDays):
        print('####### SHORT PATHS REMOVED ##########')
        uPaths = np.unique(self.locDat.loc[:,'pathID'])
        print('shape loc 1', self.locDat.shape,
                'sum keepmask', np.sum(self.keepDataMask))
        print('upaths', uPaths)
        for i in range(len(uPaths)):
            nLocs = np.sum(self.locDat.loc[:, 'pathID'] == uPaths[i])
            print('path', uPaths[i], 'nloc', nLocs, 'keep', nLocs>=minPathDays)

    def removeAverOutlier(self, northLimit):
        """
        remove locations above set latitude
        """
        yMask = self.maY < northLimit[1]
        tagMask = (np.array(self.locDat[['tag']]).flatten() == northLimit[0])
        tagYMask = yMask & tagMask 
        print('shp before cull', self.locDat.shape, 'nrem', np.sum(tagYMask))
        indexNames = self.locDat[tagYMask].index        
        self.locDat.drop(indexNames , inplace=True)
        print('shp after cull', self.locDat.shape)



###########################
###########################
def main():

    dayWindow = 3.0
    wtPower = 1.0
    gapDays = 9.0    
    minPathDays = 7
    northLimit = 50
    ## INPUT DATA PATH
    inDataPath = os.path.join('data/glsAdareBird.csv')
    ## OUTPUT FILE PATH
    outputFName = os.path.join('data/glsAdareBird_MA_Wt1_Win3.csv')
    ## INSTANCE OF PROCESSDATA CLASS - PROCESS DATA
    processingdata = ProcessingData(inDataPath, dayWindow, outputFName, gapDays, 
        minPathDays, northLimit, wtPower)




if __name__ == '__main__':
    main()

