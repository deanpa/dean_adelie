#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 22:52:01 2018

@author: behrense
"""
#Analyse of MHW
import os
from netCDF4 import Dataset  # http://code.google.com/p/netcdf4-python/
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#import pyproj

seaData = '/home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/Data_dean'


d1=Dataset(os.path.join(seaData, 'eORCA025-NEB020o_5d_20190101_20191231_grid_T.nc'))
d2 = Dataset(os.path.join(seaData, 'eORCA025-NEB020o_5d_20160101_20161231_grid_U.nc'))
d3 = Dataset(os.path.join(seaData, 'eORCA025-NEB020o_5d_20160101_20161231_grid_V.nc'))
lon=d1['nav_lon'][:]
lon[lon<0]+=360
lat=d1['nav_lat'][:]
temp=d1['votemper'][:]

## GEOTRANSFORM PARAMETERS -- ASSUMED???
ulx = 132.0
lrx = 240.0
lry = -75
uly = -55
xres = 1.0/5.0
yres = -xres
xRotation = 0
yRotation = 0
rasterShp = lat.shape
nRow = rasterShp[0]
nCol = rasterShp[1]

plt.close('all')
fig = plt.figure(figsize=(16,8))
ax1 = fig.add_subplot(111)
m =Basemap(projection='merc',llcrnrlat=-75,urcrnrlat=-55,llcrnrlon=132,urcrnrlon=240,resolution='i')
m.pcolormesh(lon,lat, temp[0,0,:,:],vmin=-1,vmax=3,latlon=True,shading='auto',zorder=0)
plt.colorbar()
parallels = np.arange(-80.,80,10)
m.drawparallels(parallels,labels=[True,False,True,False],fontsize=12)
meridians = np.arange(10.,350.,10)
m.drawmeridians(meridians,labels=[True,False,False,True],fontsize=12)

# with this command you can project your geographic animals track onto the projection
inDataPath = os.path.join('/home/dean/workfolder/projects/dean_adelie/gls_work/data/glsAdareBird.csv')
locDat = pd.read_csv(inDataPath, delimiter=',')
lon_Data = (np.array(locDat[['lon360']]).flatten())[-60:]
#lon_Data = (np.array(locDat[['lon']]).flatten())[-60:]
lat_Data = (np.array(locDat[['lat']]).flatten())[-60:]

xx,yy=m(lon_Data,lat_Data)
plt.plot(xx,yy)
plt.show()

# Create a projection transformer from WGS84 (latitude-longitude) to the projected coordinate system
proj = pyproj.Proj(proj="latlong", datum="WGS84")
transformer = pyproj.Transformer.from_proj(proj, proj)

projected_coords = transformer.transform(lon_Data, lat_Data)


# Calculate the inverse geotransform parameters
inv_transform = (ulx, xres, 0.0, uly, 0.0, -yres)  # Note the negative yres
print('inv_transform', inv_transform)

# Convert the inverse geotransform parameters to decimal degrees
inv_transform_latlon = transformer.itransform([(inv_transform[0], inv_transform[3])])
inv_transform_degrees = next(inv_transform_latlon)
print('inv_transform_degrees', inv_transform_degrees, 'inv_transform_latlon', inv_transform_latlon)

col = np.int32(ulx + (lon_Data * xres))
row = np.int32(uly + (lat_Data * yres))

print('row', row, 'col', col)



        col = np.int32(inv_transform[0] + self.maLon * inv_transform[1] + self.maLat * inv_transform[2])
        row = np.int32(inv_transform[3] + self.maLon * inv_transform[4] + self.maLat * inv_transform[5])




# Define the latitude-longitude points as a NumPy array
points = np.array([(lat1, lon1), (lat2, lon2), ...])  # Shape: (n, 2), n is the number of points

# Adjust longitude values to be in the range of -180 to 180
points[:, 1] = (points[:, 1] + 180) % 360 - 180

# Calculate the pixel coordinates for each latitude-longitude point
px = ((points[:, 1] - ulx) / xres).astype(int)
py = ((points[:, 0] - uly) / yres).astype(int)
