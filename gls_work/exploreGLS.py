#!/usr/bin/env python

import os
import numpy as np
import pylab as P
import datetime


class Params(object):
    def __init__(self):
        self.filtType = 'midnight'
        self.startDate = datetime.date(2016, 2, 26)
        self.endDate = datetime.date(2016, 10, 26)
        self.minLat = -88.0
        self.maxLat = -25.0
        self.limitLong = 130.0

class RawData(object):
    def __init__(self, params, adelieDatFile, figurePath, figureLat):
        self.params = params
        self.adelie = np.genfromtxt(adelieDatFile,  delimiter=',', names=True,
            dtype=['S10', 'S10', 'i8', 'i8', 'i8', 'S10', 'f8', 'S10', 'S10', 
                    'f8', 'i8', 'f8'])
        self.type = self.adelie['type']
        self.month = self.adelie['month']
        self.day = self.adelie['day']
        self.year = self.adelie['year']
        self.lat = self.adelie['latStation']
        self.long = self.adelie['longitude']
        self.ndat = len(self.long)
        ##### RUN FUNCTIONS
        self.filterDat()
        self.plotLat(figureLat)
        self.plotPath(figurePath)
        

    def filterDat(self):
        self.maskDat = np.zeros(self.ndat, dtype = bool)
        self.date = []
        for i in range(self.ndat):
            # create mask
            date_i = datetime.date(self.year[i], self.month[i], self.day[i])
            dateMask = (date_i >= self.params.startDate) & (self.params.endDate >= date_i)

#            print(date_i, self.params.startDate)

            self.date = np.append(self.date, date_i)

#            print(i, dateMaskEnd, dateMaskStart)
    
            latMask = (self.lat[i] >= self.params.minLat) & (self.lat[i] < self.params.maxLat)
            longMask = np.abs(self.long[i]) >= self.params.limitLong
            self.maskDat[i] = dateMask & latMask & longMask
        # filter data
        self.year = self.year[self.maskDat]
        self.month = self.month[self.maskDat]
        self.day = self.day[self.maskDat]
        self.date = self.date[self.maskDat]
        self.lat = self.lat[self.maskDat]
        self.long = self.long[self.maskDat]

    def plotLat(self, figureLat):

        P.figure()
        P.plot(self.date, self.lat)
        P.xlabel('Date', fontsize = 17)
        P.ylabel('Latitude', fontsize = 17)
        P.savefig(figureLat, format='png')
        P.show()

    def plotPath(self, figurePath):

        P.figure()
        P.plot(self.long, self.lat)
        P.xlabel('Longitude', fontsize = 17)
        P.ylabel('Latitude', fontsize = 17)
        P.savefig(figurePath, format='png')
        P.show()


  



                
########            Main function
#######
def main():
    adeliepath = os.getenv('ADELIEPROJDIR', default = '.')
    # paths and data to read in
    adelieDatFile = os.path.join(adeliepath,'gls038_ProcessGlen.csv')
    figureLat = os.path.join(adeliepath,'AdelieLatitude038Glen.png')
    figurePath = os.path.join(adeliepath,'AdeliePath038Glen.png')


    params = Params()
    # initiate rawdata class and object 
    rawdata = RawData(params, adelieDatFile, figurePath, figureLat)

if __name__ == '__main__':
    main()
