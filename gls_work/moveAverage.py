#!/usr/bin/env python
import numpy as np
from osgeo import gdal
import pandas as pd
import datetime
import os



class ProcessData():
    def __init__(self, locationDataPath, dayWindow, outMovePath, gapDays, 
        minPathDays, northLimit):

        ## RUN FUNCTIONS

        self.getLocationData(locationDataPath, northLimit)
        self.getDayDecimal()
#        self.getPathID(gapDays)
#        self.removeShortPaths(minPathDays)
        self.getMoveAverage(dayWindow)
        self.writeData(outMovePath, minPathDays, northLimit) 


    def getLocationData(self, locationDataPath, northLimit):
        self.locDat = pd.read_csv(locationDataPath, delimiter=',')
        ## REMOVE NORTH OUTLIERS
#        self.removeNorthOutliers(northLimit)

        self.x = np.array(self.locDat[['X']]).flatten()
        self.y = np.array(self.locDat[['Y']]).flatten()
        self.doy = np.array(self.locDat[['doy']]).flatten().astype(np.int)
        self.lon360 = np.array(self.locDat[['lon360']]).flatten()
        self.uDOY = np.unique(self.doy)
        self.dtime = np.array(self.locDat[['dtime']]).flatten()
        self.tag = np.array(self.locDat[['tag']]).flatten()
        ## NUMBER OF DATA
        self.ndat = len(self.x)


    def removeNorthOutliers(self, northLimit):
        """
        remove locations above set latitude
        """
        lat = np.array(self.locDat[['lat']]).flatten()
        latMask = lat > northLimit[1]
        tagMask = (np.array(self.locDat[['tag']]).flatten() == northLimit[0])
        tagLatMask = latMask & tagMask 
        print('shp before cull', self.locDat.shape, 'nrem', np.sum(tagLatMask))
        indexNames = self.locDat[tagLatMask].index        
        self.locDat.drop(indexNames , inplace=True)
        print('shp after cull', self.locDat.shape)

    def getDayDecimal(self):
        self.dayDecimal = np.zeros(self.ndat, dtype=float)
        for i in range(self.ndat):
            dt_i = datetime.datetime.strptime(self.dtime[i], '%Y-%m-%d %H:%M:%S')
            dt_iTuple = dt_i.timetuple()
            jul_i = dt_iTuple.tm_yday
            hour_i = dt_i.hour
            min_i = dt_i.minute
            sec_i = dt_i.second
            self.dayDecimal[i] = (jul_i + (((hour_i * 3600) + (min_i * 60) + sec_i) / 
                                (24*60*60)))

        print('5', self.dayDecimal[self.tag==5][:50])



    def culldata(self):
        self.cullMask = np.ones(self.ndat, dtype = bool)
        maskadd = (self.tag == 5) & (self.dayDecimal < 67)        
        self.cullMask[maskadd] = 0
        maskadd = (self.tag == 52) & (self.dayDecimal >= 275)        
        self.cullMask[maskadd] = 0
        maskadd = (self.tag == 30) & (self.dayDecimal < 70)
        self.cullMask[maskadd] = 0


    def getPathID(self, gapDays):
        """
        set path ids for diff individual and time gaps in location data
        """
        pathID = 0
        self.pathID = np.zeros(self.ndat, dtype = int)
        prevDayDec = self.dayDecimal[0] 
        for i in range(1, self.ndat):
            dayDec_i = self.dayDecimal[i]
            timeCond = (np.abs(dayDec_i - prevDayDec) < gapDays)
            if timeCond:
                self.pathID[i] = pathID
            else:
                pathID += 1
                self.pathID[i] = pathID
            prevDayDec = self.dayDecimal[i]

    def removeShortPaths(self, minPathDays):
        """
        remove paths with days less than set in minPathDays
        """
        uPaths = np.unique(self.pathID)
        self.keepDataMask = np.ones(self.ndat, dtype=bool)
        for i in range(len(uPaths)):
            mask_i = self.pathID == uPaths[i]
            nLocs = np.sum(mask_i)
            if nLocs < minPathDays:
                self.keepDataMask[mask_i] = 0

            tag_i = self.tag[mask_i][0]
            print('tag', tag_i, 'path', uPaths[i], 'nLocs', nLocs)
#            print('path', uPaths[i], 'nloc', nLocs, 'keep', nLocs>=minPathDays)
#            if uPaths[i] == 38:
#                print('keepmask', self.keepDataMask[self.pathID == uPaths[i]])

    def getMoveAverage(self, dayWindow):
        self.maX = np.zeros(self.ndat)
        self.maY = np.zeros(self.ndat)
        for i in range(self.ndat):
            tagMask = self.tag == self.tag[i]
            diffDayDecimal = np.abs(self.dayDecimal[i] - self.dayDecimal)
            dayDecimalMask = diffDayDecimal <= dayWindow
            tagDayDecimalMask = tagMask & dayDecimalMask

            dayWtSelect = (dayWindow - diffDayDecimal[tagDayDecimalMask])**2
            xSelect = self.x[tagDayDecimalMask]
            ySelect = self.y[tagDayDecimalMask]
            xMA = np.sum(xSelect * dayWtSelect) / np.sum(dayWtSelect)
            yMA = np.sum(ySelect * dayWtSelect) / np.sum(dayWtSelect)
            self.maX[i] = xMA
            self.maY[i] = yMA

    def writeData(self, outMovePath, minPathDays, northLimit):
        ## ADD ICE TO PANDAS DATA FRAME
        self.locDat.loc[:, 'dayDecimal'] = self.dayDecimal
        self.locDat.loc[:, 'xMA'] = self.maX
        self.locDat.loc[:, 'yMA'] = self.maY
        self.culldata()
        indexNames = self.locDat[~self.cullMask].index        
        self.locDat.drop(indexNames , inplace=True)


        ## remove north outliers
#        self.removeAverOutlier(northLimit)




##        self.locDat.loc[:, 'pathID'] = self.pathID
##        ## REMOVE SHORT PATHS
##        indexNames = self.locDat[~self.keepDataMask].index        
##        self.locDat.drop(indexNames , inplace=True)
        self.locDat.to_csv(outMovePath, sep=',', header=True, index_label = 'did')
#        self.printMasked(minPathDays)


    def printMasked(self, minPathDays):
        print('####### SHORT PATHS REMOVED ##########')
        uPaths = np.unique(self.locDat.loc[:,'pathID'])
        print('shape loc 1', self.locDat.shape,
                'sum keepmask', np.sum(self.keepDataMask))
        print('upaths', uPaths)
        for i in range(len(uPaths)):
            nLocs = np.sum(self.locDat.loc[:, 'pathID'] == uPaths[i])
            print('path', uPaths[i], 'nloc', nLocs, 'keep', nLocs>=minPathDays)

    def removeAverOutlier(self, northLimit):
        """
        remove locations above set latitude
        """
        yMask = self.maY < northLimit[1]
        tagMask = (np.array(self.locDat[['tag']]).flatten() == northLimit[0])
        tagYMask = yMask & tagMask 
        print('shp before cull', self.locDat.shape, 'nrem', np.sum(tagYMask))
        indexNames = self.locDat[tagYMask].index        
        self.locDat.drop(indexNames , inplace=True)
        print('shp after cull', self.locDat.shape)



###########################
###########################
def main():

    dayWindow = 3.0
    gapDays = 9.0    
    minPathDays = 7
    northLimit = [52, -3228100]
    

    locationDataPath = os.path.join('processedLoc/glsALLAntPol.csv')
    ## OUTPUT DATA WITH COVARIATES POPULATED        
    outMovePath = os.path.join('processedLoc/glsAverageLoc3_2.csv')   
    ## INSTANCE OF PROCESSDATA CLASS - PROCESS DATA
    ProcessData(locationDataPath, dayWindow, outMovePath, gapDays, minPathDays, northLimit)




if __name__ == '__main__':
    main()

