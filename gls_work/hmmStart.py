#!/usr/bin/env python

import sys
import optparse
from HMMScripts import hmmMain
import hmmParams

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--model", dest="model", help="Set model")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

if __name__ == '__main__':

    cmdargs = CmdArgs()

    params = hmmParams.HMMParams(int(cmdargs.model))

    hmmMain.main(params)


## ON COMMAND LINE, RUN SCRIPT LIKE THIS FOR KEA AND SCENARIO 1:
### ./hmmStart.py --model 1


