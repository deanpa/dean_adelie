#!/usr/bin/env python
import numpy as np
from osgeo import gdal
import pandas as pd
import datetime
import os

def getColsAndRows(easting, northing, MIN_X, MAX_Y, XRES, YRES):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    col = np.round((easting - MIN_X) / XRES).astype(np.int)
    row = np.round((MAX_Y - northing) / YRES).astype(np.int)
    return(col,row)




def readFromFile(fname, bandNumber):
    """
    Read the population data out of the first band
    of the named file
    """
    ds = gdal.Open(fname)
    band = ds.GetRasterBand(bandNumber)
    data = band.ReadAsArray()
    del ds
    return data


class ProcessData():
    def __init__(self, seaIcePath, locationDataPath, outCovPath, bathymetryPath,
            dayLengthPath):

        ## RUN FUNCTIONS

        self.getLocationData(locationDataPath)
        self.getDates()
#        self.getIceData(seaIcePath)
#        self.getBathyData(bathymetryPath)
        self.getDayLength(dayLengthPath)
        self.writeData(outCovPath)

    def getLocationData(self, locationDataPath):
        self.locDat = pd.read_csv(locationDataPath, delimiter=',')
        self.x = np.array(self.locDat[['mu.x']]).flatten()
        self.y = np.array(self.locDat[['mu.y']]).flatten()
        self.dayDec = np.array(self.locDat[['dayDecimal']]).flatten()
        self.julianYear = self.dayDec.astype(int)
        self.lat = np.array(self.locDat[['lat']]).flatten()
        self.lon = np.array(self.locDat[['lon']]).flatten()
        self.lon360 = np.where(self.lon < 0.0, 360.0 + self.lon, self.lon)

        ## NUMBER OF DATA
        self.ndat = len(self.lat)
        ## ADD WINTER INDICATOR VARIABLE: MAY - AUGUST
        maskW = (self.dayDec >= 122) & (self.dayDec < 244)
        self.winterI = np.ones(self.ndat, dtype = int)
        self.winterI[maskW] = 0         
#        print('Winter', self.winterI[~maskW][:5], self.winterI[maskW][:5])



#        print('x', np.shape(self.x[:20]), 'dayDec', self.dayDec[:20], 
#            'jul', self.julianYear[:20])
#        print('lon', self.lon[768:770], 'lon360', self.lon360[768:770])
#        print('locDat', self.locDat[['X', 'Y', 'doy']].head(10))
#        self.uDOY = np.unique(self.doy)
#        self.dtime = np.array(self.locDat[['dtime']]).flatten()

    def getDates(self):
        """
        ## GET DATES FROM JULIAN DAY OF YEAR
        """
        self.datesAll = np.empty(self.ndat, dtype=datetime.date)
        for i in range(self.ndat):
            yr = 2016
            day_i = int(self.julianYear[i] - 1)
            self.datesAll[i] = datetime.datetime(yr, 1, 1) + datetime.timedelta(day_i)
        print('datesAll', self.datesAll[:12]) 

    def getIceData(self, seaIcePath):
        ## READ IN SEA ICE MULTIBAND *.nc
        src_ds = gdal.Open(seaIcePath)
        ## GET EXTENT AND RES OF RASTER
        (ulx, xres, xrot, uly, yrot, yres) = src_ds.GetGeoTransform()
        print('Details', ulx, xres, xrot, uly, yrot, yres)
        ## EMPTY ARRAY TO POPULATE WITH ICE CONC DATA
        self.iceconc = np.zeros(self.ndat)
        uDOY = np.unique(self.julianYear)
        ## LOOP THRU DAYS OF YEAR ('DOY')
        for bandNumber in uDOY:
            b = np.int(bandNumber)
            ## GET BAND FOR DAY b
            srcband = src_ds.GetRasterBand(b).ReadAsArray()
            ## MASK OF LOCATION POINT DATA FOR DAY OF YEAR
            doyMask = self.julianYear == bandNumber
            ## GET LAT AND LONG
            long_b = self.lon360[doyMask]
            lat_b = self.lat[doyMask]
            ## GET COLUMNS AND ROWS
            (col, row) = getColsAndRows(long_b, lat_b, ulx, uly, xres, -yres)
            ## SAMPLE RASTER AND POPULATE ARRAY
            self.iceconc[doyMask] = srcband[row, col] 
            del srcband
        del src_ds
        self.iceconc[self.iceconc < 0.0] = np.nan



    def getBathyData(self, bathymetryPath):
        ## READ IN BATHYMETRY DATA
        print(bathymetryPath)
        src_ds = gdal.Open(bathymetryPath)
        srcband = src_ds.GetRasterBand(1).ReadAsArray()
        ## GET EXTENT AND RES OF RASTER
        (ulx, xres, xrot, uly, yrot, yres) = src_ds.GetGeoTransform()
        print('Details', ulx, xres, xrot, uly, yrot, yres)
        ## EMPTY ARRAY TO POPULATE WITH ICE CONC DATA
#        self.bathymetry = np.zeros(self.ndat)
        ## GET COLUMNS AND ROWS
        ## ADJUST LONG FOR MAX OF 179.9
        adjustLon = 180.0000001 - (-yres * 0.51)
        lonTemp = np.where(self.lon >= adjustLon, adjustLon, self.lon)
        (col, row) = getColsAndRows(lonTemp, self.lat, ulx, uly, xres, -yres)
        print('shp bath', np.shape(srcband), 'len col', len(col), 
            'min col', np.min(col), 'maxcol', np.max(col), 'maxrow',np.max(row))
        ## SAMPLE RASTER AND POPULATE ARRAY
        self.bathymetry = srcband[row, col] 
        del srcband
        del src_ds

    def getDayLength(self, dayLengthPath):
#        ## GET DATE AND TIME FROM LOCATION DATA
#        self.timeStamp = np.empty(self.ndat, dtype=datetime.datetime)
        ## GET DAY LENGTH DATA
        self.dayLengthData = pd.read_csv(dayLengthPath, delimiter=',')
        latDayLength = np.array(self.dayLengthData.loc[:, 'lat']).flatten() * -1

        self.seconds= np.zeros(self.ndat)
        ## LOOP THRU LOCATION DATA AND GET DAY LENGTH
        for i in range(self.ndat):
            lat_i = self.lat[i]
            ## GET DATE AND TIME FROM LOCATION DATA
            dt_i = self.datesAll[i]
#            dt_i = datetime.datetime.strptime(self.dtime[i], '%Y-%m-%d %H:%M:%S')
            month_i = dt_i.month
            day_i = dt_i.day
            ## get closest latitude
            diffLat = np.abs(latDayLength - lat_i)
            latMask = diffLat == np.min(diffLat)
            dayMask = np.array(self.dayLengthData.loc[:, 'day']).flatten() == day_i
            monthDayLength = np.array(self.dayLengthData.iloc[:, month_i]).flatten()
           
            hourMinMask = latMask & dayMask
            hourMinStr = str(monthDayLength[hourMinMask])

            if i < 5:
                print('i', i, 'lat_i', lat_i, 'dt_i', dt_i, 'hourMinSt', hourMinStr)

            time_i = datetime.datetime.strptime(hourMinStr, "['%H:%M:%S']")
            hour_i = time_i.time().hour
            min_i = datetime.datetime.strptime(hourMinStr, "['%H:%M:%S']").time().minute
            self.seconds[i] = (hour_i * 60 + min_i) * 60

            if i < 5:
                print('lat_i', lat_i, 'dt_i', dt_i, 'DL hour', hour_i, 
                    'DL_min', min_i, 'DL sec', self.seconds[i])
 


    def writeData(self, outCovPath):
        ## ADD ICE TO PANDAS DATA FRAME
#        self.locDat.loc[:, 'iceConc'] = self.iceconc
#        self.locDat.loc[:, 'bathymetry'] = self.bathymetry
        self.locDat.loc[:, 'dayLightSec'] = self.seconds
        self.locDat.loc[:, 'lon360'] = self.lon360
        self.locDat.loc[:, 'winterI'] = self.winterI
        keepData = self.locDat[['ID', 'dayDecimal', 'dayLightSec', 'winterI']]
#        keepData = self.locDat[['ID', 'dayDecimal', 'iceConc', 'bathymetry', 'dayLightSec']]
    
        keepData.to_csv(outCovPath, sep=',', header=True, index_label = 'did')


###########################
###########################
def main():

    ## DAILY SEA ICE CONCENTRATION
    seaIcePath = os.path.join('environmentData/icec.day.mean.2016.v2.nc')
    ## BATHYMETRY DATA IN WGS84
    bathymetryPath = os.path.join(
        ('environmentData/WGS84/ibcso_v1_bed.tif'))
#        ('environmentData/LBAthym_x90_X270_y-80_Y-50_nx3601_ny1801.tif'))
    ## DAILY DAY LENGTH DATA 
    dayLengthPath = os.path.join('environmentData/dayLightLength.csv')
    ## DATA WITH COORD IN ANTARCTIC POLAR COORD.
    locationDataPath = os.path.join('processedLoc/crwLatLon.csv')
    ## OUTPUT DATA WITH COVARIATES POPULATED        
    outCovPath = os.path.join('processedLoc/crwCovariates.csv')   
    ## INSTANCE OF PROCESSDATA CLASS - PROCESS DATA
    ProcessData(seaIcePath, locationDataPath, outCovPath, bathymetryPath,
        dayLengthPath)




if __name__ == '__main__':
    main()
