#!/usr/bin/env python

import os
import numpy as np
from netCDF4 import Dataset
import pandas as pd
import time
from numba import njit, prange
from scipy.stats.mstats import mquantiles
from osgeo import gdal

## read in point data
inDataPath = os.path.join('/home/dean/workfolder/projects/dean_adelie/gls_work/data/glsAdareBird_MA_Wt1_Win3.csv')
locDat = pd.read_csv(inDataPath, delimiter=',')
lon_pt = (np.array(locDat[['maLon360']]).flatten())     #[:50]
lat_pt = (np.array(locDat[['maLat']]).flatten())        #[:50]
maLon = (np.array(locDat[['maLon']]).flatten())     #[:50]
maLat = (np.array(locDat[['maLat']]).flatten())        #[:50]
site = (np.array(locDat[['site']]).flatten())        #[:50]
birdMask = site == 'Bird'
dayDec = (np.array(locDat[['dayDecimal']]).flatten())
rastBand = np.int16(dayDec / 5)
rastBand[rastBand>72] = 72

# Read the netCDF file
seaData = '/home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/Data_dean'
dataset = Dataset(os.path.join(seaData, 'eORCA025-NEB020o_5d_20190101_20191231_grid_T.nc'))
#dataset = Dataset(os.path.join(seaData, 'eORCA025-NEB020o_5d_20190101_20191231_grid_U.nc'))

# read multiband WGS U Vel
velPath = '/home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/WGSRasters'
velData = os.path.join(velPath, 'Bird_U_Vel_WGS.img')

# Extract the sst, latitude, and longitude arrays
lon=dataset.variables['nav_lon'][:]
lon[lon<0]+=360
lat=dataset.variables['nav_lat'][:]
temp=dataset.variables['votemper'][0,0,:,:] 

## TRYING TO GET RASTER VALUES USING WHOLE ARRAY OPERATIONS
# Determine the pixel coordinates corresponding to the points of interest
nrows, ncols = temp.shape
ulx = lon[0, 0]
uly = lat[0, 0]
lly = lat[-1, 0]
urx = lon[0, -1]
minLon = np.min(lon)
maxLon = np.max(lon)

# Calculate the latitude and longitude increments per pixel
lat_increment = (lly - uly) / (nrows - 1)
lon_increment = (urx - ulx) / (ncols - 1)
## GET PIXEL COORDINATES
rowID = np.int32((lat_pt - uly) / lat_increment)
colID = np.int32((lon_pt - ulx) / lon_increment)
## GET DATA FROM RASTER
#tempPts = temp[rowID, colID].filled(np.nan)
#latID = lat[rowID, colID].filled(np.nan)
#lonID = lon[rowID, colID].filled(np.nan)

#print('increments', lat_increment, lon_increment, ulx, uly, urx, 'shp', lat.shape)


## LOOP TO LOOK AT LAT AND LON ALONG MERIDIAN OR ZONAL LINES
#for i in range(1, (ncols-1)):
#    if i < nrows:
#        print('i', i, 'Lon Diff', lon[0,(i-1)] - lon[0,i], 'Lat Diff', lat[(i-1), 0] - lat[i, 0])


### SECOND METHOD
### TEST WITH USING THE LAT AND LON RASTER 
# a function to find the index of the point closest pt
# (in squared distance) to give lat/lon value.
ndat = len(lat_pt)
rasterData = np.zeros(ndat)
nearestXY = np.zeros((ndat,2))
def getRasterValue(raster,lats,lons,latpt,lonpt, ndat, nearestXY):
    for i in range(ndat):
        # find squared distance of every point on grid
        dist_sq = (lats-latpt[i])**2 + (lons-lonpt[i])**2
        # 1D index of minimum dist_sq element
        minindex_flattened = dist_sq.argmin()
        # Get 2D index for latvals and lonvals arrays from 1D index
        ymin, xmin = np.unravel_index(minindex_flattened, lat.shape)
        nearestXY[i,0] = ymin
        nearestXY[i,1] = xmin
        rasterData[i] = raster[ymin, xmin]

#startTime = time.time()
#getRasterValue(temp, lat, lon, lat_pt, lon_pt, ndat, nearestXY)
#endTime = time.time()
#execTime = endTime - startTime
#print('execTime getRasterValue FX', execTime)


## print results to compare two methods
#for i in range(ndat):
#    print('IndexRaster lat lon {}, {} ; Nearest lat lon {}, {}'.format(rowID[i], colID[i], nearestXY[i,0], nearestXY[i,1])) 

#print('type lat', type(lat), 'type temp', type(temp))

##  THIS IS UNLIKELY TO BE ROBUST
#@njit(parallel=True)
def find_pixel_coordinates(ndat, pixCoordPrange, rastShp, latitude_points, longitude_points, 
        latitude_raster, longitude_raster, tempVals, temp):

    height, width = rastShp

    for i in range(ndat):
#    for i in nb.prange(ndat):
        latitude = latitude_points[i]
        longitude = longitude_points[i]

        # Find the nearest latitude value
        lat_diff = np.abs(latitude_raster - latitude)
#        print('type', type(lat_diff))
        min_lat_index = np.argmin(lat_diff)
        # Find the nearest longitude value
        lon_diff = np.abs(longitude_raster - longitude)
        min_lon_index = np.argmin(lon_diff)

        min_lat_index = np.unravel_index(min_lat_index, rastShp)[0]
        min_lon_index = np.unravel_index(min_lon_index, rastShp)[1]

#        print('lat lon', min_lat_index, min_lon_index)

        pixCoordPrange[i] = min_lat_index, min_lon_index
#        tempVals[i] = temp[lat_i, lon_i]
#    return pixCoordPrange


## numba test
pixCoordPrange = np.zeros((ndat, 2), dtype=np.int32)
tempVals = np.zeros(ndat)

#startTime = time.time()
#find_pixel_coordinates(ndat, pixCoordPrange, lat.shape, 
#        lat_pt, lon_pt, lat.data, lon.data, tempVals, temp)
#endTime = time.time()
#execTime = endTime - startTime
#print('execTime', execTime)


## print results to compare two methods
#for i in range(ndat):
#    print('pixCoor lat lon {}, {} ; Nearest lat lon {}, {}'.format(pixCoordPrange[i,0], pixCoordPrange[i,1], nearestXY[i,0], nearestXY[i,1])) 



@njit(parallel = True)
def dist_pixel_coordinates(latitude_points, longitude_points, latitude_raster, longitude_raster, 
        temp, tempOut, ndat, rastShp, pixCoords, minDist):
    height, width = rastShp
    min_lat_index = 0
    min_lon_index = 0
    for i in prange(ndat):
        latitude = latitude_points[i]
        longitude = longitude_points[i]
        min_distance = np.inf
        for lat_index in range(height):
            for lon_index in range(width):
                lat_diff = latitude_raster[lat_index, lon_index] - latitude
                lon_diff = longitude_raster[lat_index, lon_index] - longitude
                distance = np.sqrt(lat_diff**2 + lon_diff**2)
                if distance < min_distance:
                    min_distance = distance
                    min_lat_index = lat_index
                    min_lon_index = lon_index
        pixCoords[i] = min_lat_index, min_lon_index
        tempOut[i] = temp[min_lat_index, min_lon_index]
        minDist[i] = min_distance


pixCoords = np.zeros((ndat, 2), dtype=np.int32)
tempOut = np.zeros(ndat)
rastShp = lat.shape
minDist = np.zeros(ndat)

startTime = time.time()
dist_pixel_coordinates(lat_pt, lon_pt, lat.data, lon.data, temp.data, tempOut, 
    ndat, rastShp, pixCoords, minDist)
endTime = time.time()
execTime = endTime - startTime
print('find pix coord execTime', execTime)
print('minDist', np.min(minDist), 'max', np.max(minDist))

## print results to compare two methods
#for i in range(ndat):
#    if i < 60:
#        print('Dist lat lon {}, {} ; Nearest lat lon {}, {}'.format(pixCoords[i,0], pixCoords[i,1], rowID[i], colID[i])) 



@njit(parallel=False)
def biased_walk(sst_raster, lat_raster, lon_raster, lat_pt, lon_pt, threshold, rastShp,
        sqSearch, newCoords, ndat, tempPts):
    rows, cols = rastShp
    nPixels = 0
    for i in range(ndat):
        min_distance = np.inf
        targetRow = 0
        targetCol = 0
        currentRow = 0
        currentCol = 0
        while True:
            nPixels += 1
            lat_diff = lat_raster[currentRow, currentCol] - lat_pt[i]
            lon_diff = lon_raster[currentRow, currentCol] - lon_pt[i]
            distance = np.sqrt(lat_diff ** 2 + lon_diff ** 2)
            if distance < min_distance:
                min_distance = distance
                targetRow = currentRow
                targetCol = currentCol
            if min_distance < threshold:
                # Search for a lower distance pixel in a 5x5 square around the target pixel
                for r in range(targetRow - sqSearch, targetRow + sqSearch + 1):
                    for c in range(targetCol - sqSearch, targetCol + sqSearch + 1):
                        if r >= 0 and r < rows and c >= 0 and c < cols:
                            new_distance = np.sqrt(
                                (lat_raster[r, c] - lat_pt[i]) ** 2 +
                                (lon_raster[r, c] - lon_pt[i]) ** 2)
                            if new_distance < min_distance:
                                min_distance = new_distance
                                targetRow = r
                                targetCol = c
                        nPixels += 1
                break
            AbsLatDiff = np.abs(lat_diff)
            AbsLonDiff = np.abs(lon_diff)
            if AbsLatDiff > AbsLonDiff:
                currentRow += 1
            else:
                currentCol += 1
            if currentRow < 0 or currentRow >= rows or currentCol < 0 or currentCol >= cols:
                break
        newCoords[i] = (targetRow, targetCol)
        tempPts[i] = sst_raster[targetRow, targetCol]
    return(nPixels)

newCoords = np.zeros((ndat, 2), dtype=np.int32)
tempPts = np.zeros(ndat)
threshold = np.max(minDist)*1.5
print('threshold', threshold)
#threshold = mquantiles(minDist, 0.05)
sqSearch = 5    #np.array([2, 3])
temp = temp.data
lat = lat.data
lon = lon.data
startTime = time.time()
print('shp raster', temp.data.shape)
nPixels = biased_walk(temp, lat, lon, lat_pt, lon_pt, threshold, rastShp,
            sqSearch, newCoords, ndat, tempPts)
endTime = time.time()
execTime = endTime - startTime
print('Biased Time', execTime)
print('mean pixels searched', nPixels/ndat)

## print results to compare two methods
cc = 0
for i in range(ndat):
#    if i < 360:
    if ((pixCoords[i,0] - newCoords[i, 0] != 0) or (pixCoords[i,1] - newCoords[i,1] != 0)):
        cc += 1
        print(cc, 'Dist lat lon {}, {} ; Biased lat lon {}, {}'.format(pixCoords[i,0], pixCoords[i,1], newCoords[i, 0], newCoords[i,1])) 
        
print('cc', cc)



def doGdalSampling(velData, maLat, maLon, ndat, rastBand):

    velOut = np.zeros(ndat)
    velCoords = np.zeros((ndat, 2), dtype = np.int16)

    ## READ IN VELOCITY DATA
    ds = gdal.Open(velData)
    transform = ds.GetGeoTransform()
    inv_transform = gdal.InvGeoTransform(transform) # creates one that goes coords -> col/row
    num_bands = ds.RasterCount    
    # Read the first band to get the array shape
    first_band = ds.GetRasterBand(1)
    band_shape = (first_band.YSize, first_band.XSize)

    # Create a 3D array to store all bands
    rasterData = np.zeros((num_bands, band_shape[0], band_shape[1]), dtype=float)

    # loop thru 5-day bands (layers in img)
    for n in range(num_bands):
        band = ds.GetRasterBand(n+1)
        band_array = band.ReadAsArray()    
        # Store the band array in the bands_array
        rasterData[n, :, :] = band_array


#    rasterData = ds.GetRasterBand(1).ReadAsArray()
#    print('shp rasterData', rasterData.shape)
    ## GET EXTENT AND RES OF RASTER
    xres = transform[1]
    yres = transform[5]
    x_min = transform[0]  # Minimum x-coordinate
    y_max = transform[3]  # Maximum y-coordinate
    x_max = x_min + xres * ds.RasterXSize  # Maximum x-coordinate
    y_min = y_max + yres * ds.RasterYSize  # Minimum y-coordinate
    print('Corners', x_min, y_max, x_max, y_min, 'res', xres, yres, 'Adj', x_max - (-yres * 0.3))

    print('rotation', inv_transform)
    ## ADJUST LONG FOR MAX OF 179.9
    adjustLon = x_max - (-yres * 0.3)
    maLon = np.where(maLon >= adjustLon, adjustLon, maLon)


    startTime = time.time()


    col = np.int32(inv_transform[0] + maLon * inv_transform[1] + maLat * inv_transform[2])
    row = np.int32(inv_transform[3] + maLon * inv_transform[4] + maLat * inv_transform[5])
    velCoords[:, 0] = row
    velCoords[:, 1] = col
    velOut = rasterData[rastBand, row, col]

    endTime = time.time()
    gdalExecTime = endTime - startTime
    print('gdalExecTime', gdalExecTime)

    return(velOut, velCoords)

ndat = len(maLon)
(velOut, velCoords) = doGdalSampling(velData, maLat, maLon, ndat, rastBand)

## print results to compare two methods
cc = 0
for i in range(60):
    print(cc, 'UVel {}; gdal row, col {}, {}, Band {}'.format(velOut[i], velCoords[i,0], velCoords[i,1], rastBand[i])) 
        
print('cc', cc)




