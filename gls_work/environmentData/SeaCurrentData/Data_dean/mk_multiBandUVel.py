#!/usr/bin/env python

import os
from osgeo import gdal


baseDir = '/home/dean/workfolder/projects/dean_adelie/gls_work/environmentData/SeaCurrentData/Data_dean'    
#seaCurrentData = 'eORCA025-NEB020o_5d_20160101_20161231_grid_V.nc'
#seaCurrentData = 'eORCA025-NEB020o_5d_20160101_20161231_grid_U.nc'
seaCurrentData = 'eORCA025-NEB020o_5d_20190101_20191231_grid_V.nc'
#seaCurrentData = 'eORCA025-NEB020o_5d_20190101_20191231_grid_U.nc'
#variable_name = 'vozocrtx'      ## grid_U (zonal velocity)
variable_name = 'vomecrty'      ## grid_V(meridional velocity).
vrtFile = 'Bird_V_Vel_WGS.vrt'

input_raster = os.path.join(baseDir, seaCurrentData)
output_vrt = os.path.join(baseDir, vrtFile)
print('outputvrt', output_vrt)

rastBands = [1, 61]

with open(output_vrt, 'w') as vrt_file:
    vrt_file.write('<VRTDataset rasterXSize="591" rasterYSize="321">\n')
    
    vrt_file.write('  <metadata domain="GEOLOCATION">\n')
    vrt_file.write('    <mdi key="X_DATASET">NETCDF:{0}:nav_lon</mdi>\n'.format(input_raster))
    vrt_file.write('    <mdi key="X_BAND">1</mdi>\n')
    vrt_file.write('    <mdi key="Y_DATASET">NETCDF:{0}:nav_lat</mdi>\n'.format(input_raster))
    vrt_file.write('    <mdi key="Y_BAND">1</mdi>\n')
    vrt_file.write('    <mdi key="PIXEL_OFFSET">0</mdi>\n')
    vrt_file.write('    <mdi key="LINE_OFFSET">0</mdi>\n')
    vrt_file.write('    <mdi key="PIXEL_STEP">1</mdi>\n')
    vrt_file.write('    <mdi key="LINE_STEP">1</mdi>\n')
    vrt_file.write('  </metadata>\n')
    
    for band_index in range(rastBands[0], rastBands[1] + 1):
        vrt_file.write('  <VRTRasterBand band="{0}" datatype="Float32">\n'.format(band_index))
        vrt_file.write('    <SimpleSource>\n')
        vrt_file.write('      <SourceFilename relativeToVRT="1">NETCDF:{0}:{1}</SourceFilename>\n'.format(input_raster, variable_name))
        vrt_file.write('      <SourceBand>{0}</SourceBand>\n'.format(band_index))
        vrt_file.write('      <SourceProperties RasterXSize="591" RasterYSize="321" DataType="Float32" BlockXSize="591" BlockYSize="1" />\n')
        vrt_file.write('      <SrcRect xOff="0" yOff="0" xSize="591" ySize="321" />\n')
        vrt_file.write('      <DstRect xOff="0" yOff="0" xSize="591" ySize="321" />\n')
        vrt_file.write('    </SimpleSource>\n')
        vrt_file.write('  </VRTRasterBand>\n')
    
    vrt_file.write('</VRTDataset>\n')
