#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import os
import datetime
import pylab as P
import matplotlib.ticker as ticker

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

class Params(object):
    def __init__(self):
        # set path and output file names 
        adeliepath = os.getenv('ADELIEPROJDIR', default = '.')
        # paths and data to read in
        self.aerialFile = os.path.join(adeliepath, 'Data', 'aerialSurvey_1981_2023.csv')
        self.graphFile = os.path.join(adeliepath, 'countGraph.png')
        self.droneFile = os.path.join(adeliepath, 'Data', 'droneSummary.csv')
        self.years = [1981, 2023]
        self.yearsArray = np.arange(self.years[0], (self.years[-1] + 1))
        self.nYears = len(self.yearsArray)
        self.nColonies = 7
        self.nMostColonies = 27


class ProcessData(object):
    def __init__(self, params):

        self.params = params
        self.readData()
        self.makeCountArray()
        self.getDroneCounts()
        self.getKeyColonies()
        self.plotKeyColonies()
#        self.getPercChange()
#        self.plotCount()

    def readData(self):
        """
        ## read in data
        """
        dt = ['S32']
        for i in range(self.params.nYears):
            dt.append('i8')
#        nTotalColony = len(dt)
        self.aerialDat = np.genfromtxt(self.params.aerialFile, delimiter=',', names=True,
            dtype=dt)
        self.colony = decodeBytes(self.aerialDat['Colony'][:self.params.nMostColonies], 
            self.params.nMostColonies)
#        print('colony', self.colony, len(self.colony))

        droneDat = np.genfromtxt(self.params.droneFile, delimiter=',', names=True,
            dtype = ['S32', 'S32', 'i8', np.uint32])
        
        droneColony = droneDat['colony']
        ndat = len(droneColony)
        self.droneColony = decodeBytes(droneColony, ndat)
        print('drone colony', self.droneColony)
        self.droneSub = droneDat['subcolony']
        self.droneYear = droneDat['year']
        self.droneCount = droneDat['AdultCount']
        self.droneColonies = np.unique(self.droneColony)
        self.yearsDrone = np.unique(self.droneYear)
        self.nYearsDrone = len(self.yearsDrone)

    def makeCountArray(self):
        self.count = np.zeros((self.params.nMostColonies, self.params.nYears), dtype = int)
        for i in range(self.params.nYears):
            colName = str(self.params.yearsArray[i]) 
            count_i = self.aerialDat[colName][:self.params.nMostColonies]
            self.count[:, i] = count_i

        self.count = np.where(self.count <= 100, np.nan, self.count)
#        print('count', self.count[:10, :5])
#        print('colony', self.colony)

    def getDroneCounts(self):        
        self.droneColCounts = {}
        
        for col in self.droneColonies:
            self.droneColCounts[col] = {}
            colMask = self.droneColony == col
            self.droneColCounts[col]['Years'] = [] 
            self.droneColCounts[col]['Counts'] = [] 
            for i in range(self.nYearsDrone):
                yr = self.yearsDrone[i]
                yrMask = self.droneYear == yr
                colYrMask = colMask & yrMask
#                print('col', col, 'yr', yr, 'colYrMask',colYrMask,'sum',np.sum(colYrMask))
                if np.sum(colYrMask) == 0:
                    continue
                sumCount = np.sum(self.droneCount[colYrMask])
#                print('col', col, 'yr', yr, 'sumCount', sumCount)
                self.droneColCounts[col]['Years'].append(yr)
                self.droneColCounts[col]['Counts'].append(sumCount)
            print('col', col, 'counts', self.droneColCounts[col])



    def getPercChange(self):
        self.pChange = (self.count[:, 1:] - self.count[:, :-1]) / self.count[:, :-1]
        self.nSites = np.count_nonzero(~np.isnan(self.pChange), axis=0)

        print('pchange', np.round(self.pChange[:7,-7:], 3))
        print('nsites', self.nSites[-7:])
        print('count', np.round(self.count[:7,-7:], 3))
        self.meanPChange = np.nanmean(self.pChange[:6], axis=0)
        self.sdPChange = np.nanstd(self.pChange[:6], axis = 0)


        print('mn and sd', self.meanPChange[-20:], 'sd',self.sdPChange[-12:])

        P.figure()
        nanMask = np.isnan(self.meanPChange)
        P.plot(self.params.yearsArray[1:], self.meanPChange, marker='o')
        P.plot(self.meanPChange[nanMask], marker='o',
            linestyle='none')
        P.hlines(y = 0, xmin=1982, xmax=2022)
        P.show() 

    def getKeyColonies(self):


        self.keyColonies = np.array(['Royds', 'Bird', 'Crozier', 'Adare'])
        self.royds = self.count[0, 4:]
        self.bird = np.sum(self.count[1:4, 4:], axis = 0)
        self.crozier = np.sum(self.count[4:6, 4:], axis = 0)

        hID = np.int16(np.where(self.colony == 'Cape Hallett'))[0]
        print('hID', hID)
        self.hallett = (self.count[hID, 4:]).flatten()
        
        self.adare = self.count[-1, 4:]
        print('bird', self.bird[-6:], 'adare', self.adare[-6:],
            'hallett', self.hallett[:10], self.hallett.shape)

        ## 1998 - 2018 limit
        self.yr1998_2018Mask = ((self.params.yearsArray[4:] >= 1998) & 
            (self.params.yearsArray[4:] <= 2018)) 
        ## 2015 - 2023 limit
        self.yr2015_2023Mask = ((self.params.yearsArray[4:] >= 2015) & 
            (self.params.yearsArray[4:] <= 2023)) 
        self.roydsStudy = self.royds[self.yr2015_2023Mask]
        self.birdStudy = self.bird[self.yr2015_2023Mask]
        self.crozierStudy = self.crozier[self.yr2015_2023Mask]
        self.yearStudy = self.params.yearsArray[4:][self.yr2015_2023Mask]


    def plotKeyColonies(self):
        P.figure(figsize=(10, 8))
        self.yearStudy = np.int32(self.yearStudy)
        ax1 = P.gca()
###        lns0 = ax1.plot(self.params.yearsArray[4:], self.royds, color='k', linewidth = 2,
###                marker='.', linestyle='-', label = 'Cape Royds')
###        lns1 = ax1.plot(self.params.yearsArray[4:], self.bird, color='b', linewidth = 2,
###                marker='.', linestyle='-', label = 'Cape Bird')
###        lns2 = ax1.plot(self.params.yearsArray[4:], self.crozier, color='r', linewidth = 2,
###                marker='.', linestyle='-', label = 'Cape Crozier')



        lns0 = ax1.plot(self.yearStudy, self.roydsStudy, color='k', linewidth = 2,
                marker='.', linestyle='-', label = 'Cape Royds')

        lns1 = ax1.plot(self.yearStudy, self.birdStudy, color='b', linewidth = 2,
                marker='.', linestyle='-', label = 'Cape Bird')
        lns2 = ax1.plot(self.yearStudy, self.crozierStudy, color='r', linewidth = 2,
                marker='.', linestyle='-', label = 'Cape Crozier')



        lns3 = ax1.plot(self.droneColCounts['royds']['Years'], 
            self.droneColCounts['royds']['Counts'], 'k*', ms = 6)
        lns4 = ax1.plot(self.droneColCounts['bird']['Years'], 
            self.droneColCounts['bird']['Counts'], 'b*', ms = 6)

        lns5 = ax1.plot(self.droneColCounts['crozier']['Years'], 
            self.droneColCounts['crozier']['Counts'], 'r*', ms = 6)



#        lns3 = ax1.plot(self.params.yearsArray[4:], self.adare, 'yo', ms = 10,
#                linestyle = '-', label = 'Cape Adare')
#        lns4 = ax1.plot(self.params.yearsArray[4:], self.hallett, 'ko', ms = 10,
#                linestyle = '-', label = 'Cape Hallett')

#        lns5 = ax1.plot(self.yearStudy, self.birdStudy, color='b', linewidth = 2,
#                marker='.', linestyle='-', label = 'Cape Bird')
#        lns6 = ax1.plot(self.yearStudy, self.crozierStudy, color='r', linewidth = 2,
#                marker='.', linestyle='-', label = 'Cape Crozier')

#        lns4 = ax1.plot(self.params.yearsArray, self.count[4], 'ro', ms = 10,
#                label = self.colony[4])
#        lns5 = ax1.plot(self.params.yearsArray, self.count[5], 'ko', ms = 10,
#                label = self.colony[5])
#        lns6 = ax1.plot(self.params.yearsArray, self.count[6], 'go', ms = 10,
#                label = self.colony[6])
#        lns = lns5 + lns6 
        lns = lns0 + lns1 + lns2        #+ lns4 + lns6
#        lns = lns0 + lns1 + lns2 + lns3 + lns4 + lns5 + lns6
        labs = [l.get_label() for l in lns]
        ax1.set_xlim(2014, 2024)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label1.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label1.set_fontsize(14)
        ax1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%d'))

        ax1.set_yscale('log')
        ax1.set_xlabel('Years', fontsize = 17)
        ax1.set_ylabel('Breeding pairs (log scale)', fontsize = 17)
#        ax1.set_ylim([np.nanmin(self.royds) - 1000, (np.nanmax(self.adare) + 50000)])
        ax1.legend(lns, labs, loc = 'upper left')
        P.savefig('rossStudyAll.png', format='png', dpi = 100)
#        P.savefig('aerialGraphLog.png', format='png', dpi = 600)
#        P.savefig(self.params.graphFile, format='png', dpi = 600)
        P.show() 



    def plotCount(self):
        P.figure(figsize=(10, 8))
        
        ax1 = P.gca()
        lns0 = ax1.plot(self.params.yearsArray, self.count[0], color='k', linewidth = 2,
                label = self.colony[0])
        lns1 = ax1.plot(self.params.yearsArray, self.count[1], color='b', linewidth = 2,
                label = self.colony[1])
        lns2 = ax1.plot(self.params.yearsArray, self.count[2], color='r', linewidth = 2,
                label = self.colony[2])
#        lns3 = ax1.plot(self.params.yearsArray, self.count[3], 'yo', ms = 10,
#                label = self.colony[3])
#        lns4 = ax1.plot(self.params.yearsArray, self.count[4], 'ro', ms = 10,
#                label = self.colony[4])
#        lns5 = ax1.plot(self.params.yearsArray, self.count[5], 'ko', ms = 10,
#                label = self.colony[5])
#        lns6 = ax1.plot(self.params.yearsArray, self.count[6], 'go', ms = 10,
#                label = self.colony[6])
        lns = lns0 + lns1 + lns2
#        lns = lns0 + lns1 + lns2 + lns4 + lns6
#        lns = lns0 + lns1 + lns2 + lns3 + lns4 + lns5 + lns6
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_yscale('log')
        ax1.set_xlabel('Years', fontsize = 17)
        ax1.set_ylabel('Breed pairs (log scale)', fontsize = 17)
        ax1.set_ylim([np.nanmin(self.count[0]) - 1000, (np.nanmax(self.count[6]) + 50000)])
        ax1.legend(lns, labs, loc = 'upper left')
        P.savefig('aerialGraph_RossLog.png', format='png', dpi = 600)
#        P.savefig('aerialGraphLog.png', format='png', dpi = 600)
#        P.savefig(self.params.graphFile, format='png', dpi = 600)
        P.show() 









    def plotGrowth(self):
                        
        self.dateArray = np.array(self.dates)
        self.getXTicks()
        nplots = self.nNest
        plotIndx = 0
        nfigures = np.int(np.ceil(nplots/4.0))
        for i in range(nfigures):
            P.figure(figsize=(10, 8))
            lastFigure = i == (nfigures - 1)
            for j in range(4):
                P.subplot(2,2,j+1)
                if plotIndx < nplots:
                    nest_j = self.uNest[plotIndx]
                    nestMask = (self.nest == nest_j)
                    uchick = np.unique(self.chick[nestMask])
                    if np.isscalar(uchick):
                        nchick = 1
                    else:
                        nchick = len(uchick)
                    for k in range(nchick):
                        maskChick = (self.chick == uchick[k])
                        maskNestChick = nestMask & maskChick
                        date_jk = np.unique(self.dateArray[maskNestChick])
                        nDates = len(date_jk)
                        mass_jk = np.zeros(nDates)
                        ax1 = P.gca()
                        for l in range(nDates):
                            dateMask = self.dateArray == date_jk[l]
                            nestChickDateMask = maskNestChick & dateMask
                            mass_jk[l] = self.mass[nestChickDateMask]
                        if k == 0:
                            lns1 = ax1.plot(date_jk, mass_jk, 'k-o', linewidth=5)
                        else:
                            lns1 = ax1.plot(date_jk, mass_jk, 'r-o', linewidth=3)                                            
                        for tick in ax1.xaxis.get_major_ticks():
                            tick.label.set_fontsize(8)
                        for tick in ax1.yaxis.get_major_ticks():
                            tick.label.set_fontsize(10)
                        if (j < 2):
                            ax1.set_xlabel('')
                        else:
                            ax1.set_xlabel('Months', fontsize = 12)
                        xmin = np.min(self.dateArray - datetime.timedelta(1))
                        xmax = np.max(self.dateArray + datetime.timedelta(1))
                        ymax = np.max(self.mass + 100)
                        ymin = -20.0
                        if (j==0) | (j==2):
                            ax1.set_ylabel('Mass (g)', fontsize = 12)
                        else:
                            ax1.set_ylabel('')
                        ax1.set_ylim([ymin, ymax])
#                        ax1.set_xlim([xmin, xmax])
                        P.xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
                                self.dateAxis[-1]])
#                        ax1.set_xticks([self.dateAxis])
                    P.title('nest' + str(nest_j))
                plotIndx = plotIndx + 1
            P.show(block=lastFigure)

            
    def getXTicks(self):
        startdate = np.min(self.dateArray)
        enddate = np.max(self.dateArray)
        self.dateAxis = np.empty(4, dtype = datetime.date)
        self.dateAxis[0] = startdate - datetime.timedelta(1)
        diff = enddate - startdate
        totaldays = diff.days
        self.dateAxis[1] = startdate + datetime.timedelta(int(totaldays/3))
        self.dateAxis[2] = startdate + datetime.timedelta(int(totaldays*2.0/3.0))
        self.dateAxis[-1] = enddate
        print('dateAxis', self.dateAxis)

def main():

    params = Params()
    processdata = ProcessData(params)


if __name__ == '__main__':
    main()




