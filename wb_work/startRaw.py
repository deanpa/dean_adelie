#!/usr/bin/env python

import mainRaw
import paramsRaw

params = paramsRaw.Params()
mainRaw.main(params)
