#!/usr/bin/env python

import os
import datetime
import numpy as np
import basicsRaw


class Event(object):
    def __init__(self, params):
        self.params = params
        self.readTagData()
        self.makeMassDictionaries()
        self.statesAndStorage()
        self.loopWBData()
        self.writeCSV()

    def readTagData(self):
        """
        read in tag data
        """
        self.tagDat = np.genfromtxt(self.params.tagFile, delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'S10', 'i8', 'S10', 'i8', 'i8', 'S16', 'S16', 'S10'])
        self.tagYear = self.tagDat['year']
        self.tagMonth = self.tagDat['month']
        self.tagDay = self.tagDat['day']
        self.nTags = len(self.tagYear)
        self.rfid = basicsRaw.decodeBytes(self.tagDat['rfid'], self.nTags)
        self.rfidInteger = np.arange(self.nTags, dtype = int)
        self.rfid2 = basicsRaw.decodeBytes(self.tagDat['rfid2'], self.nTags)
        self.nest = self.tagDat['nest']
        self.bird = self.tagDat['bird']


        print('len rfid', len(self.rfid), len(np.unique(self.rfid)))



        nBird = len(self.bird)
        self.nestBird = np.empty(nBird, dtype = 'U12')
        for i in range(nBird):
            nbTmp = str(self.nest[i]) + 'A' + str(self.bird[i])
            self.nestBird[i] = nbTmp
#        print('array nestBird', len(np.unique(self.nestBird)))

    def makeMassDictionaries(self):
        """
        MAKE DICTIONARIES TO STORE MASS DATA FOR INDIVIDUALS BETWEEN TIMES ON WB
        """
        uBird = np.unique(self.nestBird)
        self.individMassDict = {str(uBird[0]) : []}
        self.individLastReadDict = {str(uBird[0]) : datetime.datetime(2015, 12, 23, 4, 30, 30)}

        rfid = str(np.squeeze(self.rfid[self.nestBird == uBird[0]]))
        self.rfidBirdDict = {rfid : str(uBird[0])}
        ## LOOP THROUGH BIRDS TO MAKE DICTIONARY
        for bb in range(1, len(uBird)):
            uBird_bb = str(uBird[bb])
            self.individMassDict.update({uBird_bb : []})
            self.individLastReadDict.update({uBird_bb : datetime.datetime(2015, 12, 23, 4, 30, 30)})
            

            rfid = str(np.squeeze(self.rfid[self.nestBird == uBird[bb]]))
            self.rfidBirdDict.update({rfid : uBird_bb})

#            print('uBird', uBird[bb], 'typebird', type(uBird[bb]), 'rfid', rfid, type(rfid))
#        print('uBird', uBird, uBird[0], type(uBird[0])) #, 'Dict', self.individLastReadDict)
#        print('rfidBirdDict', self.rfidBirdDict)

    def statesAndStorage(self):
        ## STORAGE LISTS
#        self.MASS = []
#        self.timeMove = []
#        self.direction = []
#        self.individual = []
        self.individualTmp = None
        self.rfidTmp = None
        self.massTmp = []
#        self.startList = []                     ## START LOCATION
#        self.endList = []                       ## END LOCATION
        self.opticStartTime = None
        self.opticEndTime = None
        self.meanZ = []
        self.previousCode = None
        self.zCount = 0
        self.ongoingMeanZ = 0.0
        self.zSumTmp = 0.0
#        self.fileList = []
#        self.noteList = []
        self.noteTmp = None
        ## POINT MASS ARRAYS: FOR ADDITIONAL DATA ON BIRD MASS EVEN WHEN DON'T CROSS
        self.pointID = []
        self.pointRFID = []
        self.pointStart = []
        self.pointEnd = []
        self.pointMass = []
        self.pointMedian = []
        self.pointTime = []
        self.pointFile = []
        self.pointN = []
        self.pointSTD = []
        self.pointNote = []
        ## STARTING STATES
        self.startLocation = None
        self.endLocation = None
        self.collectMass = False
        self.opticEvent = False
        self.individualIdentified = False
        ## LOCATION ON WB: '01' = IN, AND '10' = OUT; '11' clear optic; '00' both opts triggered.
        self.Locations = np.array(['01', '10'])   


    def loopWBData(self):
        """
        go thru WB data to find tagged birds
        """
        for i in range(self.params.fileRange[0], self.params.fileRange[1]):
            ## REMOVE OR SKIP PROBLEM FILE OF '52' in 2019-2020
            if (i == 52) & (self.params.year == 'year2019_2020'):    
                continue
            ## ELSE CARRY ON
#            print('file', i)
            if i < 100:
                fname = 'scale00' + str(i) + '.txt'
            elif (i>99) & (i<1000):
                fname = 'scale0' + str(i) + '.txt'
            elif i>999:
                fname = 'scale' + str(i) + '.txt'
            self.scaleFPath = os.path.join(self.params.dataPath, fname)        
            self.readScaleOutput(i)


    def readScaleOutput(self, i):
        cc = -1
        ## LOOP TO GET RFID AND TIMESTAMPS
        for line in open(self.scaleFPath):

            cc += 1
        
            line = line.strip('\r\n')
            timeStamp, data = line.split(',')
###            counter = timeStamp[-3:] # save?
            timeStamp = timeStamp[:-4] # strptime can't ignore stuff
            timeStamp = datetime.datetime.strptime(timeStamp, '%y:%m:%d:%H:%M:%S')
#            timeStamp = datetime.datetime.strptime(timeStamp, '%y:%m:%d:%H:%M:%S:%f')

            
#            if (i == 349) & (cc < 5000):
#                print('i', i, 'timeStamp', line)    



            ## GET DATA CODE FOR LINE
            code_i = data[0]
            ## IF SCALE CODE 'S', DON'T DO ANYTHING
            if code_i == 'S':
                continue
            ## OPTIC DETECTION, GET OPTIC CODE  ############################################
            if code_i == 'O':                   ############################################    
                optic_i = data[1:]
                ## OPTIC SENSOR ON ONE SIDE TRIGGERED - START OPTIC EVENT
                ## In or out optics triggered for first time        
                if ((optic_i == '01') | (optic_i == '10')) & (self.opticEvent == False):
                    self.opticEvent = True           ## RULING OUT '00' and keep True if '11'
                    self.startLocation = optic_i
                    self.endLocation = self.Locations[self.Locations != optic_i]
                    ## START TIME:
                    self.opticStartTime = timeStamp
                    ## DON'T COLLECT DATA UNTIL OPTIC == '11'
                    self.collectMass = False
                    self.massTmp = []   
                ## In or out optics triggered for second time        
                elif ((optic_i == '01') | (optic_i == '10')) & (self.opticEvent == True):
                    ## ID UNKNOWN
                    if (self.individualIdentified == False):
                        self.collectMass = False             ## STOP COLLECTING AND START AGAIN
                        self.massTmp = []                    ## CLEAR TEMP MASS STORAGE LIST
                        self.startLocation = optic_i         ## MAKE THIS THE START LOCATION
                        self.endLocation = self.Locations[self.Locations != optic_i]
                        ## NEW START TIME:
                        self.opticStartTime = timeStamp
                    ## INDIVIDUAL PREVIOUSLY IDENTIFIED WITH THIS OPTIC EVENT
                    ## THEREFORE END THE EVENT AND GET DATA
                    elif (self.individualIdentified == True):


                        ## OPTIC TIME THRESHOLD
                        timeDiff = timeStamp - self.opticStartTime
                        if timeDiff.seconds <= self.params.opticTimeThreshold:
                            ## BACK TO START
                            if self.startLocation == optic_i:
                                if optic_i == '01':
                                    self.pointNote.append('BackToInside')
                                else:
                                    self.pointNote.append('BackToOutside')
                                ## IF TIME SINCE LAST MASS MEASURE FOR ID < X, 
                                ## APPEND NEW MASS MEASURES TO NESTBIRD DICTIONARY
#                                if len(self.individLastReadDict[nestBird]) == 0:
#                                    self.individLastReadDict[nestBird] = self.ptTimeTmp
#                                    self.individMassDict[nestBird] = self.massTmp
#                                else:

#                                print('dict nestbird', nestBird.shape, type(nestBird))

###                                strNestBird = str(nestBird.squeeze())
#                                print('keys', self.individLastReadDict.keys())

#                                print('self.nestBirdOptic', self.nestBirdOptic)



                                timeDiffMass = ((self.ptTimeTmp - 
                                    self.individLastReadDict[self.nestBirdOptic]).seconds)
#                                    self.individLastReadDict[strNestBird]).seconds)
                                ## MEET TIME THRESHOLD:
                                if timeDiffMass <= (self.params.maxTimeInterRead * 3600):
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
#                                    self.individLastReadDict[strNestBird] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic].extend(self.massTmp)
#                                    self.individMassDict[strNestBird].extend(self.massTmp)
                                ## DO NOT MEET TIME THRESHOLD: CLEAR OLD MASS, START NEW
                                else:
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic] = self.massTmp

                            ## CROSSED THE BRIDGE !!!
                            elif self.endLocation == optic_i:
                                if optic_i == '01':
                                    self.pointNote.append('CrossToInside')
                                else:
                                    self.pointNote.append('CrossToOutside')

                                ## IF TIME SINCE LAST MASS MEASURE FOR ID < X, 
                                ## APPEND NEW MASS MEASURES TO NESTBIRD DICTIONARY
#                                if len(self.individLastReadDict[nestBird]) == 0:
#                                    self.individLastReadDict[nestBird] = self.ptTimeTmp
#                                    self.individMassDict[nestBird] = self.massTmp
#                                else:
                                timeDiffMass = ((self.ptTimeTmp - 
                                    self.individLastReadDict[self.nestBirdOptic]).seconds)
                                if timeDiffMass <= (self.params.maxTimeInterRead * 3600):
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic].extend(self.massTmp)
                                ## DO NOT MEET TIME THRESHOLD: CLEAR OLD MASS, START NEW
                                else:
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic] = self.massTmp

                            ## IF ID APPEARED BEFORE OPTIC EVENT
                            elif self.startLocation == '99':
                                self.endLocation = optic_i
                                self.pointNote.append('StartUnsure')

                                ## IF TIME SINCE LAST MASS MEASURE FOR ID < X, 
                                ## APPEND NEW MASS MEASURES TO NESTBIRD DICTIONARY
#                                if len(self.individLastReadDict[nestBird]) == 0:
#                                    self.individLastReadDict[nestBird] = self.ptTimeTmp
#                                    self.individMassDict[nestBird] = self.massTmp
#                                else:
                                timeDiffMass = ((self.ptTimeTmp - 
                                    self.individLastReadDict[self.nestBirdOptic]).seconds)
                                if timeDiffMass <= (self.params.maxTimeInterRead * 3600):
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic].extend(self.massTmp)
                                ## DO NOT MEET TIME THRESHOLD: CLEAR OLD MASS, START NEW
                                else:
                                    self.individLastReadDict[self.nestBirdOptic] = self.ptTimeTmp
                                    self.individMassDict[self.nestBirdOptic] = self.massTmp

                            ## TODO: FIND notes PROBLEM
                            else:
                                self.pointNote.append('?????')

                            ## POPULATE ARRAYS
                            self.pointID.append(self.individualTmp)
                            self.pointRFID.append(self.rfidTmp)
                            self.pointStart.append(self.startLocation)
                            self.pointEnd.append(optic_i)
                            self.getMeanMass()
                            self.pointTime.append(self.ptTimeTmp)
                            self.pointFile.append(i)

#                            print('i', i, 'time', self.ptTimeTmp, 'ID', self.individualTmp[0], 
#                                'opticEvent', self.opticEvent,
#                                'optic_i', optic_i, 'start', self.startLocation, 
#                                'end', optic_i, 'note', self.pointNote[-1])

                        ## START A NEW EVENT IN CASE BIRD GOES BACK TO HOOP
                        self.collectMass = False             ## COLLECT NEW MASS DATA WITH '11'
                        self.massTmp = []                    ## CLEAR MASS FOR NEW EVENT


                        self.individualIdentified = False    ## NEW EVENT 


                        self.startLocation = optic_i
                        self.endLocation = self.Locations[self.Locations != optic_i]
                        self.ptTimeTmp = None
                        ## NEW START TIME:
                        self.opticStartTime = timeStamp


                ## (2) DETECT OPTIC CODE == '00' - BOTH OPTICS TRIGGERED... START AGAIN
                elif (optic_i == '00'):
                    self.opticEvent = False
                    self.collectMass = False
                    self.individualIdentified = False
                    self.massTmp = []
                    self.startLocation = None
                    self.endLocation = None
                    self.noteTmp = None
                    self.ptTimeTmp = None
                    self.opticStartTime = None

                ## (3) OPTIC SENSOR UN-BLOCKED SO READY TO COLLECT DATA
                elif (self.opticEvent == True) & (optic_i == '11'):
                    self.collectMass = True
                
            ##########################################################################
            ## COLLECT MASS DATA
            elif (code_i == 'W') & (self.collectMass == True):
                weight_i = [int(x) for x in data[1:].split()]
                self.massTmp.append(weight_i[-2])
            ##########################################################################
            ## RFID AND RFID IN INTEGER FORM
            elif (code_i == 'I'):
                # RFID tag
                rfidCode = data[7:18]

                ## REMOVE BIRD FROM 2018-2019 FROM WB DATA IN 2019-2020
                if (self.params.year == 'year2019_2020') & (rfidCode == '711*001*877'):
                    continue

                ## ELSE CARRY ON
###                rfidMask = self.rfid == rfidCode

#                print('rfid', type(rfidMask), rfidMask, 'nestbird', type(self.nestBird))


#                rfidInt = self.rfidInteger[rfidMask]
#                print('nestBird masked', self.nestBird[rfidMask].shape)
                self.nestBirdOptic = self.rfidBirdDict[rfidCode]
###                nestBird = (self.nestBird[rfidMask])
#                nestBird = (self.nestBird[rfidMask]).squeeze

#                print('self.nestBirdOptic', self.nestBirdOptic, 'time', timeStamp)


                ## (1) IDENTIFY NEW INDIVIDUAL
                if ((self.opticEvent == True) & (self.individualIdentified == False) &
                        (self.collectMass == True)):

                    self.individualTmp = self.nestBirdOptic
                    self.rfidTmp = rfidCode
                    self.individualIdentified = True
                    self.ptTimeTmp = timeStamp
                    
    

                ## (2) INDIVIDUAL ALREADY IDENTIFIED 
                elif ((self.opticEvent == True) & (self.individualIdentified == True) &
                        (self.collectMass == True)):
                    ## BUT HAVE SECOND ID
                    if (self.individualTmp != self.nestBirdOptic):
                        ## HAVE TO ELIMINATE THIS OPTIC EVENT
                        self.opticEvent = False
                        self.collectMass = False
                        self.individualIdentified = False                        

                    ## (2) SECOND READING OF SAME ID. KEEP READING WEIGHTS, BUT UP-DATE TIME
                    elif (self.individualTmp == self.nestBirdOptic):
                        self.ptTimeTmp = timeStamp

                ## (3) HAVE CODE I AFTER AN S OR E EVENT AND NO OPTIC HAS BEEN 
                ## TRIGGERED SINCE THE S OR E EVENT? WILL HAVE A LOT OF MASS DATA FOR THIS ID.
                elif ((self.opticEvent == False) & (self.individualIdentified == False) &
                        (self.collectMass == False)):
                    self.individualTmp = self.nestBirdOptic
                    self.rfidTmp = rfidCode
                    self.individualIdentified = True
                    self.ptTimeTmp = timeStamp
                    ## COLLECT SUBSEQUENT MASS DATA JUST IN CASE GET AN END OPTIC EVENT..
                    self.collectMass = True
                    self.opticEvent = True
                    ## UNKNOWN START LOCATION;
                    self.startLocation = '99'
                    ## START TIME:
                    self.opticStartTime = timeStamp
#                    self.noteTmp = 'No_startLoc'    # UNKNOWN START LOCATION

                ## HAVE CODE I AFTER AN OPTIC EVENT BUT NO '11' OPTIC TO START MASS COLLECTION 
                ## WILL HAVE MASS DATA FOR THIS ID, BUT NOT SURE OF START LOC BECAUSE ITS INITIAL
                ## OPTIC MAY HAVE BEEN TRIGGERED BY ANOTHER ANIMAL.
                elif ((self.opticEvent == True) & (self.collectMass == False) & 
                        (self.individualIdentified == False)):
                    self.individualTmp = self.nestBirdOptic
                    self.rfidTmp = rfidCode
                    self.individualIdentified = True
                    self.ptTimeTmp = timeStamp
                    ## COLLECT SUBSEQUENT MASS DATA JUST IN CASE GET AN END OPTIC EVENT..
                    self.collectMass = True
                    ## START TIME:
                    self.opticStartTime = timeStamp

                    timeDiff = timeStamp - self.opticStartTime
                    if timeDiff.seconds > self.params.opticTimeThreshold:
                        ## UNKNOWN START LOCATION
                        self.startLocation = '99'
                    ## ELSE: START LOCATION IS PREVIOUS STARTLOCATION
#                    self.startLocation = '99'

            
            ## TODO: WORK ON E EVENTS.
            ## IF END OF EVENT - JUST CONTINUE
            elif code_i == 'E':
                continue



#            ## END OF EVENT WITH INDIVIDUAL STILL ON WB, SAVE MASS DATA
#            elif (code_i == 'E') & (self.individualIdentified == True):
#                ## POPULATE POINT MASS ARRAYS
#                self.pointID.append(self.individualTmp[0])
#                self.pointStart.append(self.startLocation)
#                self.pointEnd.append('-99')
#                self.getMeanMass()
#                self.pointTime.append(self.ptTimeTmp)
#                self.pointFile.append(i)
#                self.pointNote.append('On_Bridge')      
#                ## CLEAR STATES AND SUB-LIST
#                self.massTmp = []   
#                self.opticEvent = False
#                self.collectMass = False
#                self.individualIdentified = False
#                self.startLocation = None
#                self.endLocation = None
#                self.noteTmp = None
#                self.ptTimeTmp = None
#                self.opticStartTime = None


            ## Z SCALE TARE CODE - JUST CONTINUE (BELOW ALTERNATIVE TARING OPERATION)
            if (code_i == 'Z'):
                continue

#            if (code_i == 'Z'):
#                ## GET Z TARE MASS
#                weight_i = [int(x) for x in data[1:].split()]
#                zMassTmp = weight_i[-2]
#                ## IF FIRST Z CODE IN AWHILE, CLEAR AND UPDATE Z-TARE DATA
#                if (self.previousCode != 'Z'):
#                    self.ongoingMeanZ = 0.0
#                    self.zSumTmp = zMassTmp
#                    self.zCount = 1        
#                ## SUBSEQUENT Z VALUES: UPDATE THE MEAN Z-TARE VALUE                                
#                if (self.previousCode == 'Z'):        ## CARRY ON TALLYING MASS FOR TARE
#                    self.zCount +=1
#                    self.zSumTmp += zMassTmp
#                    self.ongoingMeanZ = (self.zSumTmp / self.zCount)
#            ## UPDATE THE PREVIOUS CODE FOR NEXT LINE OF DATA
#            self.previousCode = code_i

    def getMeanMass(self):
        mass_i = np.array(self.individMassDict[self.nestBirdOptic])
#        mass_i = np.array(self.massTmp)
        maskMass = (mass_i > self.params.massCutOff[0]) & (mass_i < self.params.massCutOff[1])
        filtMass_i = mass_i[maskMass]
        if len(filtMass_i) > 0:
            filtMeanMass_i = np.nanmean(filtMass_i) 
            self.pointMass.append(filtMeanMass_i)
            nMass = len(filtMass_i)
            self.pointN.append(nMass)
            med_i = np.nanmedian(filtMass_i)
            self.pointMedian.append(med_i)
            stdMass = np.nanstd(filtMass_i)
            self.pointSTD.append(stdMass)
        else:
            self.pointMass.append(np.nan)
            self.pointN.append(np.nan)
            self.pointSTD.append(np.nan)
            self.pointMedian.append(np.nan)

            

    def writeCSV(self):
        """
        ## CREATE STRUCTURAL ARRAY AND WRITE *.CSV TO DIRECTORY
        """

        print('NestBird', self.pointID[:20], 'indTmp', self.individualTmp)

        lenPt = len(self.pointFile)
        # create new structured array with columns of different types
        structured = np.empty((lenPt,), dtype=[('fid', np.float), ('nestBird', 'U12'),
                    ('rfid', 'U32'), ('time', 'U32'), ('start', 'U12'), 
                    ('end', 'U12'), ('meanMass', np.float), ('n', np.float), 
                    ('med', np.float), ('stdev', np.float), ('note', 'U16')])
        # copy data over
        structured['fid'] = np.array(self.pointFile)
        structured['nestBird'] = self.pointID
        structured['rfid'] = self.pointRFID
        structured['time'] = self.pointTime
        structured['start'] = self.pointStart

        structured['end'] = self.pointEnd
        structured['meanMass'] = self.pointMass
        structured['n'] = self.pointN
        structured['med'] = self.pointMedian
        structured['stdev'] = self.pointSTD
        structured['note'] = self.pointNote

        np.savetxt(self.params.outPointFname, structured, fmt=['%.0f', '%s', '%s', 
            '%s', '%s', '%s', '%.0f', '%.0f', '%.0f', '%.0f', '%s'], comments = '', 
            delimiter = ',',
            header='fid, nestBird, rfid, time, start, end, meanMass, n, median, stdev, note')

