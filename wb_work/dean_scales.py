#!/usr/bin/env python

import sys
import datetime
import sys
import numpy as np
import os
import pickle

class Params(object):
    def __init__(self):
        # set path and output file names 
        adeliepath = os.getenv('ADELIEPROJDIR', default = '.')
        # paths and data to read in
        self.WBFile = os.path.join(adeliepath, 'data', 'scale0870.txt')
        # filename to pickle processed data 
        self.outProcessedFname = os.path.join(adeliepath, 'data', 'wbProcessedData.pkl')


class Event(object):
    RFID = None
    OpticsList = [] # tuples of (timeStamp, ID)
    WeightsList = [] # tuples of (timeStamp, data)

    def __repr__(self):
        "For Debugging"
        return 'RFID = {} Optics = {} Weights = {}'.format(
                self.RFID, self.OpticsList, self.WeightsList)

def readScaleOutput(fname):

    eventList = []

    currentEvent = Event()
    for line in open(fname):
        line = line.strip('\r\n')
        timeStamp, data = line.split(',')
        counter = timeStamp[-3:] # save?
        timeStamp = timeStamp[:-4] # strptime can't ignore stuff
        timeStamp = datetime.datetime.strptime(timeStamp, '%y:%m:%d:%H:%M:%S')
#        timeStamp = datetime.datetime.strptime(timeStamp, '%y:%m:%d:%H:%M:%S:%f')

        code = data[0]
        
        if code == 'W':
            # weights
            # TODO: need more info about what these numbers mean
            weightData = [int(x) for x in data[1:].split()]
            currentEvent.WeightsList.append((timeStamp, weightData))
#            print('weight', weightData[-2])


        elif code == 'O':
            # an optic sensor
            opticCode = data[1:]
            currentEvent.OpticsList.append((timeStamp, opticCode))
            print('opticcode', opticCode)

        elif code == 'I':
            # RFID tag
            rfidCode = data[7:18]
            if currentEvent.RFID is not None and currentEvent.RFID != rfidCode:
                raise ValueError('Already have an RFID for this event ' + currentEvent.RFID + ' ' + rfidCode)
            currentEvent.RFID = rfidCode

        elif code == 'E':
            # end of event
            # stash event object and start a new one
            eventList.append(currentEvent)
            currentEvent = Event()
            
    return eventList

if __name__ == '__main__':
    fname = sys.argv[1]
    events = readScaleOutput(fname)

#    print('len OpticsList', events)
#    print('len wtList', len(events.WeightsList))

#    for event in events:
#        print(event)
#        print(event.RFID)
#        print(event.WeightsList)        