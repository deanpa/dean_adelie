#!/usr/bin/env python

import numpy as np 




def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)




class RawMassPickle(object):
    def __init__(self, events):
        """
        ## add elements to pickle
        ## save to pickle so don't have to re-process previous steps repeatedly
        """
#        self.direction = events.direction       ## STORE END LOCATION (DIRECTION OF MOVE)
#        self.MASS = events.MASS
#        self.timeMove = events.timeMove
#        self.individual = events.individual
#        self.startList = events.startList
#        self.endList = events.endList
#        self.meanZ = events.meanZ
#        self.fileList = events.fileList
        self.FilePathName = events.params.outArrayFname 
#        self.noteList = events.noteList

        self.pointID = events.pointID
        self.pointStart = events.pointStart
        self.pointEnd = events.pointEnd
        self.pointMass = events.pointMass
        self.pointN = events.pointN
        self.pointSTD = events.pointSTD
        self.pointTime = events.pointTime
        self.pointFile = events.pointFile
        self.pointNote = events.pointNote

        ## NEST-BIRD INFORMATION
        self.tagYear = events.tagYear
        self.tagMonth = events.tagMonth
        self.tagDay = events.tagDay
        self.nTags = events.nTags
        self.rfid = events.rfid
        self.rfidInteger = events.rfidInteger      #np.arange(self.nTags, dtype = int)
        self.rfid2 = events.rfid2
        self.nest = events.nest
        self.bird = events.bird


