#!/usr/bin/env python

import pickle
import basicsRaw
import accumulateMassFX
#import functionRaw


########            Main function
#######
def main(params):

    print('File Range:', params.fileRange)
    
    
    eventdata = accumulateMassFX.Event(params)
#    eventdata = functionRaw.Event(params)



    massevents = basicsRaw.RawMassPickle(eventdata)
    ## SAVE PICKLE OF LISTS OF RFID, OPTICS AND WEIGHTS FROM RAW DATA
    fileobj = open(params.outArrayFname , 'wb')
    pickle.dump(massevents, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

