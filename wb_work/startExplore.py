#!/usr/bin/env python

import mainExplore
import paramsRaw

params = paramsRaw.Params()
mainExplore.main(params)
