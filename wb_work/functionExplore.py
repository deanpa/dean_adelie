#!/usr/bin/env python

import datetime
import numpy as np
import os
import pickle
import pylab as P
#import raw_wb
from scipy.stats.mstats import mquantiles
import basicsRaw


class ExploreData(object):
    def __init__(self, massdata, params):
        """
        ## add elements to pickle 
        """
        
        print('File Range:', params.fileRange)

        self.transferData(massdata)
#        self.simpleFX()
        self.getMeanMass(params)
        self.getPointMeanMass(params)
        self.plotMass()

    def transferData(self, massdata):
        """
        ## MOVE DATA INTO SELF
        """
        self.MASS = massdata.MASS
        self.timeMove = massdata.timeMove
        self.timeArray = np.array(self.timeMove)
        self.individual = massdata.individual
        self.idArray = np.array(self.individual)
        self.startList = massdata.startList
        self.endList = massdata.endList
        self.meanZ = massdata.meanZ
        self.fileList = massdata.fileList
        self.FilePathName = massdata.FilePathName 
        self.noteList = massdata.noteList
        ## POINT MASS DATA
        self.pointID = massdata.pointID
        self.pointStart = massdata.pointStart
        self.pointMass = massdata.pointMass
        self.pointTime = massdata.pointTime
        self.pointFile = massdata.pointFile
        ## NEST-BIRD INFORMATION
        self.tagYear = massdata.tagYear
        self.tagMonth = massdata.tagMonth
        self.tagDay = massdata.tagDay
        self.nTags = massdata.nTags
        self.rfid = massdata.rfid
        self.rfidInteger = massdata.rfidInteger      #np.arange(self.nTags, dtype = int)
        self.rfid2 = massdata.rfid2
        self.nest = massdata.nest
        self.bird = massdata.bird
        self.uNest = np.unique(self.nest)
        self.nNest = len(self.uNest)

    def simpleFX(self):
        print('fileList', self.fileList)
        for i in range(len(self.fileList)):
            mass_i = np.array(self.MASS[i])
            maskMass = (mass_i > 3000) & (mass_i < 5500)
            filtMass_i = mass_i[maskMass]
            if (len(mass_i) ==0):
                meanMass = 0
            else:
                meanMass = np.mean(mass_i)
            if (len(filtMass_i) == 0):
                filtMeanMass_i = 0
            else:
                filtMeanMass_i = np.mean(filtMass_i)
            nest = self.nest[self.rfidInteger == self.individual[i]]
            bird = self.bird[self.rfidInteger == self.individual[i]]
            if (nest == 7) & (bird == 1):
                print('file', self.fileList[i], 'time', self.timeMove[i],
                    'id', self.individual[i], 'nest', nest, 'bird', bird, 
                    'st', self.startList[i], 'end',
                    self.endList[i], 'filtMeanMass', filtMeanMass_i, 'meanMass', meanMass, 
                    'z', self.meanZ[i])

#            print('file', self.fileList[i], 'time', self.timeMove[i],
#                'id', self.individual[i], 'nest', nest, 'bird', bird, 
#                'st', self.startList[i], 'end',
#                self.endList[i], 'filtMeanMass', filtMeanMass_i, 'meanMass', meanMass, 
#                'z', self.meanZ[i])
#            dt = self.timeMove[i]
#            print('date time', dt.minute)

    def getMeanMass(self, params):
        """
        ## GET MEAN MASS AND DATES IN ARRAYS
        """
        nDat = len(self.fileList)
        self.meanMass = np.zeros(nDat)
        self.dates = np.zeros(nDat, dtype = datetime.date)
        self.endIn = np.repeat(np.nan, nDat)
        self.endOut = np.repeat(np.nan, nDat)
        for i in range(nDat):
            mass_i = np.array(self.MASS[i])
            maskMass = (mass_i > params.massCutOff[0]) & (mass_i < params.massCutOff[1])
            filtMass_i = mass_i[maskMass]
            if (len(filtMass_i) == 0):
                filtMeanMass_i = np.nan
            else:
                quants = mquantiles(filtMass_i, prob = params.quantileThreshold)
                quantMask = (filtMass_i >= quants[0]) & (filtMass_i <= quants[1])
                filtMeanMass_i = np.mean(filtMass_i) 
#                filtMeanMass_i = np.mean(filtMass_i[quantMask]) 
#                print('quants', quants, 'mass', filtMeanMass_i, 'meanMass', np.mean(filtMass_i))
            self.meanMass[i] = filtMeanMass_i
            date_i = self.timeArray[i]
            self.dates[i] = datetime.date(date_i.year, date_i.month, date_i.day)
            ## GET IN AND OUT FOR EACH POINT
            if self.endList[i] == '01':
                if np.isnan(filtMeanMass_i):        # == np.nan:
                    self.endIn[i] = 5300
                else:
                    self.endIn[i] = filtMeanMass_i
            if self.endList[i] == '10':
                if np.isnan(filtMeanMass_i):                 # == np.nan:
                    self.endOut[i] = 3200
                else:
                    self.endOut[i] = filtMeanMass_i
            if self.idArray[i] == 11:
                print('file', self.fileList[i], 'date', date_i, 'end', self.endList[i],
                    'in', self.endIn[i],
                    'out', self.endOut[i], 'meanMass', self.meanMass[i])

    def getPointMeanMass(self, params):
        """    
        ## GET IN AND OUT POINTS, AND POINTS FOR CROSSINGS WITH POOR MASS DATA
        """
        ptNDat = len(self.pointID)
        self.ptMeanMassIn = np.repeat(np.nan, ptNDat)
        self.ptMeanMassOut = np.repeat(np.nan, ptNDat)
        self.ptDates = np.zeros(ptNDat, dtype = datetime.date)
        for i in range(ptNDat):
            mass_i = np.array(self.pointMass[i])
            maskMass = (mass_i > params.massCutOff[0]) & (mass_i < params.massCutOff[1])
            filtMass_i = mass_i[maskMass]
            if (len(filtMass_i) > 0):
#                quants = mquantiles(filtMass_i, prob = params.quantileThreshold)
#                quantMask = (filtMass_i >= quants[0]) & (filtMass_i <= quants[1])
                filtMeanMass_i = np.mean(filtMass_i) 
#                filtMeanMass_i = np.mean(filtMass_i[quantMask]) 
#                print('quants', quants, 'mass', filtMeanMass_i, 'meanMass', np.mean(filtMass_i))
                if self.pointStart[i] == '01':
                    self.ptMeanMassIn[i] = filtMeanMass_i
                elif self.pointStart[i] == '10':
                    self.ptMeanMassOut[i] = filtMeanMass_i
            ## GET DATES 
            date_i = self.pointTime[i]
            self.ptDates[i] = datetime.date(date_i.year, date_i.month, date_i.day)
#            print('i', i, 'date', date_i, 'id', self.pointID[i], 'mass', filtMeanMass_i)         


    def plotMass(self):
        """
        ## EXPLORATORY PLOTS OF MASS AT WB MOVEMENTS
        """
        self.getXTicks()
        nplots = self.nNest
        plotIndx = 0
        nfigures = np.int(np.ceil(nplots/4.0))
        ## LOOP BIG FIGURES
        for i in range(nfigures):
            P.figure(figsize=(14, 9))
            lastFigure = i == (nfigures - 1)
            ## LOOP SUB-PLOTS
            for j in range(4):
                P.subplot(2,2,j+1)
                if plotIndx < nplots:
                    nest_j = self.uNest[plotIndx]
#                    print('i', i, 'j', j, 'nest_j', nest_j)        
                    ## LOOP BIRD IN NEST
                    for k in range(1,3):
                        ## GET MASS DATA FOR NEST_J AND BIRD_K
                        id_mask = (self.nest == nest_j) & (self.bird == k)
                        rfid_jk = self.rfidInteger[id_mask]
                        massDate_mask = (self.idArray == rfid_jk).flatten()
#                        print('idarray', self.idArray, 'mask', massDate_mask.shape, 
#                            'rfid', rfid_jk, 'summask', np.sum(massDate_mask),
#                            'timeArray', self.timeArray.shape)
                        ax1 = P.gca()
                        ## PROCEED IF HAVE MASS DATA
                        if np.sum(massDate_mask) > 0:
                            date_jk = self.timeArray[massDate_mask]
                            mass_jk = self.meanMass[massDate_mask]
                            endIn_jk = self.endIn[massDate_mask]
                            endOut_jk = self.endOut[massDate_mask]
#                            print('nest', nest_j, 'bird', k, 'id', rfid_jk, 'mass', mass_jk, 'date', date_jk) 
                            if k == 1:
                                lns1 = ax1.plot(date_jk, mass_jk, color = 'k', linewidth=1.25)
                                ptIn = ax1.plot(date_jk, endIn_jk, 'ko', markerfacecolor = 'black', 
                                    ms = 4) #, mew = 1.5)
                                ptOut = ax1.plot(date_jk, endOut_jk, 'ko', markerfacecolor = 'none', 
                                    ms = 4) #, mew = 2)
                            else:
                                lns2 = ax1.plot(date_jk, mass_jk, color = 'r', linewidth=1.25)
                                ptIn = ax1.plot(date_jk, endIn_jk, 'ro', markerfacecolor = 'red', 
                                    ms = 4) #, mew = 1.5)
                                ptOut = ax1.plot(date_jk, endOut_jk, 'ro', markerfacecolor = 'none', 
                                    ms = 4) #14, mew = 2)                             

                        ## PREPARE POINT MASS DATA
                        ptmassDate_mask = (self.pointID == rfid_jk).flatten()
                        if np.sum(ptmassDate_mask) > 0:
                            ptDate_jk = self.ptDates[ptmassDate_mask]
                            ptMassOut_jk = self.ptMeanMassOut[ptmassDate_mask]
                            ptMassIn_jk = self.ptMeanMassIn[ptmassDate_mask]
                            if k == 1:
                                print
                                ptMassOut = ax1.plot(ptDate_jk, ptMassOut_jk, 'kD', 
                                    markerfacecolor = 'none', ms = 5)        
                                ptMassIn = ax1.plot(ptDate_jk, ptMassIn_jk, 'kD', 
                                    markerfacecolor = 'black', ms = 5)        
                            else:
                                ptMassIn = ax1.plot(ptDate_jk, ptMassIn_jk, 'rD', 
                                    markerfacecolor = 'red', ms = 5)        
                                ptMassOut = ax1.plot(ptDate_jk, ptMassOut_jk, 'rD', 
                                    markerfacecolor = 'none', ms = 5)        
#                        xmin = np.nanmin(self.timeArray- datetime.timedelta(2))
#                        xmax = np.nanmax(self.timeArray + datetime.timedelta(2))
                        ymax = 5800 # np.nanmax(self.meanMass) + 150
                        ymin = 1000 #-150.0
                        if (j < 2):
                            ax1.set_xlabel('')
                        else:
                            ax1.set_xlabel('Date-time', fontsize = 12)
                        if (j==0) | (j==2):
                            ax1.set_ylabel('Mass (g)', fontsize = 12)
                        else:
                            ax1.set_ylabel('')
#                        ax1.set_ylim([ymin, ymax])
#                        ax1.set_xlim([xmin, xmax])
                        for tick in ax1.xaxis.get_major_ticks():
                            tick.label.set_fontsize(8)
                        for tick in ax1.yaxis.get_major_ticks():
                            tick.label.set_fontsize(10)
#                        P.xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
#                                self.dateAxis[-1]])
#                        ax1.set_xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
#                                self.dateAxis[-1]])
                    P.title('nest' + str(nest_j))
                plotIndx = plotIndx + 1
            P.show(block=lastFigure)


            
    def getXTicks(self):
        startdate = np.min([np.min(self.dates), np.min(self.ptDates)])
        enddate = np.max([np.max(self.dates), np.max(self.ptDates)])
        self.dateAxis = np.empty(4, dtype = datetime.date)
        print('type', type(startdate), 'startDate', startdate, 'enddate', enddate, 'dateaxis', self.dateAxis) 
        self.dateAxis[0] = startdate - datetime.timedelta(2)
        diff = enddate - startdate
        totaldays = diff.days
        self.dateAxis[1] = startdate + datetime.timedelta(int(totaldays/3))
        self.dateAxis[2] = startdate + datetime.timedelta(int(totaldays*2.0/3.0))
        self.dateAxis[-1] = enddate + datetime.timedelta(2)
        print('dateAxis', self.dateAxis)




class ExploreMassCSV(object):
    def __init__(self, params):
        """
        ## explore csv created in rawFunctions.py 
        """
        self.params = params
        ##
        self.readCSV()
        self.readTagData()
        self.getPointDates()
        self.plotWBMassData()

    def readCSV(self):
        ## READ IN CSV DATA
        print('name', self.params.outPointFname)
        ptDat = np.genfromtxt(self.params.outPointFname, delimiter=',', names=True,
            dtype=['i8', 'i8', 'U32', 'U12', 'U12', 'f8', 'i8', 'f8', 'U32'])
        self.rfidInt = ptDat['rfidInt']
        self.time = ptDat['time']
        self.start = ptDat['start']
        self.end = ptDat['end']
        self.mass = ptDat['mass']
        self.n = ptDat['n']
        self.stdev = ptDat['stdev']
        self.fid = ptDat['fid']
        self.note = ptDat['note']
        print('end', self.end[:20])


    def readTagData(self):
        """
        read in tag data
        """
        self.tagDat = np.genfromtxt(self.params.tagFile, delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'S10', 'i8', 'S10', 'i8', 'i8', 'S16', 'S16', 'S10'])
        self.tagYear = self.tagDat['year']
        self.tagMonth = self.tagDat['month']
        self.tagDay = self.tagDat['day']
        self.nTags = len(self.tagYear)
        self.rfid = basicsRaw.decodeBytes(self.tagDat['rfid'], self.nTags)
        self.rfidInteger = np.arange(self.nTags, dtype = int)
        self.rfid2 = basicsRaw.decodeBytes(self.tagDat['rfid2'], self.nTags)
        self.nest = self.tagDat['nest']
        self.bird = self.tagDat['bird']
        self.uNest = np.unique(self.nest)
        self.nNest = len(self.uNest)

    def getPointDates(self):
        """    
        ## GET IN AND OUT POINTS, AND POINTS FOR CROSSINGS WITH POOR MASS DATA
        """
        ptNDat = len(self.rfidInt)
        self.ptDates = np.zeros(ptNDat, dtype = datetime.date)
        for i in range(ptNDat):
            ## GET DATES 
#            date_i = datetime.datetime(self.time[i])
            date_i = datetime.datetime.strptime(self.time[i], '%Y-%m-%d %H:%M:%S')
            self.ptDates[i] = datetime.date(date_i.year, date_i.month, date_i.day)
#            print('i', i, 'date', date_i, 'id', self.pointID[i], 'mass', filtMeanMass_i)         

    def plotWBMassData(self):
        """
        ## MAKE PLOTS OF WB MASS DATA BY NEST AND INDIVIDUAL
        """
        self.getXTicks()
        nplots = self.nNest
        plotIndx = 0
        nfigures = np.int(np.ceil(nplots/4.0))
        ## LOOP BIG FIGURES
        for i in range(nfigures):
            P.figure(figsize=(13, 8))
            lastFigure = i == (nfigures - 1)
            ## LOOP SUB-PLOTS
            for j in range(4):
                P.subplot(2,2,j+1)
                if plotIndx < nplots:
                    nest_j = self.uNest[plotIndx]
                    ## LOOP BIRD IN NEST
                    for k in range(1,3):
                        ## GET MASS DATA FOR NEST_J AND BIRD_K
                        id_mask = (self.nest == nest_j) & (self.bird == k)
                        rfid_jk = self.rfidInteger[id_mask]
#                        massDate_mask = (self.rfidInt == rfid_jk).flatten()
                        ax1 = P.gca()
                        ## PREPARE POINT MASS DATA
                        ptmassDate_mask = (self.rfidInt == rfid_jk)     ##.flatten()
                        crossMask = (self.end != '-99') & ptmassDate_mask
                        crossInMask = (self.end == '01') & ptmassDate_mask
                        crossOutMask = (self.end == '10') & ptmassDate_mask
                        inMask = (self.end == '-99') & (self.start == '01') & ptmassDate_mask
                        outMask = (self.end == '-99') & (self.start == '10') & ptmassDate_mask
                        if np.sum(ptmassDate_mask) > 0:
                            ## DATE DATA
                            dateCross_jk = self.ptDates[crossMask]
                            dateInCross_jk = self.ptDates[crossInMask]
                            dateOutCross_jk = self.ptDates[crossOutMask]
                            dateIn_jk = self.ptDates[inMask]
                            dateOut_jk = self.ptDates[outMask]
                            ## MASS DATA
                            massCross_jk = self.mass[crossMask]
                            massInCross_jk = self.mass[crossInMask]
                            massOutCross_jk = self.mass[crossOutMask]
                            massOut_jk = self.mass[outMask]
                            massIn_jk = self.mass[inMask]
                            if k == 1:
                                lnCross = ax1.plot(dateCross_jk, massCross_jk, 
                                    color = 'k', linewidth = 1.25)
                                ptInCross = ax1.plot(dateInCross_jk, massInCross_jk, 'ko', 
                                    markerfacecolor = 'black', ms = 4)        
                                ptOutCross = ax1.plot(dateOutCross_jk, massOutCross_jk, 'ko', 
                                    markerfacecolor = 'none', ms = 4)        
                                ptMassIn = ax1.plot(dateIn_jk, massIn_jk, 'kD', 
                                    markerfacecolor = 'black', ms = 5)        
                                ptMassOut = ax1.plot(dateOut_jk, massOut_jk, 'kD', 
                                    markerfacecolor = 'none', ms = 5)        
                            else:
                                lnCross = ax1.plot(dateCross_jk, massCross_jk, 
                                    color = 'r', linewidth = 1.25)
                                ptInCross = ax1.plot(dateInCross_jk, massInCross_jk, 'ro', 
                                    markerfacecolor = 'red', ms = 4)        
                                ptOutCross = ax1.plot(dateOutCross_jk, massOutCross_jk, 'ro', 
                                    markerfacecolor = 'none', ms = 4)        
                                ptMassIn = ax1.plot(dateIn_jk, massIn_jk, 'rD', 
                                    markerfacecolor = 'red', ms = 5)        
                                ptMassOut = ax1.plot(dateOut_jk, massOut_jk, 'rD', 
                                    markerfacecolor = 'none', ms = 5)        
                        xmin = np.nanmin(self.ptDates - datetime.timedelta(2))
                        xmax = np.nanmax(self.ptDates + datetime.timedelta(2))
                        ymax = 5800 # np.nanmax(self.meanMass) + 150
                        ymin = 3000 #-150.0
                        if (j < 2):
                            ax1.set_xlabel('')
                        else:
                            ax1.set_xlabel('Date-time', fontsize = 12)
                        if (j==0) | (j==2):
                            ax1.set_ylabel('Mass (g)', fontsize = 12)
                        else:
                            ax1.set_ylabel('')
                        ax1.set_ylim([ymin, ymax])
                        ax1.set_xlim([xmin, xmax])
                        for tick in ax1.xaxis.get_major_ticks():
                            tick.label.set_fontsize(8)
                        for tick in ax1.yaxis.get_major_ticks():
                            tick.label.set_fontsize(10)
#                        P.xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
#                                self.dateAxis[-1]])
#                        ax1.set_xticks([self.dateAxis[0], self.dateAxis[1], self.dateAxis[2],
#                                self.dateAxis[-1]])
                    P.title('nest' + str(nest_j))
                plotIndx = plotIndx + 1
            P.show(block=lastFigure)


            
    def getXTicks(self):
        startdate = np.min([np.min(self.ptDates), np.min(self.ptDates)])
        enddate = np.max([np.max(self.ptDates), np.max(self.ptDates)])
        self.dateAxis = np.empty(4, dtype = datetime.date)
        print('type', type(startdate), 'startDate', startdate, 'enddate', enddate, 'dateaxis', self.dateAxis) 
        self.dateAxis[0] = startdate - datetime.timedelta(2)
        diff = enddate - startdate
        totaldays = diff.days
        self.dateAxis[1] = startdate + datetime.timedelta(int(totaldays/3))
        self.dateAxis[2] = startdate + datetime.timedelta(int(totaldays*2.0/3.0))
        self.dateAxis[-1] = enddate + datetime.timedelta(2)
        print('dateAxis', self.dateAxis)




