 #!/usr/bin/env python

import numpy as np
import os

class Params(object):
    def __init__(self):
        # set path and output file names 
        self.adeliepath = os.getenv('ADELIEPROJDIR', default = '.')
        # paths and data to read in
        ## SPECIFY THE YEAR  ####################################
        self.year = 'year2018_2019'
#        self.year = 'year2019_2020'
        #########################################################
        self.tagFile = os.path.join(self.adeliepath, 'data', self.year, 'tagID.csv')
#        self.tagFile = os.path.join(self.adeliepath, 'data', self.year, 
#                'AllData', 'tagID2019_2020.csv')
        ## YEAR 2018-2019
        self.fileRange = [363, (980 + 1)]    #363 - 980
        ## YEAR 2019-2020
#        self.fileRange = [31, (1147 + 1)]    #31 - 1147

        ## PATH TO DATA
        self.dataPath = os.path.join(self.adeliepath, 'data', self.year)
#        self.dataPath = os.path.join(self.adeliepath, 'data', self.year, 'AllData')
        ## filename to pickle processed data 
###        self.outArrayFname = os.path.join(self.adeliepath, 'data', 
###                self.year, 'AllData', 'wb_Accum_MassData2019_2020.pkl')
        self.outArrayFname = os.path.join(self.adeliepath, 'data', 
                self.year, 'wb_Accum_MassData2018_2019.pkl')
        self.opticTimeThreshold = 5.0
        self.maxTimeInterRead = 0.5    #TIME IN HOURS BETWEEN TIMES ON WB

        self.quantileThreshold = [0.25, 0.75]
        self.massCutOff = [2650, 5250]        #[3400, 5600]
###        self.outPointFname = os.path.join(self.adeliepath, 'data', self.year, 
###                'AllData', 'pointWB_Accum_2019_2020Data.csv')
        self.outPointFname = os.path.join(self.adeliepath, 'data', self.year,
            'pointWB_Accum_2018_2019Data.csv')
