#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
import datetime
from numba import jit

def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


def vupdate(y, pred, s1, s2, ndat):
    """
    ## UPDATE SIGMA FOR MASS MODEL
    """
    predDiff = y - pred
    sx = np.sum(predDiff**2.0)
    u1 = s1 + np.multiply(.5, ndat)
    u2 = s2 + np.multiply(.5, sx)                       # rate parameter    
    isg = np.random.gamma(u1, 1./u2, size = None)       # formulation using shape and scale
    newVar = 1.0/isg
    return(newVar)

def gibbsUpdater(x, y, sig, covDiag, npara):
    """
    ## GIBBS UPDATER OF NORMAL PARAMETERS
    """
    vinvert = np.linalg.inv(covDiag)
    ppart = np.dot(vinvert, np.zeros(npara))
    xTranspose = np.transpose(x)
    xCrossProd = np.dot(xTranspose, x)
    sinv = 1.0 / sig
    sx = np.multiply(xCrossProd, (1.0 / sig))
    xyCrossProd = np.dot(xTranspose, y)
    sy = np.multiply(xyCrossProd, sinv)
    bigv = np.linalg.inv(sx + vinvert)
    smallv = sy + ppart
    meanVariate = np.dot(bigv, smallv)
    newPara = np.random.multivariate_normal(meanVariate, bigv)
    return(np.transpose(newPara))

@jit(nopython = True)
def massSurvFX(mt_1, t, r, maxMass, gam):
    sss = 1.0          
    ## CONVERT TO KG
    m = mt_1 / 1000.0
    for dd in range(t):
        m = m * np.exp(r * (1.0 - (m / maxMass)))
        sss = sss * np.exp(-gam / m**2.0)
    ## CONVERT BACK TO GRAMS
    m = m * 1000.0
    return(m, sss)

@jit(nopython = True)
def fullMassSurvFX(n, mt_1, m, s, t, r, maxMass, gam):
    """
    ##  CALC PREDICTED MASS (MU) AND PROB OF SURVIVAL AT TIME T
    """
    ## LOOP THRU ALL MEASURE DATA FOR ALL CHICKS
    for i in range(n):
        ## SET FIRST PROBABILITY OF SURVIVAL TO 1.0
        s_i = 1.0        
        ## CONVERT TO KG
        m_i = mt_1[i] / 1000.0
        ## DAY INTERVAL SINCE LAST MEASURE
        t_i = t[i]
        gam_i = gam[i]
        ## GROWTH RATE
        r_i = r[i]
        ## LOOP THRU DAYS SINCE LAST MEASURE
        for j in range(t_i):
            m_i = m_i * np.exp(r_i * (1.0 - (m_i / maxMass)))
            s_i = s_i * np.exp(-gam_i / m_i**2.0)
        ## CONVERT MASS BACK TO GRAMS
        m[i] = m_i * 1000.0
        s[i] = s_i
    return(m, s)


@jit
def logNormPDF(y, m, s):
    """
    NORMAL PDF FOR NUMBA FUNCTIONS
    """
    lognpdf = np.log(1.0 / (s * np.sqrt(2.0 * np.pi))) * np.exp(-(y - m)**2 / 2 / s**2)
    return(lognpdf)

@jit(nopython = True)
def bernLogPMF(k, p):
    logpmf = np.log((p**k) * ((1.0 - p)**(1.0 - k)))
    return(logpmf)


@jit(nopython = True)
def dayUpdate(ndat, r, MassT_1, gamArray, maskLatentDays, trt,
            mass, alive, mu, s, dayInterval, maxMass, betaVar, tauVar, hatch):        

    for i in range(ndat):
        if ~maskLatentDays[i]:
            continue
        range_i = np.arange(hatch[i,0], hatch[i,1] + 1)
        maskDay = np.where(range_i == dayInterval[i], False, True)
        range_i = range_i[maskDay]
        randDay_s = np.random.choice(range_i, 1)
        randDayInt_s = np.int64(randDay_s[0])
        r_i = r[i]
        gam_i = gamArray[i] 
        massT_1_i = MassT_1[i]   

        (mu_s, s_s) = massSurvFX(massT_1_i, randDayInt_s, r_i, maxMass, gam_i)
        
        ## PRESENT LIKELIHOODS
        ## BETA DATA
        if trt[i] < 2:
            lMassPDF = (logNormPDF(np.log(mass[i]), np.log(mu[i]), 
                np.sqrt(betaVar)))
            lMassPDF_s = (logNormPDF(np.log(mass[i]), np.log(mu_s),
                np.sqrt(betaVar)))
        ## TAU DATA
        if trt[i] == 2:

           lMassPDF = (logNormPDF(np.log(mass[i]), np.log(mu[i]), 
                np.sqrt(tauVar)))
           lMassPDF_s = (logNormPDF(np.log(mass[i]), np.log(mu_s), 
                np.sqrt(tauVar)))
        ## SURVIVAL LIKELIHOODS
        lBernPMF =  bernLogPMF(alive[i], s[i])  #stats.binom.logpmf(alive[i], 1, s[i])
        lBernPMF_s =  bernLogPMF(alive[i], s_s)
        ## CALC IMPORTANCE RATIO FOR ACCEPT/REJECT
        pnow = lMassPDF + lBernPMF 
        pnew = lMassPDF_s + lBernPMF_s 
        pdiff = pnew - pnow
        rValue = np.exp(pdiff)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0)
        ## UPDATE PARAMETERS AND PREDICTIONS
        if rValue > zValue:
            mu[i] = mu_s
            s[i] = s_s
            dayInterval[i] = randDayInt_s

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        self.T = 5          ## N SESSIONS
        self.ddays = 7
        ## INITIAL DATE
        self.dateArr = np.array(datetime.date(2019,12,21))
        ## DEVELOPEMENT STAGE EFFECT (BR, GUARD, CRECHE)
#        self.GAM = np.array([0.0075, 0.05, 0.1])
        self.GAM = np.array([0.001, 0.005, 0.01])
#        self.GAM = 0.03 
        self.nGAM = len(self.GAM)
        ## TRT EFFECT ON N PROVISIONING FREQ FOR MC AND SPLASH
        ## LOG NORMAL: INTERCEPT, SPLASH, ICE
        self.DELTA = np.array([-1.5, -0.02, 0.6])
        self.SIGDELTA = 0.01
        ## TRT EFFECT ON PROVISIONING MASS MC AND SPLASH (NORMAL PROCESS)
        ## LOG NORMAL: INTERCEPT, SPLASH, ICE
        self.ALPHA = np.array([0.1, -0.03, 0.2])   ## EFFECT OF MC AND SPLASH
        self.SIGALPHA = 0.01
        ## HATCHING MASS = 85 GRAMS.
        self.HatchMass = 85.0   # / 1000.
        self.probUnknownDate0 = 0.4

        self.nInd = 120 
        self.nNests = np.int64(self.nInd / 2)
        self.nestPerTrt = 20
        self.nTrt = 3

        ## BETAS FOR R WB MODEL
        ## INTERCEPT, N_PROV, PROV_MASS, MassDiff
        self.B = np.array([.08, .1, .08, 0.08])
        self.nBetaPara = len(self.B)
        ## TAU FOR R NON-WB MODEL
        ## INTERCEPT, MassDiff, ICE
        self.TAU = np.array([0.12, 0.05, 0.06])
        self.nTauPara = len(self.TAU)
        ## NEST EFFECT MEAN AND SD
        self.nestMeanSD = [0.0, 0.04]
        # variance used to simulate MASS data on log scale
        self.BETAVAR = 0.07
        self.TAUVAR = 0.08
        ## MAX MASS IN KG
        self.maxMass = 5.0

        # Initial values
        self.b = np.array([0.1, 0.05, -0.01, 0.05])
        self.betaVar = 0.05     # self.BETAVAR
        self.tau = np.array([0.08, 0.1, 0.02])  #self.TAU.copy()
        self.tauVar = 0.05      # self.TAUVAR
        self.alpha = np.array([0.05, -0.1, 0.8])
        self.sigAlpha = 0.05
        self.delta = np.array([-0.5, 0.04, 0.5])
        self.sigDelta = 0.07
#        self.gam = 0.05
        self.gam = np.array([0.005, 0.001, 0.15])    # self.GAM.copy()  

        # PRIORS AND SEARCH PARAMETERS
        self.betaPriors = np.array([0, 5])
        self.betaSearch = [0.01, 0.01, 0.1, 0.05]
        self.tauPriors = np.array([0, 5])
        self.tauSearch = [0.01, 0.01, 0.2]      #0.01

        ## SIGMA PRIORS
        self.s1 = .1      # GAMMA S
        self.s2 = .1
        ## NPROV PRIORS FOR TRT EFFECT
        self.nProvSDPrior = 0.9          ## LOG NORMAL SD PRIOR FOR NORMAL.
        ## GAMMA PRIOR - LOG NORMAL DISTRIBUTION
        self.gamPrior = [.1, 10.0]  
        self.gammaSearch = 0.05
        ## NEST EFFECT PRIORS AND SEARCH
        self.nestEffPriors = [0.0, 0.1]
        self.nestEffSearch = 0.01
        ## DELTA PRIORS
        self.nDeltaPara = len(self.delta)
        self.deltaDiag = np.diagflat(np.tile(10, self.nDeltaPara))
        ## ALPHA PRIORS
        self.nAlphaPara = len(self.alpha)
        self.alphaDiag = np.diagflat(np.tile(10, self.nAlphaPara))


         ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 50           # number of estimates to save for each parameter
        self.thinrate = 20          # thin rate
        self.burnin = 100           # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        print('ngibbs', self.ngibbs, 'nIterations = ', self.ngibbs*self.thinrate + self.burnin)

class Basicdata(object):
    def __init__(self, params):
        self.params = params
        self.b = self.params.b
        self.betaVar = self.params.betaVar
        self.tau = self.params.tau
        self.tauVar = self.params.tauVar
        self.alpha = self.params.alpha
        self.sigAlpha = self.params.sigAlpha
        self.delta = self.params.delta
        self.sigDelta = self.params.sigDelta
        self.gam = self.params.gam

        #########################################
        ##  RUN FUNCTIONS
        self.makedesigndata()
        self.makeHatchDate()
        self.makeCovariates()
        self.massSurvData()

        self.removeDeadChicks()
        self.getInitialValues()
        #########################################

    def makedesigndata(self):
        self.ind = np.repeat(np.arange(self.params.nInd), self.params.T)
        self.nest = np.repeat(np.arange(self.params.nNests), (self.params.T * 2))
        self.trt = np.repeat(np.arange(self.params.nTrt), 
                (self.params.nestPerTrt * self.params.T * 2))
        self.session = np.tile(np.arange(self.params.T), self.params.nInd) 
        self.uNest = np.unique(self.nest)
        self.ndat = len(self.trt)
        self.alive = np.zeros(self.ndat, dtype = int)
#        self.alive[self.session==0] = 1
        ## MAKE DATES
        self.dateArr = self.params.dateArr
        date_i = self.params.dateArr
        for i in range(self.params.T-1):
            date_i = date_i + datetime.timedelta(days = self.params.ddays)
            self.dateArr = np.append(self.dateArr, date_i)
            print('diff', (date_i - self.dateArr[0]).days)
        print(self.dateArr, 'ndat', self.ndat, len(self.ind), len(self.nest), len(self.session))

#        for i in range(self.ndat):
#            print('sess', self.session[i], 'nest', self.nest[i], 'ind', self.ind[i],
#                'trt', self.trt[i])



    def makeHatchDate(self):
        self.uInd = np.unique(self.ind)
        self.DAYINTERVAL = np.repeat(7.0, self.ndat)
        self.dayInterval = self.DAYINTERVAL.copy()
        sess0Mask = self.session == 0
        nSess0 = np.sum(sess0Mask)
        ## REAL INTERVAL DATA USED TO SIMULATE DATA
        self.DAYINTERVAL[sess0Mask] = np.round(np.random.uniform(3, 7, nSess0), 0)
        self.knownHatchDate = np.ones(self.ndat)
        self.hatchRange = np.zeros((self.ndat, 2))
        ## LOOP THRU INDIVID AND DETERMINE IF KNOWN HATCH DATE
        for i in range(self.params.nInd):
            indMask = self.ind == self.uInd[i]
            sess0IndMask = sess0Mask & indMask
            knownHatchDate_i = np.random.binomial(1, 0.5)
            self.knownHatchDate[sess0IndMask] = knownHatchDate_i
            if knownHatchDate_i == 0:
                low_i = np.random.uniform(self.DAYINTERVAL[sess0IndMask] - 2, 
                        self.DAYINTERVAL[sess0IndMask])
                self.hatchRange[sess0IndMask, 0] = np.round(low_i, 0)
                high_i = np.random.uniform(self.DAYINTERVAL[sess0IndMask] + 1, 
                        self.DAYINTERVAL[sess0IndMask] + 2)
                self.hatchRange[sess0IndMask, 1] = np.round(high_i, 0)
#                print('i', i, 'knownHatchDate', knownHatchDate_i, 'KNOWNINTER', 
#                    self.DAYINTERVAL[sess0IndMask],
#                    'range', self.hatchRange[sess0IndMask])

                ## INITIAL DAY INTERVAL - WILL BE UPDATED FOR UNKNOWN DATES (SESSION 0)
                self.dayInterval[sess0IndMask] = np.round(np.random.uniform(
                        self.hatchRange[sess0IndMask, 0],
                        self.hatchRange[sess0IndMask, 1]), 0)


    def makeCovariates(self):        
        ## MAKE DEVELOPMENT PERIODS: BROOD, GUARD, CRECHE
        self.devel = np.zeros(self.params.T, dtype = int)
        self.devel[1] = 1
        self.devel[2:] = 2
#        print('self.devel', self.devel)
        self.develArray = np.tile(self.devel, self.params.nInd)
#        print('devel arr', self.develArray[100:400], 'len', len(self.develArray))
        ## MAKE GAMMA ARRAY FOR EACH DEVEL PERIOD
        self.GAMArray = self.params.GAM[self.develArray]

        ## MAKE X DATA FOR PROVISIONING RATE MODEL
        ## LENGTH OF WB DATA AND CONTROL NESTS
        self.maskWB = self.trt < 2
        self.nWB = len(self.trt[self.maskWB])
        ## MASK OF CONTROL NESTS AND N DATA
        self.maskControl = self.trt == 2
        self.nControl = np.sum(self.maskControl)

        self.getIce()
        self.getNestProv()

        ## X DATA FOR PROVISION RATE MODEL - INTERCEPT, N_PROV, PROV_MASS, PERCDIFF MASS
        self.xBeta = np.zeros((self.nWB, self.params.nBetaPara))
        ## INTERCEPT
        self.xBeta[:, 0] = 1.0
        ## PROV FREQ AND MASS
        self.xBeta[:, 1] = self.PROVFREQ    ## INITIAL VALUES
        self.xBeta[:, 2] = self.PROVMASS    ## INITIAL VALUES
        ## CONTROL X DATA - INTERCEPT, ICE, PERC DIFF MASS
        self.xTau = np.zeros((self.nControl, self.params.nTauPara))
        ## CONTROL INTERCEPT
        self.xTau[:, 0] = 1.0
        ## CONTROL ICE EFFECT 
        self.xTau[:, 1] = self.ice[self.trt == 2]

        ## SCALE COVARIATES - EXCEPT PERC MASS DIFF
        ## COLUMNS TO SCALE
#        self.covToScale = [1, 2, 4, 5]
#        mcov = np.mean(self.x[:, self.covToScale], axis = 0)
#        scov = np.std(self.x[:, self.covToScale], axis = 0)
#        self.xScale = self.x.copy()
#        self.xScale[:, self.covToScale] = (self.xScale[:, self.covToScale] - mcov) / scov

        ## MAKE NEST EFFECT

#        self.NESTEFFECT = np.zeros(self.params.nNests)

        self.NESTEFFECT = np.random.normal(self.params.nestMeanSD[0], self.params.nestMeanSD[1], 
                        self.params.nNests)  ## SIM DATA
        self.nestEffArray = self.NESTEFFECT[self.nest]
#        for i in range(self.ndat):
#            print('i', i, 'nest', self.nest[i], 'nest effect', self.nestEffArray[i])

        ## MASK FOR ID LATENT DATA FOR DAYS, PROVFREQ AND PROVMASS
        self.maskLatentProvFreq = self.session == 0
        self.maskLatentProvMass = self.session == 0

        self.maskLatentDays = self.knownHatchDate == 0
        
        self.nLatentDays = np.int64(np.sum(self.maskLatentDays))
        self.hatchRangeLatent = self.hatchRange[self.maskLatentDays]
        print('1   nlatentdays', self.nLatentDays, 'len lat', len(self.hatchRangeLatent))


    def getIce(self):
        self.ice = np.zeros(self.ndat)
        for i in range(self.params.T):
            sessMask = self.session == i
            ice_i = np.random.uniform(0.02, 0.90)
            self.ice[sessMask] = ice_i
#        for i in range(self.ndat):
#            print('i', i, 'sess', self.session[i], 'nest', self.nest[i], 'ice', self.ice[i])

    def getNestProv(self):
        ## X COVARIATES FOR PROV FREQ AND PROV MASS MODELS
        self.xPROV = np.ones((self.nWB, self.params.nDeltaPara))
        self.splashMask = self.trt[self.maskWB] == 1.0
        self.xPROV[~self.splashMask, 1] = 0.0
        self.xPROV[:, 2] = self.ice[self.maskWB]
        self.muDelt = np.dot(self.xPROV, self.params.DELTA) 
        self.PROVFREQ = np.zeros(self.nWB)
        self.muAlpha = np.dot(self.xPROV, self.params.ALPHA) 
        self.PROVMASS = np.zeros(self.nWB)

        ## WB ARRAYS
        self.wbSession = self.session[self.maskWB]
        self.wbInd = self.ind[self.maskWB]
        self.wbNest = self.nest[self.maskWB]
        self.uNestWB = np.unique(self.wbNest)
        self.nNestWB = len(self.uNestWB)

        ## LOOP THRU SESSIONS
        for j in range(self.params.T):
            sessMask = self.session[self.maskWB] == j
            ## LOOP THRU NESTS IN WB
            for i in range(self.nNestWB):
                nestMask = self.wbNest == self.uNestWB[i]
                nestSessMask = sessMask & nestMask
                muDelt_ij = self.muDelt[nestSessMask][0]
                provfreq =  np.exp(np.random.normal(muDelt_ij, np.sqrt(self.params.SIGDELTA)))
                self.PROVFREQ[nestSessMask] = provfreq

                muAlpha_ij = self.muAlpha[nestSessMask][0]
                provmass =  (np.random.normal(muAlpha_ij, np.sqrt(self.params.SIGALPHA)))
                self.PROVMASS[nestSessMask] = provmass

#        for i in range(self.nWB):
#            print('sess', self.wbSession[i], 'nest', self.wbNest[i], 'ind', self.wbInd[i],
#                'muDelt', self.muDelt[i], 'provfreq', self.PROVFREQ[i])



    def massSurvData(self):
        """
        MAKE MASS AND SURVIVAL DATA
        """
        self.keepMask = np.zeros(self.ndat, dtype = bool)
#        self.controlKeepMask = np.zeros(self.nControl, dtype = bool)
#        self.wbKeepMask = np.zeros(self.nWB, dtype = bool)

        self.R = np.zeros(self.ndat)
        self.S = np.zeros(self.ndat)
        self.MU = np.zeros(self.ndat)
        self.mass = np.zeros(self.ndat)
        self.MassT_1 = np.zeros(self.ndat)
        ## LOOP NESTS
        for i in range(self.params.nNests):
            nest_i = self.uNest[i]
            nestMask = self.nest == nest_i


            ## FOR GETTING X DATA FOR CONTROL AND WB
            trtNest = self.trt[nestMask][0]
            ## WB
            if trtNest < 2:
                xNestMask = nestMask[self.maskWB]
            elif trtNest == 2:
                xNestMask = nestMask[self.maskControl] 


            ## LOOP SESSIONS
            for j in range(self.params.T):
                sessMask = self.session == j
                nestSessMask = nestMask & sessMask
                if j == 0:
                    massT_1 = np.repeat(self.params.HatchMass, 2)
                    self.keepMask[sessMask] = True
                else:
                    sessT_1Mask = (self.session == (j-1))
                    nestSessT_1Mask = nestMask & sessT_1Mask
                    massT_1 = self.mass[nestSessT_1Mask]
                sumMassT_1 = np.sum(massT_1)
                ## POPULATE ARRAY FOR MCMC
                self.MassT_1[nestSessMask] = massT_1
                ## IF BOTH CHICKS DEAD, THEN BREAK LOOP FOR THIS NEST
                if sumMassT_1 == 0:
                    break
                percDiff_ij = (massT_1 / np.sum(massT_1))**2

                #########################################################
                ## POPULATE THE COVARIATE MATRIX FOR R MODEL            # 
                ## WB                                                   #
                if trtNest < 2:                                         #
                    xSessMask = sessMask[self.maskWB]                   #
                    xSessNestMask = xSessMask & xNestMask               #
                    self.xBeta[xSessNestMask, -1] = percDiff_ij         #
                elif trtNest == 2:                                      #
                    xSessMask = sessMask[self.maskControl]              #
                    xSessNestMask = xSessMask & xNestMask               #
                    self.xTau[xSessNestMask, -1] = percDiff_ij          #

#                self.xScale[nestSessMask, 3] = percDiff_ij
                #########################################################

                ind_ij = np.unique(self.ind[nestSessMask])
                ## LOOP INDIVIDUALS
                for k in range(len(ind_ij)):
                    ind_ijk = ind_ij[k]
                    indMask = self.ind == ind_ijk
                    nestSessIndMask = nestSessMask & indMask
                    if j > 0:
                        nestSessIndT_1Mask = nestSessT_1Mask  & indMask
                        ## IF NOT ALIVE, GO TO NEXT INDIVIDUAL
                        if (self.alive[nestSessIndT_1Mask] == 0):
                            continue
                    self.keepMask[nestSessIndMask] = True
                    ## MASS PREVIOUS TIME SESSION IN KG


                    massT_1_ijk = massT_1[k] # / 1000.
#                    massT_1_ijk = np.array([massT_1[k]]) # / 1000.
 
                    #################################################################
                    ## CALC R IN Kg FOR WB AND CONTROL NESTS                        #
                    ## WB                                                           #
                    if trtNest < 2:                                                 #
                        xIndMask = indMask[self.maskWB]                             #
                        xIndSessNestMask = xIndMask & xSessNestMask
                        r = (np.dot(self.xBeta[xIndSessNestMask], 
                            self.params.B) + self.nestEffArray[nestSessIndMask])
                        r = r[0]
                    ## CONTROL
                    elif trtNest == 2:
                        xIndMask = indMask[self.maskControl]
                        xIndSessNestMask = xIndMask & xSessNestMask
                        r = (np.dot(self.xTau[xIndSessNestMask], 
                            self.params.TAU) + self.nestEffArray[nestSessIndMask])
                        r = r[0]
                    self.R[nestSessIndMask] = r 
                    ##################################################################

                    daysInt = np.int64(self.DAYINTERVAL[nestSessIndMask])
                    gam = self.GAMArray[nestSessIndMask][0]

                    (mu, s_ij) = massSurvFX(massT_1_ijk, daysInt, r, self.params.maxMass, gam)

                    self.MU[nestSessIndMask] = mu

                    self.S[nestSessIndMask] = s_ij
                    alive_ij = np.random.binomial(1, s_ij)
                    self.alive[nestSessIndMask] = alive_ij
                    ## RANDOM VARIATE OF MASS IN KG FOR WB AND CONTROL
                    if trtNest < 2:
                        massTrt = (np.exp(np.random.normal(
                        np.log(self.MU[nestSessIndMask]), np.sqrt(self.params.BETAVAR))))
                    if trtNest == 2:
                        massTrt = (np.exp(np.random.normal(
                        np.log(self.MU[nestSessIndMask]), np.sqrt(self.params.TAUVAR))))
                    self.mass[nestSessIndMask] = massTrt

#                    print('n', i, 'ses', j, 
#                         'tr', self.trt[nestSessIndMask], 
#                        'ind', ind_ijk,
#                         'aliv', alive_ij, self.keepMask[nestSessIndMask], 
#                        'day', self.DAYINTERVAL[nestSessIndMask], 
#                        'dev', self.develArray[nestSessIndMask], 
#                        self.GAMArray[nestSessIndMask], 'sij', np.round(s_ij, 2),
#                        'R', np.round(self.R[nestSessIndMask],3),
#                        'mT_1', np.round(massT_1_ijk, 3),
#                        'mu', np.round(self.MU[nestSessIndMask], 2),
#                        'mass', np.round(self.mass[nestSessIndMask],2))
            

    def removeDeadChicks(self):
#        keepMask = (self.alive == 1) | (self.session == 0)

        self.mass = self.mass[self.keepMask]
        self.MassT_1 = self.MassT_1[self.keepMask]
        self.R = self.R[self.keepMask]
        self.S = self.S[self.keepMask]
        self.GAMArray = self.GAMArray[self.keepMask]
        self.MU = self.MU[self.keepMask]
        self.alive = self.alive[self.keepMask]
        self.develArray = self.develArray[self.keepMask]
        self.ind = self.ind[self.keepMask]
        self.nest = self.nest[self.keepMask]
        self.trt = self.trt[self.keepMask]
        self.session = self.session[self.keepMask]
        self.dayInterval = (self.dayInterval[self.keepMask]).astype(int)
        self.DAYINTERVAL = self.DAYINTERVAL[self.keepMask]
        self.nestEffArray = self.nestEffArray[self.keepMask]
        self.ndat = len(self.trt)

        self.maskLatentProvFreq = self.maskLatentProvFreq[self.keepMask]
        self.maskLatentProvMass = self.maskLatentProvMass[self.keepMask]

        self.maskLatentDays = self.maskLatentDays[self.keepMask]
        self.knownHatchDate = self.knownHatchDate[self.keepMask]
        self.hatchRange = self.hatchRange[self.keepMask]

        self.wbKeepMask = self.keepMask[self.maskWB]
        self.controlKeepMask = self.keepMask[self.maskControl]
        self.xPROV = self.xPROV[self.wbKeepMask]
        self.PROVFREQ = self.PROVFREQ[self.wbKeepMask]
        self.PROVMASS = self.PROVMASS[self.wbKeepMask]
        self.xBeta = self.xBeta[self.wbKeepMask]
        self.xTau = self.xTau[self.controlKeepMask]
        self.nWB = np.sum(self.wbKeepMask)
        self.nControl = np.sum(self.controlKeepMask)

    
        ## LENGTH OF WB DATA AND CONTROL NESTS
        self.maskWB = self.trt < 2
        self.nWB = np.sum(self.maskWB)
        ## MASK OF CONTROL NESTS AND N DATA
        self.maskControl = self.trt == 2
        self.nControl = np.sum(self.maskControl)




#        for i in range(self.ndat):
#            print('ses', self.session[i], 'tr', self.trt[i], 
#                        'ind', self.ind[i], 'aliv', self.alive[i], 
#                        'day', self.DAYINTERVAL[i], 
#                        'dev', self.develArray[i], 
#                        self.GAMArray[i], 'sij', np.round(self.S[i], 2),
#                        'mT_1', np.round(self.MassT_1[i], 3),
#                        'mu', np.round(self.MU[i], 2),
#                        'mass', np.round(self.mass[i],2),
#                        'R', np.round(self.R[i], 2))


#        for i in range(self.nWB):
#            if (self.nest[i] < 35) & (self.nest[i] > 15):
#                print('ses', self.session[i],'tr', self.trt[i], 
#                    'nest', self.nest[i], 'ind', self.ind[i], 'mass', np.round(self.mass[i],2),
#                    'aliv', self.alive[i],  self.xBeta[i])



    def getInitialValues(self):
        self.nestEffect = np.random.normal(0,0.01, self.params.nNests)  ## SIM DATA
        self.nestEffArray = self.nestEffect[self.nest]
        self.r = np.zeros(self.ndat)
        self.s = np.zeros(self.ndat)
        self.mu = np.zeros(self.ndat)

        self.gamArray = self.gam[self.develArray]

        self.r[self.maskWB] = np.dot(self.xBeta, self.b) + self.nestEffArray[self.maskWB]
        self.r[self.maskControl] = (np.dot(self.xTau, self.tau) + 
            self.nestEffArray[self.maskControl])

#        print('type dayint', type(self.dayInterval[5]), 'ndat type', type(self.ndat), 
#            'ndat', self.ndat, 'mu type', type(self.mu), 'mu shp', np.shape(self.mu),
#            'MassT_1 shp', np.shape(self.MassT_1), 'massT_1 type', type(self.MassT_1),
#            's', type(self.s), 'r', np.shape(self.r), 'type r', type(self.r),
#            'max', type(self.params.maxMass), 'gam', type(self.gamArray))



        (self.mu, self.s) = fullMassSurvFX(self.ndat, self.MassT_1, self.mu, self.s, 
            self.dayInterval, self.r, self.params.maxMass, self.gamArray)

        self.provFreq = self.PROVFREQ.copy()        # np.exp(np.dot(self.xPROV, self.delta))
        self.provMass = self.PROVMASS.copy()        # np.dot(self.xPROV, self.alpha)
        self.muDelt = np.dot(self.xPROV, self.delta) 
        self.muAlpha = np.dot(self.xPROV, self.alpha) 


        self.makeHatchRangeList()


#        P.figure()
#        P.subplot(2,2,1)
#        P.plot(self.mass, self.mu)
#        P.title('mass v mu')
#        P.subplot(2,2,2)
#        P.plot(self.S, self.s)
#        P.title('S v s')
#        P.subplot(2,2,3)
#        P.plot(self.R, self.r)
#        P.title('R v r')

#        for i in range(self.nWB):
##            if (self.nest[i] < 23) & (self.nest[i] > 17):
#                print('ses', self.session[i],'tr', self.trt[i], 
#                    'nest', self.nest[i], 'ind', self.ind[i], 
#                    'mu', np.round(self.mu[i], 3),
#                    'masT_1', np.round(self.MassT_1[i], 3),
#                    'r', np.round(self.r[i], 3), 
#                    's', np.round(self.s[i], 3),
#                    'aliv', self.alive[i])


    def makeHatchRangeList(self):
        self.hatchRangeList = []
        for i in range(self.nLatentDays):
            range_i = (np.arange(self.hatchRangeLatent[i,0], self.hatchRangeLatent[i,1] + 1))
            self.hatchRangeList.append(range_i)
#            if i < 5:
#                rand_i = np.random.choice(self.hatchRangeList[i], 1)[0]
#                print('hatchrangelist', self.hatchRangeList[i], 'rand_i', rand_i, 'type',
#                    type(rand_i))



class MCMC(object):
    def __init__(self, params, basicdata):

        self.params = params
        self.basicdata = basicdata

        self.mu_s = np.zeros(self.basicdata.ndat)
        self.s_s = np.zeros(self.basicdata.ndat)        
        self.trtLatentDays = self.basicdata.trt[self.basicdata.maskLatentDays]
        self.iter = (self.params.ngibbs * self.params.thinrate) + self.params.burnin

#        self.getProposedDayIntervals()


    def getProposedDayIntervals(self):
        """
        ## Make array (ngibbs x nLatDays) PROPOSED DAY INTERVALS
        """
        self.iter = (self.params.ngibbs * self.params.thinrate) + self.params.burnin

        self.proposedDayInt = np.zeros((self.iter, 
                self.basicdata.nLatentDays))
        for i in range(self.basicdata.nLatentDays):
            range_i = (np.arange(self.basicdata.hatchRangeLatent[i,0], 
                self.basicdata.hatchRangeLatent[i,1] + 1))
            self.proposedDayInt[:, i] = np.random.choice(range_i, self.iter)


    def rCovUpdate(self, x, coeffPara, varPara, npara, search, priors, mask):
        """
        ## UPDATE BETAS FOR MASS AND SURVIVAL MODELS
        """
        massT_1 = self.basicdata.MassT_1[mask]
        dayInter = self.basicdata.dayInterval[mask]
        mass = self.basicdata.mass[mask]
        alive = self.basicdata.alive[mask]
        nObs = len(alive)
        mu_s = np.zeros(nObs)
        s_s = np.zeros(nObs)
        for i in range(npara):

            b_s = coeffPara.copy()
            b_s[i] = np.random.normal(coeffPara[i], search[i])


#            r_s = ((np.dot(x, b_s) + self.basicdata.nestEffArray[mask]) * 
#                self.basicdata.dayInterval[mask])
#            mu_s = massT_1 * np.exp(r_s * (1.0 - (massT_1 / self.params.maxMass)))
#            meanMass_s = (mu_s + massT_1) / 2.0 / 1000.0
#            s_s = (np.exp(-self.basicdata.gamArray[mask] / 
#                    meanMass_s**2 ))**dayInter

            r_s = np.dot(x, b_s) + self.basicdata.nestEffArray[mask]

            (mu_s, s_s) = fullMassSurvFX(nObs, massT_1, mu_s, s_s, 
                dayInter, r_s, self.params.maxMass, self.basicdata.gamArray[mask])

            lMassPDF = np.sum(stats.norm.logpdf(np.log(mass), 
                    np.log(self.basicdata.mu[mask]), np.sqrt(varPara)))
            lMassPDF_s = np.sum(stats.norm.logpdf(np.log(mass), np.log(mu_s), 
                    np.sqrt(varPara)))
            lBernPMF =  np.sum(stats.binom.logpmf(alive, 1, self.basicdata.s[mask]))
            lBernPMF_s =  np.sum(stats.binom.logpmf(alive, 1, s_s))
            bpriors = (stats.norm.logpdf(coeffPara[i], priors[0], priors[1]))
            bpriors_s = (stats.norm.logpdf(b_s[i], priors[0], priors[1]))

#            if (g<3000) & (g>2990):
#                print('i', i, 'B', self.params.B[i], 
#                    'b', np.round(self.basicdata.b[i], 3), 'b_s', np.round(b_s[i],3), 
#                    'm', np.round(lMassPDF, 2), 'm_s', np.round(lMassPDF_s, 2),
#                    'bern', np.round(lBernPMF, 2), 'bern_s', np.round(lBernPMF_s, 2))

            pnow = lMassPDF + lBernPMF + bpriors
            pnew = lMassPDF_s + lBernPMF_s + bpriors_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0)
            if (rValue > zValue):
                coeffPara = b_s.copy()
                self.basicdata.r[mask] = r_s
                self.basicdata.mu[mask] = mu_s
#                self.basicdata.meanMass[mask] = meanMass_s
                self.basicdata.s[mask] = s_s
        return(coeffPara)


    def gammaUpdate(self):
        """
        ## UPDATE GAMMA SURVIVAL PARAMETER
        """
        for i in range(self.params.nGAM):
            gam_s = self.basicdata.gam.copy()
            gam_s[i] = np.exp(np.random.normal(np.log(self.basicdata.gam[i]), 
                        self.params.gammaSearch))
            ## MAKE GAMMA ARRAY FOR EACH DEVEL PERIOD
            gamArray_s = gam_s[self.basicdata.develArray]

#        for i in range(self.basicdata.ndat):
#            print('sess', self.basicdata.session[i], 'develArr', self.basicdata.develArray[i], 
#                'gam', np.round(gamArray_s[i], 2))

#            s_s = (np.exp(-gamArray_s / self.basicdata.meanMass**2 ))**self.basicdata.dayInterval

            (self.mu_s, self.s_s) = fullMassSurvFX(self.basicdata.ndat, self.basicdata.MassT_1, 
                self.mu_s, self.s_s, self.basicdata.dayInterval, self.basicdata.r, 
                self.params.maxMass, gamArray_s)

            maskDevel = self.basicdata.develArray == i
            bern_logpmf = np.sum(stats.binom.logpmf(self.basicdata.alive, 1, 
                    self.basicdata.s)[maskDevel])
            bern_logpmf_s = np.sum(stats.binom.logpmf(self.basicdata.alive, 1, self.s_s)[maskDevel])
            priorPDF = np.log(gamma_pdf(self.basicdata.gam[i], self.params.gamPrior[0], 
                    self.params.gamPrior[1]))
            priorPDF_s = np.log(gamma_pdf(gam_s[i], self.params.gamPrior[0], 
                    self.params.gamPrior[1]))
            pnow = bern_logpmf + priorPDF
            pnew = bern_logpmf_s + priorPDF_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0)

#        print('g', self.basicdata.gam, 'gs', gam_s, 'bern', bern_logpmf, 'bern_s', 
#            bern_logpmf_s, 'prior', priorPDF, 'prior_s', priorPDF_s)

            if (rValue > zValue):
                self.basicdata.gam = gam_s.copy()
                self.basicdata.gamArray = gamArray_s.copy()
                self.basicdata.s = self.s_s.copy()
 

    def nestEffectUpdate(self):
        """
        ## UPDATE NEST EFFECT FOR MASS AND SURVIVAL MODEL
        """
        nestEff_s = np.random.normal(self.basicdata.nestEffect, self.params.nestEffSearch)
        nestEffArray_s = nestEff_s[self.basicdata.nest]

#        r_s = np.exp(np.log(self.basicdata.r) - self.basicdata.nestEffArray + nestEffArray_s)

#        r_s = np.zeros(self.basicdata.ndat)
#        r_s[self.basicdata.maskWB] = ((np.dot(self.basicdata.xBeta, self.basicdata.b) + 
#            nestEffArray_s[self.basicdata.maskWB]) * 
#            self.basicdata.dayInterval[self.basicdata.maskWB])
#        r_s[self.basicdata.maskControl] = ((np.dot(self.basicdata.xTau, self.basicdata.tau) + 
#            nestEffArray_s[self.basicdata.maskControl]) * 
#            self.basicdata.dayInterval[self.basicdata.maskControl])

        r_s = self.basicdata.r - self.basicdata.nestEffArray + nestEffArray_s

#        r_s[self.basicdata.maskWB] = (np.dot(self.basicdata.xBeta, self.basicdata.b) + 
#            nestEffArray_s[self.basicdata.maskWB])
#        r_s[self.basicdata.maskControl] = (np.dot(self.basicdata.xTau, self.basicdata.tau) + 
#            nestEffArray_s[self.basicdata.maskControl]) 

        (self.mu_s, self.s_s) = fullMassSurvFX(self.basicdata.ndat, self.basicdata.MassT_1, 
            self.mu_s, self.s_s, self.basicdata.dayInterval, r_s, 
            self.params.maxMass, self.basicdata.gamArray)

#        mu_s = self.basicdata.MassT_1 * np.exp(r_s * (1.0 - 
#            (self.basicdata.MassT_1 / self.params.maxMass)))

#        meanMass_s = (mu_s + self.basicdata.MassT_1) / 2.0 / 1000.0

        betaMass = self.basicdata.mass[self.basicdata.maskWB]
        tauMass = self.basicdata.mass[self.basicdata.maskControl]
        betaMu = self.basicdata.mu[self.basicdata.maskWB]
        tauMu = self.basicdata.mu[self.basicdata.maskControl]
        betaMu_s = self.mu_s[self.basicdata.maskWB]
        tauMu_s = self.mu_s[self.basicdata.maskControl]

#        s_s = (np.exp(-self.basicdata.gamArray / meanMass_s**2 ))**self.basicdata.dayInterval

        ## CURRENT LIKELIHOODS
        lMassPDF = np.zeros(self.basicdata.ndat)
        lMassPDF[self.basicdata.maskWB] = (stats.norm.logpdf(np.log(betaMass), np.log(betaMu), 
                np.sqrt(self.basicdata.betaVar)))
        lMassPDF[self.basicdata.maskControl] = (stats.norm.logpdf(np.log(tauMass), np.log(tauMu), 
                np.sqrt(self.basicdata.tauVar)))
        ## NEW PROPOSAL LIKELIHOODS
        lMassPDF_s = np.zeros(self.basicdata.ndat)
        lMassPDF_s[self.basicdata.maskWB] = (stats.norm.logpdf(np.log(betaMass), np.log(betaMu_s), 
                np.sqrt(self.basicdata.betaVar)))
        lMassPDF_s[self.basicdata.maskControl] = (stats.norm.logpdf(np.log(tauMass), np.log(tauMu_s), 
                np.sqrt(self.basicdata.tauVar)))

        lBernPMF =  stats.binom.logpmf(self.basicdata.alive, 1, self.basicdata.s)
        lBernPMF_s =  stats.binom.logpmf(self.basicdata.alive, 1, self.s_s)
        bpriors = (stats.norm.logpdf(self.basicdata.nestEffect, self.params.nestEffPriors[0],
                self.params.nestEffPriors[1]))
        bpriors_s = (stats.norm.logpdf(nestEff_s, self.params.nestEffPriors[0],
                self.params.nestEffPriors[1]))
        for i in range(self.params.nNests):
            nestMask = self.basicdata.nest == i
            pnow = np.sum(lMassPDF[nestMask] + lBernPMF[nestMask]) + bpriors[i]
            pnew = np.sum(lMassPDF_s[nestMask] + lBernPMF_s[nestMask]) + bpriors_s[i]
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0)
            if (rValue > zValue):
                self.basicdata.nestEffect[i]  = nestEff_s[i]
                self.basicdata.nestEffArray[nestMask] = nestEff_s[i]
                self.basicdata.r[nestMask] = r_s[nestMask]
                self.basicdata.mu[nestMask] = self.mu_s[nestMask]
#                self.basicdata.meanMass[nestMask] = meanMass_s[nestMask]
                self.basicdata.s[nestMask] = self.s_s[nestMask]

    def dayUpdateYYY(self, g):
        """
        METROPOLIS UPDATER FOR UNOBSERVED DAYS
        """
###        dayInt = (self.basicdata.dayInterval[self.basicdata.maskLatentDays]).copy()
#        dayInt_s = np.zeros(self.basicdata.nLatentDays)
#        massT_1 = self.basicdata.MassT_1[self.basicdata.maskLatentDays]
#        r = self.basicdata.r[self.basicdata.maskLatentDays]
#        gamPara = self.basicdata.gamArray[self.basicdata.maskLatentDays]
#        mass = self.basicdata.mass[self.basicdata.maskLatentDays]
#        alive = self.basicdata.alive[self.basicdata.maskLatentDays]
#        mu = self.basicdata.mu[self.basicdata.maskLatentDays]
#        s = self.basicdata.s[self.basicdata.maskLatentDays]
#        changeMask = np.zeros(self.basicdata.nLatentDays, dtype = bool)
#        mu_s = np.zeros(self.basicdata.nLatentDays)
#        s_s = np.zeros(self.basicdata.nLatentDays)
#        dayInt_s = np.zeros(self.basicdata.nLatentDays, dtype = float)
#        trt = self.basicdata.trt[self.basicdata.maskLatentDays]
        ## NUMBA LOOPING FX

        print('BEFORE', self.basicdata.dayInterval[self.basicdata.maskLatentDays][:5])

        loopLatentDays(self.basicdata.ndat, self.basicdata.r,
            self.basicdata.MassT_1, self.basicdata.gamArray,
            self.basicdata.maskLatentDays, self.basicdata.trt,
            self.basicdata.mass, self.basicdata.alive, self.basicdata.mu,
            self.basicdata.s, self.basicdata.dayInterval,
            self.params.maxMass, self.basicdata.betaVar, self.basicdata.tauVar,
            self.basicdata.hatchRange)        


#        (changeMask, mu_s, s_s, dayInt_s) = loopLatentDays(g, self.basicdata.nLatentDays, 
#            self.proposedDayInt, r, gamPara, massT_1, self.trtLatentDays, 
#            self.params.maxMass, mass, mu, self.basicdata.betaVar, self.basicdata.tauVar, 
#            alive, s, changeMask, mu_s, s_s, dayInt_s)        

        ## UPDATE PARAMETERS AND PREDICTIONS
#        self.basicdata.mu[self.basicdata.maskLatentDays][changeMask] = mu_s[changeMask]
#        self.basicdata.s[self.basicdata.maskLatentDays][changeMask] = s_s[changeMask]
#        self.basicdata.dayInterval[self.basicdata.maskLatentDays][changeMask] = (
#            dayInt_s[changeMask])


        print('AFTER', self.basicdata.dayInterval[self.basicdata.maskLatentDays][:5],
            'REAL', self.basicdata.DAYINTERVAL[self.basicdata.maskLatentDays][:5],
            'mask', changeMask[:5],
            'day_s', dayInt_s[:5])

#        if g < 5:
#            print('changemask', changeMask[:5], 'sum', np.sum(changeMask),
#                'dayInter',
#                self.basicdata.dayInterval[self.basicdata.maskLatentDays][:5],
#                'DAYINTR', self.basicdata.dayInterval[self.basicdata.maskLatentDays][:5],
#                'propDI', self.proposedDayInt[g,:5])
    



    def dayUpdateXXX(self):
        """
        METROPOLIS UPDATER FOR UNOBSERVED DAYS
        """
        dayInt = (self.basicdata.dayInterval[self.basicdata.maskLatentDays]).copy()
        dayInt_s = np.zeros(self.basicdata.nLatentDays)
        massT_1 = self.basicdata.MassT_1[self.basicdata.maskLatentDays]
        r_s = self.basicdata[self.basicdata.maskLatentDays]
        gamPara = self.basicdata.gamArray[self.basicdata.maskLatentDays]
        for i in range(self.basicdata.nLatentDays):
            range_i = np.arange(self.basicdata.hatchRangeLatent[i,0], 
                self.basicdata.hatchRangeLatent[i,1] + 1)
            mask_i = ~np.in1d(range_i, dayInt[i]) 
            dayInt_s[i] = np.random.choice(range_i[mask_i], 1)
            days = np.int64(dayInt_s[i])
#            print('dayInt_i', dayInt[i], 'range', range_i, 'dayInt_s', dayInt_s[i])

            (mu_s, s_s) = massSurvFX(massT_1[i], days, r_s[i], 
                self.params.maxMass, gamPara[i]) 
                
        ## PROPOSED R FOR LATENT DAYS
#        r_s (self.basicdata.r[self.basicdata.maskLatentDays] / 
#            self.basicdata.dayInterval[self.basicdata.maskLatentDays] * dayInt_s)

#        massT_1 = self.basicdata.MassT_1[self.basicdata.maskLatentDays]

#        mu_s = massT_1 * np.exp(r_s * (1.0 - (massT_1 / self.params.maxMass)))

#        meanMass_s = (mu_s + massT_1) / 2.0 / 1000.0

#        gamPara = self.basicdata.gamArray[self.basicdata.maskLatentDays]

#        s_s = ((np.exp(-gamPara / meanMass_s**2 ))**
#                self.basicdata.dayInterval[self.basicdata.maskLatentDays])

        mu = self.basicdata.mu[self.basicdata.maskLatentDays]
        mass = self.basicdata.mass[self.basicdata.maskLatentDays]
        s = self.basicdata.s[self.basicdata.maskLatentDays]
        alive = self.basicdata.alive[self.basicdata.maskLatentDays]    


#        lMassPDF = stats.norm.logpdf(np.log(mass), np.log(mu), np.sqrt(self.basicdata.sg))
#        lMassPDF_s = stats.norm.logpdf(np.log(mass), np.log(mu_s), np.sqrt(self.basicdata.sg))

        betaMass = self.basicdata.mass[self.basicdata.maskWB]
        tauMass = self.basicdata.mass[self.basicdata.maskControl]
        betaMu = self.basicdata.mu[self.basicdata.maskWB]
        tauMu = self.basicdata.mu[self.basicdata.maskControl]
        betaMu_s = self.mu_s[self.basicdata.maskWB]
        tauMu_s = self.mu_s[self.basicdata.maskControl]
        ## CURRENT LIKELIHOODS
        lMassPDF = np.zeros(self.basicdata.ndat)
        lMassPDF[self.basicdata.maskWB] = (stats.norm.logpdf(np.log(betaMass), np.log(betaMu), 
                np.sqrt(self.basicdata.betaVar)))
        lMassPDF[self.basicdata.maskControl] = (stats.norm.logpdf(np.log(tauMass), np.log(tauMu), 
                np.sqrt(self.basicdata.tauVar)))
        ## NEW PROPOSAL LIKELIHOODS
        lMassPDF_s = np.zeros(self.basicdata.ndat)
        lMassPDF_s[self.basicdata.maskWB] = (stats.norm.logpdf(np.log(betaMass), np.log(betaMu_s), 
                np.sqrt(self.basicdata.betaVar)))
        lMassPDF_s[self.basicdata.maskControl] = (stats.norm.logpdf(np.log(tauMass), np.log(tauMu_s), 
                np.sqrt(self.basicdata.tauVar)))



        lBernPMF =  stats.binom.logpmf(alive, 1, s)
        lBernPMF_s =  stats.binom.logpmf(alive, 1, s_s)
        pnow = lMassPDF + lBernPMF 
        pnew = lMassPDF_s + lBernPMF_s 
        pdiff = pnew - pnow
        rValue = np.exp(pdiff)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, self.basicdata.nLatentDays)
        changeMask = rValue > zValue
        ## UPDATE PARAMETERS AND PREDICTIONS
        self.basicdata.r[self.basicdata.maskLatentDays][changeMask] = r_s[changeMask]
        self.basicdata.mu[self.basicdata.maskLatentDays][changeMask] = mu_s[changeMask]
#        self.basicdata.meanMass[self.basicdata.maskLatentDays][changeMask] = meanMass_s[changeMask]
        self.basicdata.s[self.basicdata.maskLatentDays][changeMask] = s_s[changeMask]
        self.basicdata.dayInterval[self.basicdata.maskLatentDays][changeMask] = dayInt_s[changeMask]



########            MAIN MCMC FUNCTION
########
    def mcmcFX(self):
        """
        ## RUN FUNCTIONS TO UPDATE PARAMETERS WITH MCMC
        """
        cc = 0          ## COUNTER
        ## EMPTY ARRAYS TO POPULATE WITH MCMC
        self.bgibbs = np.zeros([self.params.ngibbs, self.params.nBetaPara])
        self.bVargibbs = np.zeros(self.params.ngibbs)
        self.tgibbs = np.zeros([self.params.ngibbs, self.params.nTauPara])
        self.tVargibbs = np.zeros(self.params.ngibbs)

#        self.gammagibbs = np.zeros(self.params.ngibbs)
        self.gammagibbs = np.zeros([self.params.ngibbs, self.params.nGAM])

        self.nestEffGibbs = np.zeros([self.params.ngibbs, self.params.nNests])
        self.daygibbs = np.zeros([self.params.ngibbs, self.basicdata.nLatentDays])
        self.deltagibbs = np.zeros([self.params.ngibbs, self.params.nDeltaPara])
        self.sigdeltagibbs = np.zeros(self.params.ngibbs)
        self.alphagibbs = np.zeros([self.params.ngibbs, self.params.nAlphaPara])
        self.sigalphagibbs = np.zeros(self.params.ngibbs)
        self.meanSgibbs = np.zeros(self.basicdata.ndat)
        self.mugibbs = np.zeros(self.basicdata.ndat)
        self.rgibbs = np.zeros(self.basicdata.ndat)
        
        ## LOOP THROUGH ITERATIONS OF MCMC
        for g in range(self.iter):

            ## UPDATE BETA VARIANCE PARAMETER FOR GROWTH RATE MODEL
            self.basicdata.betaVar = vupdate(np.log(self.basicdata.mass[self.basicdata.maskWB]), 
                np.log(self.basicdata.mu[self.basicdata.maskWB]), 
                self.params.s1, self.params.s2, self.basicdata.nWB)

            ## UPDATE BETAS FOR GROWTH RATE - MASS MODEL
            self.basicdata.b = self.rCovUpdate(self.basicdata.xBeta, self.basicdata.b, 
                    self.basicdata.betaVar, self.params.nBetaPara, self.params.betaSearch, 
                    self.params.betaPriors, self.basicdata.maskWB)

            ## UPDATE TAU VARIANCE PARAMETER FOR GROWTH RATE MODEL
            self.basicdata.tauVar = vupdate(np.log(self.basicdata.mass[self.basicdata.maskControl]), 
                np.log(self.basicdata.mu[self.basicdata.maskControl]), 
                self.params.s1, self.params.s2, self.basicdata.nControl)

            ## UPDATE TAU FOR GROWTH RATE - MASS MODEL
            self.basicdata.tau = self.rCovUpdate(self.basicdata.xTau, 
                    self.basicdata.tau, self.basicdata.tauVar, self.params.nTauPara,
                    self.params.tauSearch, self.params.tauPriors, self.basicdata.maskControl)
            ## UPDATE SURVIVAL RATE GAMMA PARAMETER
            self.gammaUpdate()

            ## UPDATE NEST EFFECT
            self.nestEffectUpdate()

#            ## UPDATE UN-OBSERVED DAY INTERVALS
#            self.dayUpdate()

            ## UPDATE LATENT DAYINTERVAL
            dayUpdate(self.basicdata.ndat, self.basicdata.r, self.basicdata.MassT_1, 
                self.basicdata.gamArray, self.basicdata.maskLatentDays, self.basicdata.trt,
                self.basicdata.mass, self.basicdata.alive, self.basicdata.mu,
                self.basicdata.s, self.basicdata.dayInterval, self.params.maxMass, 
                self.basicdata.betaVar, self.basicdata.tauVar, self.basicdata.hatchRange)        

            ## UPDATE DELTA PARAMETERS FOR PROV FREQUENCY
            self.basicdata.delta = gibbsUpdater(self.basicdata.xPROV, 
                np.log(self.basicdata.provFreq),
                self.basicdata.sigDelta, self.params.deltaDiag, self.params.nDeltaPara)
            ## UPDATE PREDICTED PROV FREQ DATA WITH NEW DELTA PARAMETERS  
            self.basicdata.muDelt = np.dot(self.basicdata.xPROV, self.basicdata.delta)
            ## UPDATE DELTA VARIANCE PARAMETER 'sigDelta'
            self.basicdata.sigDelta = vupdate(np.log(self.basicdata.provFreq),
                self.basicdata.muDelt, self.params.s1, self.params.s2,
                self.basicdata.nWB)

            ## UPDATE ALPHA PARAMETERS FOR PROV MASS
            self.basicdata.alpha = gibbsUpdater(self.basicdata.xPROV, self.basicdata.provMass,
                self.basicdata.sigAlpha, self.params.alphaDiag, self.params.nAlphaPara)
            ## UPDATE PREDICTED PROV  DATA WITH NEW DELTA PARAMETERS  
            self.basicdata.muAlpha = np.dot(self.basicdata.xPROV, self.basicdata.alpha)
            ## UPDATE DELTA VARIANCE PARAMETER 'sigDelta'
            self.basicdata.sigAlpha = vupdate(self.basicdata.provMass,
                self.basicdata.muAlpha, self.params.s1, self.params.s2,
                self.basicdata.nWB)


            ## POPULATE STORAGE ARRAYS WITH PARAMETER ESTIMATES
            if (g in self.params.keepseq):
                self.bgibbs[cc] = self.basicdata.b
                self.bVargibbs[cc] = self.basicdata.betaVar

                self.tgibbs[cc] = self.basicdata.tau
                self.tVargibbs[cc] = self.basicdata.tauVar

                self.gammagibbs[cc] = self.basicdata.gam

                self.nestEffGibbs[cc] = self.basicdata.nestEffect
                self.daygibbs[cc] = (
                    self.basicdata.dayInterval[self.basicdata.maskLatentDays])
                self.deltagibbs[cc] = self.basicdata.delta
                self.sigdeltagibbs[cc] = self.basicdata.sigDelta
                self.alphagibbs[cc] = self.basicdata.alpha
                self.sigalphagibbs[cc] = self.basicdata.sigAlpha
                self.meanSgibbs += self.basicdata.s
                self.mugibbs += self.basicdata.mu
                self.rgibbs += self.basicdata.r

                cc = cc + 1


#        for i in range(self.basicdata.ndat):
#            print('sess', self.basicdata.session[i], 'develArr', self.basicdata.develArray[i], 
#                'gam', np.round(self.basicdata.gamArray[i], 2), 
#                'S', np.round(self.basicdata.S[i], 2), 's', np.round(self.basicdata.s[i],2))


#        print('B', self.params.B, 'bgibbs', np.mean(self.bgibbs, axis = 0), 
#            'SG', self.params.SG, 'sg', np.mean(self.sgibbs),
#            'GAM', self.params.GAM, 'gam', np.mean(self.gammagibbs, axis = 0),)
#        meanNE = np.mean(self.nestEffGibbs, axis = 1)
#        print(self.basicdata.nestEffect[:5],'mean NestEff', meanNE[:5], 
#            'sd',np.std(self.nestEffGibbs, axis = 1)[:5]) 
#        print('DELTA', self.params.DELTA, 'deltagib', np.mean(self.deltagibbs, axis = 0))
#        print('sigDelta', self.params.SIGDELTA, 
#            'sigdeltgibbs', np.mean(self.sigdeltagibbs))
#        print('ALPHA', self.params.ALPHA, 'alphagib', np.mean(self.alphagibbs, axis = 0))
#        print('sigAlpha', self.params.SIGALPHA, 'sig alpha gibbs', np.mean(self.sigalphagibbs))
    ################
    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        npara = self.params.nBetaPara + self.params.nTauPara + 4 + 3 + 3 + 3 + 3

        resultTable = np.zeros(shape = ((npara), 4))
        ## BGIBBS
        resultTable[:self.params.nBetaPara, 0] = self.params.B
        resultTable[:self.params.nBetaPara, 1] = np.mean(self.bgibbs, axis = 0)
        resultTable[:self.params.nBetaPara, -2:] = np.transpose(np.apply_along_axis(
                self.quantileFX, 0, self.bgibbs))

        ## BETAVARGIBBS
        row0 = self.params.nBetaPara
        resultTable[row0, 0] = self.params.BETAVAR
        resultTable[row0, 1] = np.mean(self.bVargibbs)
        resultTable[row0, -2:] = self.quantileFX(self.bVargibbs)

        ## TGIBBS
        row0 = row0 + 1
#        row1 = row0 + 1
        row1 = row0 + self.params.nTauPara
        resultTable[row0:row1, 0] = self.params.TAU
        resultTable[row0:row1, 1] = np.mean(self.tgibbs, axis = 0)
        resultTable[row0:row1, -2:] = np.transpose(np.apply_along_axis(
                self.quantileFX, 0, self.tgibbs))

        ## TAUVARGIBBS
        row0 = row1
        resultTable[row0, 0] = self.params.TAUVAR
        resultTable[row0, 1] = np.mean(self.tVargibbs)
        resultTable[row0, -2:] = self.quantileFX(self.tVargibbs)


        ## gammagibbs
        row0 = row0 + 1
#        row1 = row0 + 1
        row1 = row0 + self.params.nGAM
#        resultTable[row0 , 0] = self.params.GAM
#        resultTable[row0 , 1] = np.mean(self.gammagibbs)
#        resultTable[row0, -2:] = self.quantileFX(self.gammagibbs)

        resultTable[row0:row1 , 0] = self.params.GAM
        resultTable[row0:row1 , 1] = np.mean(self.gammagibbs, axis = 0)
        resultTable[row0:row1, -2:] = np.transpose(np.apply_along_axis(
               self.quantileFX, 0, self.gammagibbs))


        ## deltagibbs
        row0 = row1 
        row1 = row0 + self.params.nDeltaPara
        resultTable[row0:row1 , 0] = self.params.DELTA
        resultTable[row0:row1 , 1] = np.mean(self.deltagibbs, axis = 0)
        resultTable[row0:row1, -2:] = np.transpose(np.apply_along_axis(
                self.quantileFX, 0, self.deltagibbs))
        ## sigdeltagibbs
        row0 = row1
        row1 = row0 + 1

        resultTable[row0:row1 , 0] = self.params.SIGDELTA
        resultTable[row0:row1 , 1] = np.mean(self.sigdeltagibbs, axis = 0)
        resultTable[row0:row1, -2:] = self.quantileFX(self.sigdeltagibbs)
        ## alphagibbs
        row0 = row1
        row1 = row0 + self.params.nAlphaPara
       
        resultTable[row0:row1 , 0] = self.params.ALPHA
        resultTable[row0:row1 , 1] = np.mean(self.alphagibbs, axis = 0)
        resultTable[row0:row1, -2:] = np.transpose(np.apply_along_axis(
                self.quantileFX, 0, self.alphagibbs))
        ## sigalphagibbs
        row0 = row1
        row1 = row0 + 1
        resultTable[row0:row1 , 0] = self.params.SIGALPHA
        resultTable[row0:row1 , 1] = np.mean(self.sigalphagibbs, axis = 0)
        resultTable[row0:row1, -2:] = self.quantileFX(self.sigalphagibbs)
        resultTable = np.round(resultTable, 3)
        ## SUMMARY OF R FOR TRT GROUPS
        row0 = row1
        resultTable[row0, 1] = np.round(np.mean(self.rgibbs[self.basicdata.trt == 0] /
                self.params.ngibbs),3)
        resultTable[row0, -2:] = np.round(self.quantileFX(self.rgibbs[self.basicdata.trt == 0] /
                self.params.ngibbs), 3)
        row0 = row1
        resultTable[(row0 + 1), 1] = np.round(np.mean(self.rgibbs[self.basicdata.trt == 1] /
                self.params.ngibbs), 3)
        resultTable[(row0 + 1), -2:] = np.round(self.quantileFX(self.rgibbs[self.basicdata.trt == 1] /
                self.params.ngibbs), 3)
        row0 = row1
        resultTable[(row0 + 2), 1] = np.round(np.mean(self.rgibbs[self.basicdata.trt == 2] /
                self.params.ngibbs), 3)
        resultTable[(row0 + 2), -2:] = np.round(self.quantileFX(self.rgibbs[self.basicdata.trt == 2] /
                self.params.ngibbs), 3)

        ## MAKE A PRETTY TABLE
        aa = prettytable.PrettyTable(['Names', 'SETVALUE', 'Mean', 'Low CI', 'High CI'])
        ## INTERCEPT, N_PROV, PROV_MASS, MassDiff, CONTROL, ICE_CONTROL
        self.names = ['B_intercept', 'B_N_PROV', 'B_PROV_MASS', 'B_MassDiff', 'B_Var', 
                'T_intercept', 'T_MassDiff', 'T_ICE_CONTROL', 'T_Var',
                'gam_Br', 'gam_Gr', 'gam_Cr', 'DeltaIntercept', 'DeltaSplash', 'DeltaIce',
                'DeltaSigma', 'AlphaIntercept', 'AlphaSplash', 'AlphaIce', 'AlphaSigma',
                'MC_R', 'SPLASH_R', 'Control_R']
        for i in range(np.shape(resultTable)[0]):
            name = self.names[i]
#            print('i', i, 'name', name)
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self):
        """
        make diagnostic trace plots
        """
        cc = 0
        for i in range(8):
            if i == 0:
                P.figure(figsize=(10, 8))
                for j in range(self.params.nBetaPara):                
                    P.subplot(2, 2, j+1)
                    P.axhline(y=self.params.B[j], color = 'r', linewidth = 2)
                    P.plot(self.bgibbs[:, j])
                    P.title(self.names[cc])
                    cc += 1
            elif i == 1:
                cc += 1
                P.figure(figsize=(10, 8))
                for j in range(self.params.nTauPara):                
                    P.subplot(2, 2, j+1)
                    P.axhline(y=self.params.TAU[j], color = 'r', linewidth = 2)
                    P.plot(self.tgibbs[:, j])
                    P.title(self.names[cc])
                    cc += 1
            elif i == 2:
                P.figure(figsize=(10, 8))
                P.subplot(2, 2, 1)
                P.axhline(y=self.params.BETAVAR, color = 'r', linewidth = 2)
                P.plot(self.bVargibbs)
                P.title('Beta Var')
                P.subplot(2, 2, 2)
                P.axhline(y=self.params.TAUVAR, color = 'r', linewidth = 2)
                P.plot(self.tVargibbs)
                P.title('Tau Var')
                P.subplot(2, 2, 3)
                P.axhline(y=self.params.SIGDELTA, color = 'r', linewidth = 2)
                P.plot(self.sigdeltagibbs)
                P.title('sigma Delta')
                P.subplot(2, 2, 4)
                P.axhline(y=self.params.SIGALPHA, color = 'r', linewidth = 2)
                P.plot(self.sigalphagibbs)
                P.title('sigma Alpha')
            elif i == 3:
                P.figure(figsize=(10, 8))
                for j in range(3):
                    P.subplot(2, 2, (j+1))
                    P.axhline(y=self.params.GAM[j], color = 'r', linewidth = 2)
                    P.plot(self.gammagibbs[:, j])
                    P.title(self.names[9+j])
            elif i == 4:
                P.figure(figsize=(10, 8))
                for j in range(3):
                    P.subplot(2, 2, (j+1))
                    P.axhline(y=self.params.DELTA[j], color = 'r', linewidth = 2)
                    P.plot(self.deltagibbs[:, j])
                    P.title(self.names[12+j])
            elif i == 5:
                P.figure(figsize=(10, 8))
                for j in range(3):
                    P.subplot(2, 2, (j+1))
                    P.axhline(y=self.params.ALPHA[j], color = 'r', linewidth = 2)
                    P.plot(self.alphagibbs[:, j])
                    P.title(self.names[16+j])
            elif i == 6:
                P.figure(figsize=(10, 8))
                nesttrace = [1, 15, 21, 45] 
                for j in range(4):
                    P.subplot(2, 2, (j+1))
                    P.axhline(y=self.basicdata.NESTEFFECT[j], color = 'r', linewidth = 2)
                    P.plot(self.nestEffGibbs[:, j])
                    P.title('nestEff' + str(nesttrace[j]))                
            elif i == 7:
                P.figure(figsize=(10, 8))
                daytrace = [1, 15, 21, 33] 
                for j in range(4):
                    P.subplot(2, 2, (j+1))
                    P.axhline(y=
                        self.basicdata.DAYINTERVAL[self.basicdata.maskLatentDays][j], 
                        color = 'r', linewidth = 2)
                    P.plot(self.daygibbs[:, j])
                    P.title('dayInterval' + str(daytrace[j]))                

        P.figure(figsize=(10, 8))
        P.subplot(1,2,1)
        P.scatter(self.basicdata.S[self.basicdata.trt == 0], 
                self.meanSgibbs[self.basicdata.trt == 0] / self.params.ngibbs, color='k')
        P.scatter(self.basicdata.S[self.basicdata.trt == 1], 
                self.meanSgibbs[self.basicdata.trt == 1] / self.params.ngibbs, color='b')
        P.scatter(self.basicdata.S[self.basicdata.trt == 2], 
                self.meanSgibbs[self.basicdata.trt == 2] / self.params.ngibbs, color='r')
        P.title('S vs s - Trt')

        P.subplot(1,2,2)
        P.scatter(self.basicdata.S[self.basicdata.session == 0], 
                self.meanSgibbs[self.basicdata.session == 0] / self.params.ngibbs, color='k')
        P.scatter(self.basicdata.S[self.basicdata.session == 1], 
                self.meanSgibbs[self.basicdata.session == 1] / self.params.ngibbs, color='b')
        P.scatter(self.basicdata.S[self.basicdata.session == 2], 
                self.meanSgibbs[self.basicdata.session == 2] / self.params.ngibbs, color='r')
        P.scatter(self.basicdata.S[self.basicdata.session == 3], 
                self.meanSgibbs[self.basicdata.session == 3] / self.params.ngibbs, color='y')
        P.scatter(self.basicdata.S[self.basicdata.session == 4], 
                self.meanSgibbs[self.basicdata.session == 4] / self.params.ngibbs, color='g')
        P.scatter(self.basicdata.S[self.basicdata.session == 5], 
                self.meanSgibbs[self.basicdata.session == 5] / self.params.ngibbs, color='m')
        P.title('S vs s - Sess')

        P.figure(figsize=(10, 8))
        P.subplot(2,2,1)
        P.scatter(self.basicdata.NESTEFFECT, np.mean(self.nestEffGibbs, axis=0))
        P.title('Nest v nest')
        P.subplot(2,2,2)
        P.scatter(self.basicdata.mass, self.mugibbs / self.params.ngibbs)
        P.title('mass v mu')
        P.subplot(2,2,3)
        P.scatter(self.basicdata.R, self.rgibbs / self.params.ngibbs)
        P.title('R v r')

        P.show()



########            Main function
#######
def main():


    params = Params()
    basicdata = Basicdata(params)

    mcmcobj = MCMC(params, basicdata)
    mcmcobj.mcmcFX()
    mcmcobj.makeTableFX()
    mcmcobj.plotFX()

if __name__ == '__main__':
    main()

