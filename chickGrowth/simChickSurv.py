#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
import datetime

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        self.T = 5          ## N SESSIONS
        self.ddays = 7
        ## INITIAL DATE
        self.dateArr = np.array(datetime.date(2019,12,21))
        ## DEVELOPEMENT STAGE EFFECT (BR, GUARD, CRECHE)
        self.GAM = np.array([0.05, 0.22])
        ## TRT EFFECT ON N PROVISIONING FREQ FOR MC AND SPLASH (POISON PROCESS)
        self.PHI = np.array([2.5, 2.0])
        ## TRT EFFECT ON PROVISIONING MASS MC AND SPLASH (NORMAL PROCESS)
        self.TAU = np.array([0.4, 0.3])   ## EFFECT OF MC AND SPLASH
        self.EPSIL = 0.2       ## PROVISION MASS STANDARD DEVIATION
        ## HATCHING MASS = 85 GRAMS.
        self.HatchMass = 85.0
        self.probUnknownDate0 = 0.4


        self.nInd = 120 
        self.nNests = np.int(self.nInd / 2)
        self.nestPerTrt = 20
#        self.ndat = 20
        self.nTrt = 3

        # betas log scale used to simulate data
        ## INTERCEPT, N_PROV, PROV_MASS, MassDiff, NON_WB
        self.B = np.array([-3.0, 0.04, 0.03, 1.4, 0.05])
        self.npara = len(self.B)

        # variance used to simulate MASS data on log scale
        self.SG = 0.01

        # Priors
        self.diag = np.diagflat(np.tile(1000., self.npara))
        self.vinvert = np.linalg.inv(self.diag)
        self.s1 = .1
        self.s2 = .1
        self.ppart = np.dot(self.vinvert, np.zeros(self.npara))
        ## NPROV PRIORS FOR TRT EFFECT
        self.nProvSDPrior = 0.9          ## LOG NORMAL SD PRIOR FOR NORMAL.

        # Initial values
        self.b = np.array([-6.5, 6.5])
        self.sg = 0.025

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 10             # number of estimates to save for each parameter
        self.thinrate = 1           # thin rate
        self.burnin = 0             # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        print('ngibbs', self.ngibbs)

class Basicdata(object):
    def __init__(self, params):
        self.params = params
        ##  RUN FUNCTIONS
        self.makedesigndata()
        self.makeCovariates()
        self.removeDeadChicks()

    def makedesigndata(self):
        self.ind = np.repeat(np.arange(self.params.nInd), self.params.T)
        self.nest = np.repeat(np.arange(self.params.nNests), (self.params.T * 2))
        self.trt = np.repeat(np.arange(self.params.nTrt), 
                (self.params.nestPerTrt * self.params.T * 2))
        self.session = np.tile(np.arange(self.params.T), self.params.nInd) 
        self.uNest = np.unique(self.nest)
        self.ndat = len(self.trt)
        self.alive = np.zeros(self.ndat, dtype = int)
        self.alive[self.session==0] = 1
        ## MAKE DATES
        self.dateArr = self.params.dateArr
        date_i = self.params.dateArr
        for i in range(self.params.T-1):
            date_i = date_i + datetime.timedelta(days = self.params.ddays)
            self.dateArr = np.append(self.dateArr, date_i)
            print('diff', (date_i - self.dateArr[0]).days)
        print(self.dateArr, 'ndat', self.ndat, len(self.ind), len(self.nest), len(self.session))

        self.makeHatchDate()


    def makeCovariates(self):        
        ## MAKE DEVELOPMENT PERIODS: BROOD, GUARD, CRECHE
        self.devel = np.zeros(self.params.T, dtype = int)
        self.devel[:3] = 0
        self.devel[3:] = 1
        print('self.devel', self.devel)
        self.develArray = np.tile(self.devel, self.params.nInd)

        ## MAKE GAMMA ARRAY FOR EACH DEVEL PERIOD
        self.GAMArray = self.params.GAM[self.develArray]
#        print('gamArray', self.GAMArray) 

        ## MAKE X DATA FOR PROVISIONING RATE MODEL
        ## LENGTH OF WB DATA AND CONTROL NESTS
        self.maskWB = self.trt < 2
        self.nWB = len(self.trt[self.maskWB])
#        print('nwb', self.nWB, 'nControl', self.nControl)

        ## X DATA FOR PROVISION RATE MODEL - INTERCEPT, N_PROV, PROV_MASS, C_PLUS, CONTROL
        self.x = np.zeros((self.ndat, self.params.npara))
        ## INTERCEPT
        self.x[:, 0] = 1.0
        #NPROV
        self.trtWB = self.trt[self.maskWB]
        self.NPROV = np.zeros(self.ndat)
        self.NPROV[self.maskWB] = (np.random.poisson(self.params.PHI[self.trtWB]) / 
                self.DAYINTERVAL[self.maskWB])
        self.x[self.maskWB, 1] = self.NPROV[self.maskWB]    ## INITIAL VALUES
        ## GET PRIORS FOR NPROV-GET RANGE FOR EACH NEST
        sessNotZeroMask = self.session > 0
        sessWBMask = self.maskWB & sessNotZeroMask
        self.params.nProvLogMeanPrior = np.log(np.mean(self.NPROV[sessWBMask]))
        print('prior nprov', self.params.nProvLogMeanPrior, self.params.nProvSDPrior)
        ## MEASURED NPROV DATA, I.E. MISSING SESSION 0; INITIAL VALUES
        self.nprov = self.NPROV.copy()
        sess0Mask = self.session == 0
        sess0WBMask = sess0Mask & self.maskWB
        self.nprov[sess0WBMask] = np.exp(np.random.normal(self.params.nProvLogMeanPrior,
                self.params.nProvSDPrior, size = np.sum(sess0WBMask)))

        ## PROV_MASS
        self.PROVMASS = np.zeros(self.ndat)
        self.PROVMASS[self.maskWB] = (np.random.normal(self.params.TAU[self.trtWB], 
                    self.params.EPSIL) / self.DAYINTERVAL[self.maskWB])
        ## PRIORS FOR UNOBSERVED PROV_MASS
        self.meanProvMassPrior = np.mean(self.PROVMASS[sessWBMask])
        self.sdProvMassPrior = np.std(self.PROVMASS[sessWBMask]) 
        self.x[self.maskWB, 2] = self.PROVMASS[self.maskWB]  
        ## INITIAL VALUES OF PROV MASS
        self.x[sess0WBMask, 2] = np.random.normal(self.meanProvMassPrior,
            self.sdProvMassPrior, size = np.sum(sess0WBMask)) 


        ## MAKE INITIAL MASS DATA
        self.makeInitialMass()
        ## MAKE NEST EFFECT
        self.NESTEFFECT = np.random.normal(0,.001, self.ndat)
        ## NON-WB EFFECT
        self.x[(self.trt == 2), 4] = 1.0

        self.massSurvData()

        ## LOOK AT DATA
#        for i in range(self.ndat):
#            print('sess', self.session[i],'ind', self.ind[i], 'nest', self.nest[i], 
#                'trt', self.trt[i],
#                'nprov', self.x[i,1], 'pmass', np.round(self.x[i,2],2), 
#                'mass', self.mass[i], 'lilBro', self.x[i,3],
#                'control', self.x[i, 4],
#                'predMass', np.round(self.predMass[i],1), 'R',np.round(self.R[i],2),
#                'S',np.round(self.S[i],2),
#                'A', self.alive[i])

#        print('x', self.x[:20, 4], self.x[:20, -1])


    def makeHatchDate(self):
        self.DAYINTERVAL = np.repeat(7.0, self.ndat)
        sess0Mask = self.session == 0
        nSess0 = np.sum(sess0Mask)
        self.DAYINTERVAL[sess0Mask] = np.round(np.random.uniform(3, 7, nSess0), 0)
        self.knownHatchDate = np.ones(self.ndat, dtype = bool)
        self.HatchRange = np.zeros((self.ndat, 2))
        ## LOOP THRU INDIVID AND DETERMINE IF KNOWN HATCH DATE
        for i in range(self.params.nInd):
            indMask = self.ind == self.ind[i]
            sess0IndMask = sess0Mask & indMask
            knownHatchDate_i = np.random.binomial(1, 0.5)
            self.knownHatchDate[sess0IndMask] = knownHatchDate_i
            if knownHatchDate_i == 0:
                low_i = np.random.uniform(self.DAYINTERVAL[sess0IndMask] - 2, 
                        self.DAYINTERVAL[sess0IndMask])
                self.HatchRange[sess0IndMask, 0] = np.round(low_i, 0)
                high_i = np.random.uniform(self.DAYINTERVAL[sess0IndMask] + 1, 
                        self.DAYINTERVAL[sess0IndMask] + 2)
                self.HatchRange[sess0IndMask, 1] = np.round(high_i, 0)
#                print('bool', knownHatchDate_i, 'KNOWNINTER', 
#                    self.DAYINTERVAL[sess0IndMask],
#                    'range', self.HatchRange[sess0IndMask], 'low_i', low_i, 'hi_i', high_i)
        self.diffDays = self.DAYINTERVAL.copy()

    def makeInitialMass(self):
        """
        Make initial mass data for time step 0
        """
        sess0Mask = self.session == 0
        self.nSess1_T = np.sum(~sess0Mask)
        self.mass = np.zeros(self.ndat)
        ## MASS DIFF BETWEEN SIBLININGS
        for i in range(self.params.nNests):
            nest_i = self.uNest[i]
            nestMask = self.nest == nest_i
            sessNestMask = sess0Mask & nestMask
            mass_i = np.zeros(2)
            mass_i[0] = np.random.uniform(200, 400)
            mass_i[1] = np.random.uniform(350, 550)
            self.mass[sessNestMask] = mass_i
            percentMass = (mass_i / np.sum(mass_i))**2         
            self.x[sessNestMask, 3] = percentMass
#        print('x', self.x[sess0Mask][:20])

    def massSurvData(self):
        """
        MAKE MASS AND SURVIVAL DATA
        """
        self.keepMask = np.zeros(self.ndat, dtype = bool)
        self.R = np.zeros(self.ndat)
        self.S = np.zeros(self.ndat)
        self.predMass = np.zeros(self.ndat)
        self.uInd = np.unique(self.ind)
        ## LOOP NESTS
        for i in range(self.params.nNests):
            nest_i = self.uNest[i]
            nestMask = self.nest == nest_i
            ## LOOP SESSIONS
            for j in range(self.params.T):

                if j == 0:
                    aa =1


                sessMask = (self.session == j)
                sessT_1Mask = (self.session == (j-1))
                nestSessMask = nestMask & sessMask
                nestSessT_1Mask = nestMask & sessT_1Mask
                mass_ij = self.mass[nestSessT_1Mask]
                sumMass_ij = np.sum(mass_ij)
                if sumMass_ij == 0:
                    break
                percDiff_ij = (mass_ij / np.sum(mass_ij))**2
#                print('percDiff', percDiff_ij)

                self.x[nestSessMask, 3] = percDiff_ij

                ind_ij = np.unique(self.ind[nestSessMask])

                ## LOOP INDIVIDUALS
                for k in range(len(ind_ij)):
                    ind_ijk = ind_ij[k]
                    indMask = self.ind == ind_ijk
                    nestSessIndMask = nestSessMask & indMask
                    nestSessIndT_1Mask = nestSessT_1Mask & indMask
                    if self.alive[nestSessIndT_1Mask] == 0:
                        continue
                    self.keepMask[nestSessIndMask] = True
                    ## CALC R IN Kg
                    self.R[nestSessIndMask] = np.exp(np.dot(self.x[nestSessIndMask], 
                        self.params.B) + self.NESTEFFECT[nestSessIndMask])
                    ## MASS PREVIOUS TIME SESSION IN KG
                    massT_1_ijk = self.mass[nestSessIndT_1Mask] / 1000.0
                    self.predMass[nestSessIndMask] = massT_1_ijk + (self.R[nestSessIndMask] * 
                        self.params.ddays)
                    meanMass = ((self.predMass[nestSessIndMask] + massT_1_ijk) / 2.0)
                    s_ij = np.exp(-self.GAMArray[nestSessIndMask] / (meanMass)**2 )

                    self.S[nestSessIndMask] = s_ij
                    alive_ij = np.random.binomial(1, s_ij)
                    self.alive[nestSessIndMask] = alive_ij
                    ## RANDOM VARIATE OF MASS IN GRAMS.
                    self.mass[nestSessIndMask] = (np.exp(np.random.normal(
                        np.log(self.predMass[nestSessIndMask]), self.params.SG)) * 1000.)

#                    print('nest', i, 'sess', j, 'ind', ind_ijk, 'x', self.x[nestSessIndMask]) 
                    print('nest', i, 'sess', j, 'ind', ind_ijk, 'alive', alive_ij, 
                        'sij', s_ij, 'mass', self.mass[nestSessIndMask])
                
    def removeDeadChicks(self):
        keepMask = self.alive == 1
        self.x = self.x[keepMask]
        self.mass = self.mass[keepMask]
        self.R = self.R[keepMask]
        self.S = self.S[keepMask]
        self.predMass = self.predMass[keepMask]
        self.alive = self.alive[keepMask]
        self.GAMArray = self.GAMArray[keepMask]
        self.ind = self.ind[keepMask]
        self.nest = self.nest[keepMask]
        self.trt = self.trt[keepMask]
        self.session = self.session[keepMask]
        self.ndat = len(self.trt)
        print('ndat', self.ndat)        

class MCMC(object):
    def __init__(self, params):

        self.params = params

        self.ResultsGibbs = np.zeros([self.params.ngibbs, (self.params.npara +1)]) 


    def bupdate(self):
        """
        This is my function
        """
        xTranspose = np.transpose(self.params.x)
        xCrossProd = np.dot(xTranspose, self.params.x)
        sinv = 1.0/self.params.sg
        sx = np.multiply(xCrossProd, (1./self.params.sg))
        xyCrossProd = np.dot(xTranspose, self.params.y)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vinvert)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        b = np.random.multivariate_normal(meanVariate, bigv)
        self.params.b =  np.transpose(b)
#        self.params.mu = np.dot(self.basicdata.xdat, self.params.b)

    def vupdate(self):
        pred = np.dot(self.params.x, self.params.b)
#        pred = pred.transpose()
        predDiff = self.params.y - pred
#        mufx = self.params.y - pred
#        muTranspose = mufx.transpose()
#        sx = np.dot(mufx, muTranspose)
        sx = np.sum(predDiff**2.0)
        u1 = self.params.s1 + np.multiply(.5, self.params.ndat)
        u2 = self.params.s2 + np.multiply(.5, sx)               # rate parameter    
        isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape and scale
        self.params.sg = 1.0/isg

#


########            Main mcmc function
########
    def mcmcFX(self):
        """
        Run gibbs sampler
        """
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            self.vupdate()

            self.bupdate()


            if g in self.params.keepseq:
                self.ResultsGibbs[cc,0:self.params.npara] = self.params.b
                self.ResultsGibbs[cc,-1] = self.params.sg

                cc = cc + 1

#        print('gibbs', self.ResultsGibbs[:300])


    ################
    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (4, (self.params.npara+1)))

        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.ResultsGibbs)

        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.ResultsGibbs)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Intercept', 'Beta1', 'Variance']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self, params):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, self.ResultsGibbs)
        Ypredicted = np.dot(self.params.x, meanBeta[0:2])
        nplots = self.params.npara + 1  # + 1
        P.figure(0)
        for i in range(nplots):
            P.subplot(2, 2, i+1)
            P.plot(self.ResultsGibbs[:, i])
        P.subplot(2,2, 4)
        P.plot(self.ResultsGibbs[:,0], self.ResultsGibbs[:,1])
#        P.scatter(self.params.y, Ypredicted)
        P.show()





########            Main function
#######
def main():


    params = Params()
    basicdata = Basicdata(params)

#    mcmcobj = MCMC(params)

#    mcmcobj.mcmcFX()
#    mcmcobj.makeTableFX()
#    mcmcobj.plotFX(params)

if __name__ == '__main__':
    main()

